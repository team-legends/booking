import Rating from "@mui/material/Rating";
import { useNavigate } from "react-router";
import {
  imagesPath,
  defaultImage,
  defaultRoomImage,
} from "../../constants/paths";

const SearchItem = ({ hotel }) => {
  const navigate = useNavigate();
  const {
    id,
    address,
    contacts,
    description,
    name,
    services,
    starsRate,
    types,
    image,
  } = hotel;
  const onClickHandler = (id) => {
    navigate(`/hotel/${id}`);
  };

  return (
    <div className="searchItem">
      <img
        src={
          hotel?.image
            ? `${imagesPath.val}/HOTEL/${hotel.id}/${hotel?.image}?w=248&fit=crop&auto=format`
            : `${imagesPath.val}/HOTEL/${defaultImage.val}?w=248&fit=crop&auto=format`
        }
        alt=""
        className="siImg"
      />
      <div className="siDesc">
        <h1 className="siTitle" onClick={() => onClickHandler(id)}>
          {name}
        </h1>
        {/* <span className="siDistance">500m from center</span>
          <span className="siTaxiOp">Free airport taxi</span> */}
        <span className="siSubtitle">{description}</span>
        {/* <span className="siFeatures">
            Entire studio • 1 bathroom • 21m² 1 full bed
          </span> */}
        {/* <span className="siCancelOp">Free cancellation </span> */}
        {/* <span className="siCancelOpSubtitle">
            You can cancel later, so lock in this great price today!
          </span> */}
      </div>
      <div className="siDetails">
        <div className="siRating">
          {/* <span>Excellent</span>
            <button>8.9</button> */}
          <Rating name="read-only" value={starsRate} readOnly />
        </div>
        <div className="siDetailTexts">
          {/* <span className="siPrice">$112</span> */}
          <span className="siTaxOp">Includes taxes and fees</span>
          {/* <button className="siCheckButton">See availability</button> */}
        </div>
      </div>
    </div>
  );
};

export default SearchItem;
