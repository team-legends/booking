import "./navbar.css";
import { useNavigate } from "react-router-dom";

const Navbar = () => {
	const navigate = useNavigate();

	const navigateToRegister = () => {
		navigate("/register");
	};
	const navigateToLogin = () => {
		navigate("/login");
	};
	const navigateToStart = () => {
		navigate("/");
	};
	let isToken = !!localStorage.getItem("token");

	return (
		<div className="navbar">
			<div className="navContainer">
				<span className="logo" onClick={navigateToStart}>
					Booking
				</span>
				<div className="navItems">
					<button
						className="navButton"
						onClick={navigateToRegister}
						hidden={isToken}
					>
						Register
					</button>

					<button className="navButton" onClick={navigateToLogin}>
						Login
					</button>
				</div>
			</div>
		</div>
	);
};

export default Navbar;
