import React from "react";

const SimpleLoader = () => {
  return (
    <div style={{ padding: "20px", textAlign: "center" }}>Загрузка...</div>
  );
};

export default SimpleLoader;