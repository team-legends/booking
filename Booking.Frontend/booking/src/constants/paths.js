export const imagesPath = {
  val: "https://localhost:7209/Images",
};

export const defaultImage = {
  val: "defaultHotelImage.jpg",
};

export const defaultRoomImage = {
  val: "DefaultRoomImage.jpg",
};
