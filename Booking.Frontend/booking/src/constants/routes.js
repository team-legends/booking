const routes = {
  HOME: "/",
  HOTELS: "/hotels",
  LANDING: "/",
  REGISTER: "/register",
  LOGIN: "/login",
  MANAGEHOTELS: "/managehotels",
  ORDER: "/order",
};

export default routes;
