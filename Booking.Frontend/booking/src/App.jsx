import "./App.css";
import { Route, Routes, BrowserRouter } from "react-router-dom";
import React, { Suspense, useState } from "react";
import SimpleLoader from "./components/SimpleLoader";
import LayoutPaperbase from "./pages/LayoutPaperbase";
import Page404 from "./pages/Page404";
import HotelsPage from "./pages/HotelsPage";
import HotelRoomPage from "./pages/HotelRoomPage";
import HomePage from "./pages/HomePage";
import RegistrationAccount from "./pages/RegistrationAccount";
import LoginAccount from "./pages/LoginAccount";
import ManageHotels from "./pages/ManageHotelsPage";
import OrderCardPage from "./pages/OrderCardPage";
import UserOrdersPage from "./pages/UserOrders/UserOrdersPage";

export const FilterContext = React.createContext(null);
export const UserContext = React.createContext();

function App() {
  const [destination, setDestination] = useState("");
  const [date, setDate] = useState([
    {
      startDate: new Date(),
      endDate: new Date(),
      key: "selection",
    },
  ]);
  const [options, setOptions] = useState({
    adult: 1,
    children: 0,
    room: 1,
  });
  const [hotels, setHotels] = useState([]);

  const [userName, setUserName] = useState("");
  const [login, setLogin] = useState("");
  const [password, setPassword] = useState("");
  const [repeatPassword, setRepeatPassword] = useState("");
  const [email, setEmail] = useState("");
  const [userId, setUserId] = useState("");

  const filterContextValue = {
    destination,
    setDestination,
    date,
    setDate,
    options,
    setOptions,
    hotels,
    setHotels,
  };
  const userContextValue = {
    userName,
    setUserName,
    login,
    setLogin,
    password,
    setPassword,
    repeatPassword,
    setRepeatPassword,
    email,
    setEmail,
    userId,
    setUserId,
  };
  return (
    <div className="App">
      <FilterContext.Provider value={filterContextValue}>
        <UserContext.Provider value={userContextValue}>
          <BrowserRouter>
            <Suspense fallback={<SimpleLoader />}>
              <Routes>
                <Route path={"/"} element={<LayoutPaperbase />}>
                  <Route path="/" element={<HomePage />} />
                  <Route path="hotels" element={<HotelsPage />} />
                  <Route path="hotel/:id" element={<HotelRoomPage />} />
                  <Route path="register" element={<RegistrationAccount />} />
                  <Route path="login" element={<LoginAccount />} />
                  <Route path="managehotels" element={<ManageHotels />} />
                  <Route
                    path="order/:hotelId/:roomId"
                    element={<OrderCardPage />}
                  />
                  <Route path="orders" element={<UserOrdersPage />} />
                </Route>
                <Route path="*" element={<Page404 />} />
              </Routes>
            </Suspense>
          </BrowserRouter>
        </UserContext.Provider>
      </FilterContext.Provider>
    </div>
  );
}

export default App;
