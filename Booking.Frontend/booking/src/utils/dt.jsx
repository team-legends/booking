import { format } from "date-fns";

export const ConvertToDate = (date) =>{
    return format(date, "MM/dd/yyyy");
  };
  