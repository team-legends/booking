import React from "react";
import { useContext, useState } from "react";
import { UserContext } from "../../App";
import { useNavigate } from "react-router-dom";
import Checkbox from "@mui/material/Checkbox";
import FormGroup from "@mui/material/FormGroup";
import FormControlLabel from "@mui/material/FormControlLabel";

const validationErrorInitialState = {
  userName: "",
  login: "",
  email: "",
  password: "",
  repeatPassword: "",
};
export default function RegistrationAccount() {
  const {
    userName,
    setUserName,
    login,
    setLogin,
    password,
    setPassword,
    repeatPassword,
    setRepeatPassword,
    email,
    setEmail,
    setUserId,
  } = useContext(UserContext);
  const [apiError, setApiError] = useState("");
  const [validationError, setValidationError] = useState({
    ...validationErrorInitialState,
  });
  const navigate = useNavigate();

  const handleChangeUserName = (event) => {
    setUserName(event.target.value);
    setApiError("");
  };

  const handleChangeLogin = (event) => {
    setLogin(event.target.value);
    setApiError("");
  };

  const handleChangeEmail = (event) => {
    setEmail(event.target.value);
    setApiError("");
  };

  const handleChangePassword = (event) => {
    setPassword(event.target.value);
    setApiError("");
  };

  const handleChangeRepeatPassword = (event) => {
    setRepeatPassword(event.target.value);
    setApiError("");
  };
  const [isChecked, setIsChecked] = useState(false);
  const handleCheckBox = () => {
    setIsChecked((current) => !current);
  };

  const checkEmpty = (str) => !!str.trim();
  const validate = () => {
    let err = validationErrorInitialState;

    if (!checkEmpty(userName)) {
      err.userName = "Поле Name должно быть заполнено";
    }

    if (!checkEmpty(login)) {
      err.login = "Поле Login должно быть заполнено";
    }
    if (!checkEmpty(email)) {
      err.email = "Поле Email должно быть заполнено";
    }

    if (!checkEmpty(password)) {
      err.password = "Поле Password должно быть заполнено";
    }

    if (password !== repeatPassword) {
      err.repeatPassword = "Пароли не совпадают";
    } else if (!checkEmpty(repeatPassword)) {
      err.repeatPassword = "Поле RepeatPassword должно быть заполнено";
    }
    console.log(err);
    setValidationError(err);
    return err;
  };

  const regAccount = (event) => {
    event.preventDefault();
    let errLocal = validate();

    if (
      errLocal.userName ||
      errLocal.login ||
      errLocal.email ||
      errLocal.password ||
      errLocal.repeatPassword ||
      apiError
    ) {
      return;
    }

    fetch(`/api/Auth/Register`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
      },
      body: JSON.stringify({
        Name: userName,
        Login: login,
        Password: password,
        Email: email,
        IsManager: isChecked,
      }),
    })
      .then((res) => res.json())
      .then((res) => {
        if (res?.token && res?.id) {
          localStorage.setItem("token", res.token);
          localStorage.setItem("userId", res.id);
        }
        if (isChecked) navigate("/managehotels");
        else navigate("/");
      })
      .catch(() => {
        setApiError("Что-то пошло не так. Попробуйте позже.");
      });
  };
  const error = <div className="error">{apiError}</div>;

  return (
    <div className="form_wrapper">
      <div className="form_container">
        <div className="title_container">
          <h2>Registration</h2>
        </div>
        <div className="row clearfix">
          <div className="">
            <form>
              <div className="input_field">
                {" "}
                <span>
                  <i aria-hidden="true" className="fa fa-user"></i>
                </span>
                <input
                  type="userName"
                  name="userName"
                  placeholder="Name"
                  required
                  onChange={handleChangeUserName}
                />
                <div className="error">{validationError.userName}</div>
              </div>
              <div className="input_field">
                {" "}
                <span>
                  <i aria-hidden="true" className="fa fa-user"></i>
                </span>
                <input
                  type="login"
                  name="login"
                  placeholder="Login"
                  required
                  onChange={handleChangeLogin}
                />
                <div className="error">{validationError.login}</div>
              </div>
              <div className="input_field">
                {" "}
                <span>
                  <i aria-hidden="true" className="fa fa-envelope"></i>
                </span>
                <input
                  type="email"
                  name="email"
                  placeholder="Email"
                  required
                  onChange={handleChangeEmail}
                />
                <div className="error">{validationError.email}</div>
              </div>
              <div className="input_field">
                {" "}
                <span>
                  <i aria-hidden="true" className="fa fa-lock"></i>
                </span>
                <input
                  type="password"
                  name="password"
                  placeholder="Password"
                  required
                  onChange={handleChangePassword}
                />
                <div className="error">{validationError.password}</div>
              </div>
              <div className="input_field">
                {" "}
                <span>
                  <i aria-hidden="true" className="fa fa-lock"></i>
                </span>
                <input
                  type="password"
                  name="password"
                  placeholder="Re-type Password"
                  required
                  onChange={handleChangeRepeatPassword}
                />
                <div className="error">{validationError.repeatPassword}</div>
              </div>

              <FormGroup>
                <FormControlLabel
                  sx={{ fontSize: "120%" }}
                  control={
                    <Checkbox checked={isChecked} onChange={handleCheckBox} />
                  }
                  label="IsManager"
                />
              </FormGroup>

              <input
                className="button"
                type="submit"
                value="Register"
                onClick={regAccount}
              />
              {error}
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}
