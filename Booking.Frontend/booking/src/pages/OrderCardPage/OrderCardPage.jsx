import React from "react";
import { useEffect, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";
import FormControl from "@mui/material/FormControl";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import MuiModal from "@mui/material/Modal";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";

const defaultOrderData = {
  userId: "",
  hotelId: "",
  roomId: "",
  hotelCountry: "",
  hotelName: "",
  roomPrice: 0,
  roomPersonQuantity: 1,
  userName: "",
  userEmail: "",
};
export default function OrderCardPage() {
  const navigate = useNavigate();
  const [orderData, setOrderData] = useState(defaultOrderData);
  const [open, setOpen] = useState(false);
  const handleClose = () => {
    navigate("/");
    setOpen(false);
  };
  let { hotelId } = useParams();
  let { roomId } = useParams();

  const token = localStorage.getItem("token");
  const userId = localStorage.getItem("userId");

  useEffect(() => {
    GetOrderInfo();
  }, []);

  const GetOrderInfo = () => {
    let request = { userId, hotelId, roomId };
    fetch(`/api/Order/GetOrderInfo`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(request),
    })
      .then((res) => {
        return res.json();
      })
      .then((res) => {
        let initialOrderData = {
          userId,
          hotelId,
          roomId,
          hotelCountry: res.hotelCountry,
          hotelName: res.hotelName,
          roomPrice: res.roomPrice,
          roomPersonQuantity: res.roomPersonQuantity,
          userName: res.userName,
          userEmail: res.userEmail,
        };
        setOrderData(initialOrderData);
      })
      .catch((err) => {
        alert("You should login or register");
      });
  };

  const handleConfirm = () => {
    fetch(`/api/Order/Create`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(orderData),
    })
      .then((res) => {
        if (res.ok) setOpen(true);
        else alert("Произошла ошибка во время создания заказа");
      })
      .catch((err) => {
        console.error(err);
      });
  };

  const navigateToOrdersPage = () => {
    navigate(`/orders`);
  };

  const Modal = () => (
    <MuiModal
      open={open}
      onClose={handleClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box className="box_modal">
        <Typography
          id="modal-modal-title"
          variant="h9"
          component="h2"
          sx={{ p: 3 }}
        >
          Комната успешно забронирована.
        </Typography>
        <Button onClick={() => navigateToOrdersPage()}>
          На страницу заказов
        </Button>
      </Box>
    </MuiModal>
  );

  return (
    <FormControl>
      <Typography
        id="modal-modal-title"
        variant="h9"
        component="h2"
        sx={{ mb: 5 }}
      >
        Проверьте данные для бронирования отеля
      </Typography>
      <TextField
        sx={{ mb: 3 }}
        id="text_contact_info"
        value={orderData.hotelCountry}
        InputProps={{
          readOnly: true,
        }}
        variant="standard"
        helperText="Страна"
      />
      <TextField
        sx={{ mb: 3 }}
        id="text_contact_info"
        value={orderData.hotelName}
        InputProps={{
          readOnly: true,
        }}
        variant="standard"
        helperText="Название отеля"
      />
      <TextField
        sx={{ mb: 3 }}
        id="text_contact_info"
        value={orderData.roomPersonQuantity}
        InputProps={{
          readOnly: true,
        }}
        variant="standard"
        helperText="Количество человек"
      />
      <TextField
        sx={{ mb: 3 }}
        id="text_contact_info"
        value={`${orderData.roomPrice} руб.`}
        InputProps={{
          readOnly: true,
        }}
        variant="standard"
        helperText="Цена"
      />
      <TextField
        sx={{ mb: 3 }}
        id="text_contact_info"
        value={orderData.userName}
        InputProps={{
          readOnly: true,
        }}
        variant="standard"
        helperText="Имя"
      />
      <TextField
        sx={{ mb: 3 }}
        id="text_contact_info"
        value={orderData.userEmail}
        InputProps={{
          readOnly: true,
        }}
        variant="standard"
        helperText="Адрес электронной почты"
      />
      <Button onClick={() => handleConfirm()}>Забронировать</Button>
      <Modal />
    </FormControl>
  );
}
