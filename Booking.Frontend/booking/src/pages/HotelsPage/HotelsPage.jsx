import React, { useContext } from "react";
import { FilterContext } from "../../App";
import { useLocation } from "react-router-dom";
import { useEffect, useState } from "react";
import { format } from "date-fns";
import { DateRange } from "react-date-range";
import SearchItem from "../../components/SearchItem";
import { ConvertToDate } from "../../utils/dt";

const HotelsPage = () => {
  const {
    destination,
    setDestination,
    date,
    setDate,
    options,
    setOptions,
    hotels,
    setHotels,
  } = useContext(FilterContext);

  useEffect(() => {}, []);
  const [openDate, setOpenDate] = useState(false);

  const handleSearch = () => {
    fetch(`/api/Scheduler/GetFilteredHotels`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
      },
      body: JSON.stringify({
        Destination: destination,
        DateFrom: ConvertToDate(date[0].startDate),
        DateTo: ConvertToDate(date[0].endDate),
        Option: {
          Adult: options.adult,
          Children: options.children,
          Room: options.room,
        },
      }),
    })
      .then((res) => {
        res.json();
      })
      .then((res) => {
        setHotels(res);
      });
  };

  return (
    <>
      {/* <div>HotelsPage</div> */}
      <div className="listContainer">
        <div className="listWrapper">
          <div className="listSearch">
            <h1 className="lsTitle">Search</h1>
            <div className="lsItem">
              <label>Destination</label>
              <input placeholder={destination} type="text" />
            </div>
            <div className="lsItem">
              <label>Check-in Date</label>
              <span onClick={() => setOpenDate(!openDate)}>{`${format(
                date[0].startDate,
                "MM/dd/yyyy"
              )} to ${format(date[0].endDate, "MM/dd/yyyy")}`}</span>
              {openDate && (
                <DateRange
                  onChange={(item) => setDate([item.selection])}
                  minDate={new Date()}
                  ranges={date}
                />
              )}
            </div>
            <div className="lsItem">
              <label>Options</label>
              <div className="lsOptions">
                <div className="lsOptionItem">
                  <span className="lsOptionText">
                    Min price <small>per night</small>
                  </span>
                  <input type="number" className="lsOptionInput" />
                </div>
                <div className="lsOptionItem">
                  <span className="lsOptionText">
                    Max price <small>per night</small>
                  </span>
                  <input type="number" className="lsOptionInput" />
                </div>
                <div className="lsOptionItem">
                  <span className="lsOptionText">Adult</span>
                  <input
                    type="number"
                    min={1}
                    className="lsOptionInput"
                    placeholder={options.adult}
                  />
                </div>
                <div className="lsOptionItem">
                  <span className="lsOptionText">Children</span>
                  <input
                    type="number"
                    min={0}
                    className="lsOptionInput"
                    placeholder={options.children}
                  />
                </div>
                <div className="lsOptionItem">
                  <span className="lsOptionText">Room</span>
                  <input
                    type="number"
                    min={1}
                    className="lsOptionInput"
                    placeholder={options.room}
                  />
                </div>
              </div>
            </div>
            <button onClick={handleSearch}>Search</button>
          </div>
          <div className="listResult">
            {hotels
              ?.filter((hotel) => hotel.rooms)
              ?.map((hotel, i) => (
                <SearchItem hotel={hotel} key={i} />
              ))}
          </div>
        </div>
      </div>
    </>
  );
};

export default HotelsPage;
