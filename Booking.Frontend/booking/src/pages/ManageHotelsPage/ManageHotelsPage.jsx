import * as React from "react";
import { useEffect, useState } from "react";
import ImageList from "@mui/material/ImageList";
import ImageListItem from "@mui/material/ImageListItem";
import ImageListItemBar from "@mui/material/ImageListItemBar";
import IconButton from "@mui/material/IconButton";
import DeleteIcon from "@mui/icons-material/Delete";
import AddIcon from "@mui/icons-material/Add";
import EditIcon from "@mui/icons-material/Edit";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import MuiModal from "@mui/material/Modal";
import TextField from "@mui/material/TextField";
import Stack from "@mui/material/Stack";
import Button from "@mui/material/Button";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import update from "immutability-helper";
import {
  imagesPath,
  defaultImage,
  defaultRoomImage,
} from "../../constants/paths";

const editedHotelInitialState = {
  rooms: [
    {
      peopleQuantity: 1,
      price: 0,
      images: [],
    },
  ],
  name: "",
  description: "",
  types: "RESORT",
  image: defaultImage.val,
  address: {
    geoposition: "",
    countryName: "",
    city: "",
    region: "",
    postalCode: "",
    countryCode: "",
    street: "",
    building: "",
  },
  starsRate: 5,
  contacts: [{ type: "VK", value: "", infoText: "" }],
  services: [{ type: "PARKING", informationText: "" }],
  managerId: "",
};

export default function ManageHotels() {
  const token = localStorage.getItem("token");
  const userId = localStorage.getItem("userId");

  const [managerHotels, setManagerHotels] = useState([]);
  const [open, setOpen] = useState(false);
  const [hotelMode, setHotelMode] = useState("add");
  const [editedHotel, setEditedHotel] = useState(editedHotelInitialState);

  useEffect(() => {
    getHotelsByManagerId();
  }, []);

  //hotel
  const getHotelsByManagerId = () => {
    fetch(`/api/Manage/GetHotelsByManagerId`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({ managerId: userId }),
    })
      .then((res) => res.json())
      .then((res) => {
        {
          setManagerHotels(res);
        }
      });
  };

  const handleOpenModalForEdit = (id) => {
    setHotelMode("edit");
    setEditedHotel(managerHotels.find((hotel) => hotel.id === id));
    setOpen(true);
  };

  const saveHotel = () => {
    hotelMode === "add" ? addHotel() : editHotel();
  };

  const editHotel = () => {
    fetch("/api/Manage/EditHotel", {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(editedHotel),
    })
      .then((res) => res.json())
      .then((res) => {
        const updated = update(managerHotels, {
          [editedHotel.id]: { $set: editedHotel },
        });
        setManagerHotels(updated);
        setOpen(false);
      });
  };

  const addHotel = () => {
    fetch(`/api/Manage/CreateHotel/`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(editedHotel),
    })
      .then((res) => res.json())
      .then((res) => {
        editedHotel.id = res.id;

        const updated = update(managerHotels, { $push: [editedHotel] });
        setManagerHotels(updated);
        setOpen(false);
      });
  };

  const deleteHotel = async (index) => {
    const hotelToDeleteId = managerHotels[index]?.id;
    //const reader = new FileReader();
    const API_ENDPOINT = `/api/Image/DeleteImage/HOTEL/${managerHotels[index]?.id}/${managerHotels[index]?.image}`;

    const request = new XMLHttpRequest();
    request.open("DELETE", API_ENDPOINT, true);
    request.onreadystatechange = () => {
      if (request.readyState === 4 && request.status === 200) {
        console.log("Ok.");
      }
    };
    request.send();

    //DeleteHotel
    const response = await fetch(
      "/api/Manage/MarkDeleteHotel/" + hotelToDeleteId,
      {
        method: "PUT",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*",
          Authorization: `Bearer ${token}`,
        },
      }
    );
    if (response.ok) {
      const updated = update(managerHotels, { $splice: [[index, 1]] });
      setManagerHotels(updated);
    }
  };

  //hotel images
  const onHotelFileUpload = (file) => {
    if (!file) {
      return;
    }
    const reader = new FileReader();
    reader.onloadend = () => {
      const API_ENDPOINT = `/api/Image/SaveImages/HOTEL/${editedHotel.id}`;
      const request = new XMLHttpRequest();
      const formData = new FormData();

      request.open("POST", API_ENDPOINT, true);
      request.onreadystatechange = () => {
        if (request.readyState === 4 && request.status === 200) {
          setEditedHotel({
            ...editedHotel,
            image: file.name,
          });
        }
      };
      formData.append("formFiles", file);

      request.send(formData);
    };
    reader.onerror = () => {
      console.log(
        "Произошла ошибка при загрузке файла. Попробуйте  другой файл."
      );
    };
    reader.readAsDataURL(file);
  };

  const deleteHotelImage = () => {
    if (!editedHotel.image || editHotel.image === defaultImage.val) {
      return;
    }
    const API_ENDPOINT = `/api/Image/DeleteImage/HOTEL/${editedHotel.id}/${editedHotel.image}`;
    const request = new XMLHttpRequest();
    request.open("DELETE", API_ENDPOINT, true);
    request.onreadystatechange = () => {
      if (request.readyState === 4 && request.status === 200) {
        setEditedHotel({
          ...editedHotel,
          image: defaultImage.val,
        });
      }
    };
    request.send();
  };

  //room images
  const onRoomFileUpload = (file, index) => {
    if (!file /*|| editHotel.rooms[index].images.includes(file.name)*/) {
      return;
    }
    const API_ENDPOINT = `/api/Image/SaveImages/HOTEL/${editedHotel.id}`;
    const reader = new FileReader();
    reader.onloadend = () => {
      const request = new XMLHttpRequest();
      const formData = new FormData();

      request.open("POST", API_ENDPOINT, true);
      request.onreadystatechange = () => {
        if (request.readyState === 4 && request.status === 200) {
          //var updatedImages = editedHotel.rooms[index].images.push(file.name);
          const updated = update(editedHotel, {
            rooms: {
              [index]: {
                images: { $push: [file.name] },
              },
            },
          });
          setEditedHotel(updated);
        }
      };
      formData.append("formFiles", file.name);

      request.send(formData);
    };
    reader.onerror = () => {
      console.log(
        "Произошла ошибка при загрузке файла. Попробуйте  другой файл."
      );
    };
    reader.readAsDataURL(file);
  };

  const deleteRoomImage = (roomIndex, imgIndex) => {
    let room = editedHotel.rooms[roomIndex];
    let image = room.images[imgIndex];

    if (!image || image === defaultImage.val) {
      return;
    }
    const API_ENDPOINT = `/api/Image/DeleteImage/ROOM/${room.id}/${image}`;
    const request = new XMLHttpRequest();
    request.open("DELETE", API_ENDPOINT, true);
    request.onreadystatechange = () => {
      const updated = update(editedHotel, {
        rooms: {
          [roomIndex]: {
            images: { $splice: [[imgIndex, 1]] },
          },
        },
      });
      setEditedHotel(updated);
    };
    request.send();
  };
  //rooms
  const addRoom = () => {
    const updated = update(editedHotel, {
      rooms: { $push: [{ peopleQuantity: "1", price: 0 }] },
    });
    setEditedHotel(updated);
  };

  const deleteRoom = (index) => {
    const API_ENDPOINT = `/api/Image/DeleteImages/ROOM/${editedHotel.rooms[index]?.id}`;

    const request = new XMLHttpRequest();
    request.open("DELETE", API_ENDPOINT, true);
    request.onreadystatechange = () => {
      if (request.readyState === 4 && request.status === 200) {
        console.log("Ok.");
      }
    };
    request.send();

    const updated = update(editedHotel, {
      rooms: { $splice: [[index, 1]] },
    });
    setEditedHotel(updated);
  };

  //services
  const addService = () => {
    const updated = update(editedHotel, {
      services: { $push: [{ type: "AIRCONDITIONER", informationText: "" }] },
    });
    setEditedHotel(updated);
  };

  const deleteService = (index) => {
    const updated = update(editedHotel, {
      services: { $splice: [[index, 1]] },
    });
    setEditedHotel(updated);
  };

  //contacts
  const addContact = () => {
    const updated = update(editedHotel, {
      contacts: { $push: [{ type: "VK", infoText: "", value: "" }] },
    });
    setEditedHotel(updated);
  };

  const deleteContact = (index) => {
    const updated = update(editedHotel, {
      contacts: { $splice: [[index, 1]] },
    });
    setEditedHotel(updated);
  };

  //modal
  const handleClose = () => setOpen(false);

  const handleOpenModalForAdd = () => {
    setHotelMode("add");
    setEditedHotel(editedHotelInitialState);
    setOpen(true);
  };

  const Modal = () => (
    <MuiModal
      open={open}
      onClose={handleClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box className="box_modal">
        <Typography id="modal-modal-title" variant="h9" component="h2">
          {hotelMode === "add" ? "Добавить" : "Отредактировать"} отель
        </Typography>
        <TextField
          sx={{ mr: 3 }}
          id="text_name"
          label="Название отеля"
          value={editedHotel?.name}
          onChange={(event) => {
            setEditedHotel({ ...editedHotel, name: event.target.value });
          }}
          variant="standard"
        />
        <TextField
          id="text_description"
          label="Описание отеля"
          value={editedHotel.description}
          multiline={true}
          onChange={(event) => {
            setEditedHotel({
              ...editedHotel,
              description: event.target.value,
            });
          }}
          variant="standard"
        />
        <ImageList>
          <ImageListItem key={editedHotel.id}>
            <img
              type="file"
              // src={`${imagesPath.val}/HOTEL/${editedHotel.id}/${
              //   editedHotel.image ?? defaultImage.val
              // }?w=248&fit=crop&auto=format`}
              // srcSet={`${imagesPath.val}/HOTEL/${editedHotel.id}/${
              //   editedHotel.image ?? defaultImage.val
              // }?w=248&fit=crop&auto=format&dpr=2 2x`}
              src={
                editedHotel?.image
                  ? `${imagesPath.val}/HOTEL/${editedHotel.id}/${editedHotel.image}?w=248&fit=crop&auto=format`
                  : `${imagesPath.val}/HOTEL/${defaultImage.val}?w=248&fit=crop&auto=format`
              }
              srcSet={
                editedHotel?.image
                  ? `${imagesPath.val}/HOTEL/${editedHotel.id}/${editedHotel.image}?w=248&fit=crop&auto=format&dpr=2 2x`
                  : `${imagesPath.val}/HOTEL/${defaultImage.val}?w=248&fit=crop&auto=format&dpr=2 2x`
              }
              alt={editedHotel.name}
              loading="lazy"
            />
            <ImageListItemBar
              title={editedHotel.name}
              actionIcon={
                <IconButton
                  sx={{ color: "rgba(255, 255, 255, 0.54)" }}
                  aria-label={`info about ${editedHotel.name}`}
                >
                  <DeleteIcon onClick={() => deleteHotelImage()} />
                </IconButton>
              }
            />
          </ImageListItem>
        </ImageList>
        <Button
          variant="contained"
          component="label"
          style={{ display: hotelMode === "add" ? "none" : "block" }}
        >
          Загрузить изображение отеля
          <input
            hidden
            accept="image/*"
            multiple
            type="file"
            onChange={(event) => onHotelFileUpload(event?.target?.files?.[0])}
          />
        </Button>
        <Stack spacing={2} pt={3}>
          <Typography id="modal-modal-title" variant="h6" component="h4">
            Комнаты:
          </Typography>
          {editedHotel?.rooms?.length > 0 && (
            <Box>
              {editedHotel?.rooms?.map((room, index) => (
                <FormControl
                  fullWidth
                  sx={{
                    mb: 3,
                    borderColor: "grey.500",
                    borderStyle: "dotted",
                    borderWidth: "0.1em",
                    borderRadius: "16px",
                    pt: 5,
                  }}
                >
                  <InputLabel id="demo-simple-select-label" sx={{ mt: 3 }}>
                    Количество человек:
                  </InputLabel>
                  <Select
                    sx={{ mb: 3 }}
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={room?.peopleQuantity}
                    label="Количество человек"
                    onChange={(event) => {
                      const updated = update(editedHotel, {
                        rooms: {
                          [index]: {
                            peopleQuantity: { $set: event.target.value },
                          },
                        },
                      });
                      setEditedHotel(updated);
                    }}
                  >
                    <MenuItem value={"1"}>1</MenuItem>
                    <MenuItem value={"2"}>2</MenuItem>
                    <MenuItem value={"3"}>3</MenuItem>
                    <MenuItem value={"4"}>4</MenuItem>
                    <MenuItem value={"5"}>5</MenuItem>
                    <MenuItem value={"6"}>6</MenuItem>
                  </Select>
                  <TextField
                    sx={{ mb: 3 }}
                    id="text_contact_info"
                    label="Цена"
                    value={room?.price}
                    onChange={(event) => {
                      const updated = update(editedHotel, {
                        rooms: {
                          [index]: { price: { $set: event.target.value } },
                        },
                      });
                      setEditedHotel(updated);
                    }}
                    variant="standard"
                  />
                  {room.images?.map((image, i) => (
                    <ImageList>
                      <ImageListItem key={image}>
                        <img
                          type="file"
                          // src={`${imagesPath.val}/ROOM/${room.id}/${
                          //   image ?? defaultRoomImage.val
                          // }?w=248&fit=crop&auto=format`}
                          // srcSet={`${imagesPath.val}/ROOM/${room.id}/${
                          //   image ?? defaultRoomImage.val
                          // }?w=248&fit=crop&auto=format&dpr=2 2x`}
                          src={
                            image
                              ? `${imagesPath.val}/ROOM/${room.id}/${image}?w=248&fit=crop&auto=format`
                              : `${imagesPath.val}/ROOM/${defaultRoomImage.val}?w=248&fit=crop&auto=format`
                          }
                          srcSet={
                            image
                              ? `${imagesPath.val}/ROOM/${room.id}/${image}?w=248&fit=crop&auto=format&dpr=2 2x`
                              : `${imagesPath.val}/ROOM/${defaultRoomImage.val}?w=248&fit=crop&auto=format&dpr=2 2x`
                          }
                          loading="lazy"
                        />
                        <ImageListItemBar
                          actionIcon={
                            <IconButton
                              sx={{ color: "rgba(255, 255, 255, 0.54)" }}
                              aria-label={`info about ${editedHotel.name}`}
                            >
                              <DeleteIcon
                                onClick={() => deleteRoomImage(index, i)}
                              />
                            </IconButton>
                          }
                        />
                      </ImageListItem>
                    </ImageList>
                  ))}
                  <Button
                    variant="contained"
                    component="label"
                    style={{ display: !room.id ? "none" : "block" }}
                  >
                    Загрузить изображения комнаты
                    <input
                      hidden
                      accept="image/*"
                      multiple
                      type="file"
                      onChange={(event) =>
                        onRoomFileUpload(event?.target?.files?.[0], index)
                      }
                    />
                  </Button>
                  <Button
                    endIcon={<DeleteIcon />}
                    onClick={() => deleteRoom(index)}
                  >
                    Удалить комнату
                  </Button>
                </FormControl>
              ))}
            </Box>
          )}
          <Button
            sx={{ mt: 0 }}
            endIcon={<AddIcon />}
            onClick={() => addRoom()}
          >
            Добавить комнату
          </Button>
        </Stack>
        <Stack spacing={2} pt={3}>
          <Typography id="modal-modal-title" variant="h6" component="h4">
            Услуги:
          </Typography>
          {editedHotel?.services?.length > 0 && (
            <Box>
              {editedHotel?.services?.map((service, index) => (
                <FormControl
                  fullWidth
                  sx={{
                    mb: 3,
                    borderColor: "grey.500",
                    borderStyle: "dotted",
                    borderWidth: "0.1em",
                    borderRadius: "16px",
                    pt: 5,
                  }}
                >
                  <InputLabel id="demo-simple-select-label" sx={{ mt: 3 }}>
                    Тип:
                  </InputLabel>
                  <Select
                    sx={{ mb: 3 }}
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={service?.type}
                    label="Тип"
                    onChange={(event) => {
                      const updated = update(editedHotel, {
                        services: {
                          [index]: {
                            type: { $set: event.target.value },
                          },
                        },
                      });
                      setEditedHotel(updated);
                    }}
                  >
                    <MenuItem value={"AIRCONDITIONER"}>AIRCONDITIONER</MenuItem>
                    <MenuItem value={"PARKING"}>PARKING</MenuItem>
                    <MenuItem value={"POOL"}>POOL</MenuItem>
                    <MenuItem value={"WIFI"}>WIFI</MenuItem>
                    <MenuItem value={"BAR"}>BAR</MenuItem>
                    <MenuItem value={"KITCHEN"}>KITCHEN</MenuItem>
                  </Select>
                  <TextField
                    sx={{ mb: 3 }}
                    id="text_contact_info"
                    label="Информация"
                    value={service?.informationText}
                    onChange={(event) => {
                      const updated = update(editedHotel, {
                        services: {
                          [index]: {
                            priinformationTextce: {
                              $set: event.target.value,
                            },
                          },
                        },
                      });
                      setEditedHotel(updated);
                    }}
                    variant="standard"
                  />
                  <Button
                    endIcon={<DeleteIcon />}
                    onClick={() => deleteService(index)}
                  >
                    Удалить услугу
                  </Button>
                </FormControl>
              ))}
            </Box>
          )}
          <Button
            sx={{ mt: 0 }}
            endIcon={<AddIcon />}
            onClick={() => addService()}
          >
            Добавить услугу
          </Button>
        </Stack>
        <Stack spacing={2} pt={3}>
          <Typography
            mb={-2}
            id="modal-modal-title"
            variant="h6"
            component="h4"
          >
            Адрес:
          </Typography>
          <TextField
            id="text_address_geoposition"
            label="Геопозиция"
            value={editedHotel?.address?.geoposition}
            onChange={(event) => {
              setEditedHotel({
                ...editedHotel,
                address: {
                  ...editedHotel.address,
                  geoposition: event.target.value,
                },
              });
            }}
            variant="standard"
          />
          <TextField
            id="text_address_countryName"
            label="Страна"
            value={editedHotel?.address?.countryName}
            onChange={(event) => {
              setEditedHotel({
                ...editedHotel,
                address: {
                  ...editedHotel.address,
                  countryName: event.target.value,
                },
              });
            }}
            variant="standard"
          />
          <TextField
            id="text_address_city"
            label="Город"
            value={editedHotel?.address?.city}
            onChange={(event) => {
              setEditedHotel({
                ...editedHotel,
                address: {
                  ...editedHotel.address,
                  city: event.target.value,
                },
              });
            }}
            variant="standard"
          />
          <TextField
            id="text_address_region"
            label="Район"
            value={editedHotel?.address?.region}
            onChange={(event) => {
              setEditedHotel({
                ...editedHotel,
                address: {
                  ...editedHotel.address,
                  region: event.target.value,
                },
              });
            }}
            variant="standard"
          />
          <TextField
            id="text_address_postalCode"
            label="Почтовый код"
            value={editedHotel?.address?.postalCode}
            onChange={(event) => {
              setEditedHotel({
                ...editedHotel,
                address: {
                  ...editedHotel.address,
                  postalCode: event.target.value,
                },
              });
            }}
            variant="standard"
          />
          <TextField
            id="text_address_countryCode"
            label="Код страны"
            value={editedHotel?.address?.countryCode}
            onChange={(event) => {
              setEditedHotel({
                ...editedHotel,
                address: {
                  ...editedHotel.address,
                  countryCode: event.target.value,
                },
              });
            }}
            variant="standard"
          />
          <TextField
            id="text_address_street"
            label="Улица"
            value={editedHotel?.address?.street}
            onChange={(event) => {
              setEditedHotel({
                ...editedHotel,
                address: {
                  ...editedHotel.address,
                  street: event.target.value,
                },
              });
            }}
            variant="standard"
          />
          <TextField
            id="text_address_building"
            label="Номер дома"
            value={editedHotel?.address?.building}
            onChange={(event) => {
              setEditedHotel({
                ...editedHotel,
                address: {
                  ...editedHotel.address,
                  building: event.target.value,
                },
              });
            }}
            variant="standard"
          />
        </Stack>
        <Stack spacing={2} pt={3}>
          <Typography id="modal-modal-title" variant="h6" component="h4">
            Контакты:
          </Typography>
          {editedHotel?.contacts?.length > 0 && (
            <Box>
              {editedHotel?.contacts?.map((contact, index) => (
                <FormControl
                  fullWidth
                  sx={{
                    mb: 3,
                    borderColor: "grey.500",
                    borderStyle: "dotted",
                    borderWidth: "0.1em",
                    borderRadius: "16px",
                    pt: 5,
                  }}
                >
                  <InputLabel id="demo-simple-select-label" sx={{ mt: 3 }}>
                    Тип
                  </InputLabel>
                  <Select
                    sx={{ mb: 3 }}
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={contact?.type}
                    label="Тип"
                    onChange={(event) => {
                      const updated = update(editedHotel, {
                        contacts: {
                          [index]: { type: { $set: event.target.value } },
                        },
                      });
                      setEditedHotel(updated);
                    }}
                  >
                    <MenuItem value={"VK"}>VK</MenuItem>
                    <MenuItem value={"PHONE"}>PHONE</MenuItem>
                    <MenuItem value={"EMAIL"}>EMAIL</MenuItem>
                    <MenuItem value={"FAX"}>FAX</MenuItem>
                    <MenuItem value={"WEBSITE"}>WEBSITE</MenuItem>
                    <MenuItem value={"TELEGRAM"}>TELEGRAM</MenuItem>
                    <MenuItem value={"WHATSUPP"}>WHATSUPP</MenuItem>
                    <MenuItem value={"FB"}>FB</MenuItem>
                  </Select>
                  <TextField
                    sx={{ mb: 3 }}
                    id="text_contact_info"
                    label="Информация"
                    value={contact?.infoText}
                    multiline={true}
                    onChange={(event) => {
                      const updated = update(editedHotel, {
                        contacts: {
                          [index]: { infoText: { $set: event.target.value } },
                        },
                      });
                      setEditedHotel(updated);
                    }}
                    variant="standard"
                  />
                  <TextField
                    sx={{ mb: 5 }}
                    id="text_contact_value"
                    label="Контактные данные"
                    value={contact?.value}
                    multiline={true}
                    onChange={(event) => {
                      const updated = update(editedHotel, {
                        contacts: {
                          [index]: { value: { $set: event.target.value } },
                        },
                      });
                      setEditedHotel(updated);
                    }}
                    variant="standard"
                  />
                  <Button
                    endIcon={<DeleteIcon />}
                    onClick={() => deleteContact(index)}
                  >
                    Удалить контакт
                  </Button>
                </FormControl>
              ))}
            </Box>
          )}
          <Button
            sx={{ mt: 0 }}
            endIcon={<AddIcon />}
            onClick={() => addContact()}
          >
            Добавить контакт
          </Button>
        </Stack>
        <Button sx={{ mt: 5 }} variant="contained" onClick={() => saveHotel()}>
          Сохранить
        </Button>
      </Box>
    </MuiModal>
  );

  //result
  return (
    <>
      {managerHotels.length > 0 && (
        <ImageList>
          {managerHotels.map((hotel, index) => (
            <ImageListItem key={hotel.id}>
              <img
                type="file"
                // src={`${imagesPath.val}/HOTEL/${hotel.id}/${
                //   hotel.image ?? defaultImage.val
                // }?w=248&fit=crop&auto=format`}
                // srcSet={`${imagesPath.val}/HOTEL/${hotel.id}/${
                //   hotel.image ?? defaultImage.val
                // }?w=248&fit=crop&auto=format&dpr=2 2x`}
                src={
                  hotel?.image
                    ? `${imagesPath.val}/HOTEL/${hotel.id}/${hotel.image}?w=248&fit=crop&auto=format`
                    : `${imagesPath.val}/HOTEL/${defaultRoomImage.val}?w=248&fit=crop&auto=format`
                }
                srcSet={
                  hotel?.image
                    ? `${imagesPath.val}/HOTEL/${hotel.id}/${hotel.image}?w=248&fit=crop&auto=format&dpr=2 2x`
                    : `${imagesPath.val}/HOTEL/${defaultRoomImage.val}?w=248&fit=crop&auto=format&dpr=2 2x`
                }
                alt={hotel.name}
                loading="lazy"
              />
              <ImageListItemBar
                title={hotel.name}
                subtitle={hotel.description}
                actionIcon={
                  <IconButton
                    sx={{ color: "rgba(255, 255, 255, 0.54)" }}
                    aria-label={`info about ${hotel.name}`}
                  >
                    <EditIcon
                      onClick={() => {
                        handleOpenModalForEdit(hotel.id);
                      }}
                    />
                    <DeleteIcon onClick={() => deleteHotel(index)} />
                  </IconButton>
                }
              />
            </ImageListItem>
          ))}
        </ImageList>
      )}
      <Button endIcon={<AddIcon />} onClick={() => handleOpenModalForAdd()}>
        Добавить отель
      </Button>
      <Modal />
    </>
  );
}
