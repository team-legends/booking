import React from "react";
import { useEffect, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";

export default function UserOrdersPage() {
  const navigate = useNavigate();
  const [orders, setOrders] = useState([]);

  const token = localStorage.getItem("token");
  const userId = localStorage.getItem("userId");

  useEffect(() => {
    GetOrdersByUserId();
  }, []);

  const GetOrdersByUserId = () => {
    fetch(`/api/Order/GetOrdersByUserId/${userId}`, {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((res) => {
        console.log(res?.items);
        setOrders(res?.items);
      })
      .catch((err) => {
        console.error(err);
      });
  };

  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Название отеля</TableCell>
            <TableCell align="right">Кол-во человек</TableCell>
            <TableCell align="right">Цена</TableCell>
            <TableCell align="right">Статус</TableCell>
            <TableCell align="right">Дата</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {orders?.map((order) => (
            <TableRow
              key={order.orderId}
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              <TableCell component="th" scope="row">
                {order.hotel?.name}
              </TableCell>
              <TableCell align="right">{order.room?.peopleQuantity}</TableCell>
              <TableCell align="right">{order.room?.price}</TableCell>
              <TableCell align="right">{order.status}</TableCell>
              <TableCell align="right">{order.orderDate}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
