import "./home.css";
import {
  faBed,
  faCalendarDays,
  faPerson,
} from "@fortawesome/free-solid-svg-icons";
import React from "react";
import { useNavigate } from "react-router-dom";
import { DateRange } from "react-date-range";
import { useContext, useState } from "react";
import { format } from "date-fns";
import { FilterContext } from "../../App";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { ConvertToDate } from "../../utils/dt";

const HomePage = () => {
  const {
    destination,
    setDestination,
    date,
    setDate,
    options,
    setOptions,
    hotels,
    setHotels,
  } = useContext(FilterContext);
  const [openDate, setOpenDate] = useState(false);
  const [openOptions, setOpenOptions] = useState(false);
  const navigate = useNavigate();
  const handleOption = (name, operation) => {
    setOptions((prev) => {
      return {
        ...prev,
        [name]: operation === "i" ? options[name] + 1 : options[name] - 1,
      };
    });
  };

  const handleSearch = async () => {
    await fetch(`/api/Scheduler/GetFilteredHotels`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
      },
      body: JSON.stringify({
        Destination: destination,
        DateFrom: ConvertToDate(date[0].startDate),
        DateTo: ConvertToDate(date[0].endDate),
        Option: {
          Adult: options.adult,
          Children: options.children,
          Room: options.room,
        },
      }),
    })
      .then((res) => {
        return res.json();
      })
      .then((res) => {
        console.log("res_home", res);
        return setHotels(res);
      });
    navigate("/hotels");
  };

  return (
    <div className="home">
      <div className="homeSearch">
        <div className="homeSearchItem">
          <FontAwesomeIcon icon={faBed} className="homeIcon" />
          <input
            type="text"
            placeholder="Where are you going?"
            className="homeSearchInput"
            onChange={(e) => setDestination(e.target.value)}
          />
        </div>
        <div className="homeSearchItem">
          <FontAwesomeIcon icon={faCalendarDays} className="homeIcon" />
          <span
            onClick={() => setOpenDate(!openDate)}
            className="homeSearchText"
          >{`${format(date[0].startDate, "MM/dd/yyyy")} to ${format(
            date[0].endDate,
            "MM/dd/yyyy"
          )}`}</span>
          {openDate && (
            <DateRange
              editableDateInputs={true}
              onChange={(item) => setDate([item.selection])}
              moveRangeOnFirstSelection={false}
              ranges={date}
              className="date"
              minDate={new Date()}
            />
          )}
        </div>
        <div className="homeSearchItem">
          <FontAwesomeIcon icon={faPerson} className="homeIcon" />
          <span
            onClick={() => setOpenOptions(!openOptions)}
            className="homeSearchText"
          >{`${options.adult} adult · ${options.children} children · ${options.room} room`}</span>
          {openOptions && (
            <div className="options">
              <div className="optionItem">
                <span className="optionText">Adult</span>
                <div className="optionCounter">
                  <button
                    disabled={options.adult <= 1}
                    className="optionCounterButton"
                    onClick={() => handleOption("adult", "d")}
                  >
                    -
                  </button>
                  <span className="optionCounterNumber">{options.adult}</span>
                  <button
                    className="optionCounterButton"
                    onClick={() => handleOption("adult", "i")}
                  >
                    +
                  </button>
                </div>
              </div>
              <div className="optionItem">
                <span className="optionText">Children</span>
                <div className="optionCounter">
                  <button
                    disabled={options.children <= 0}
                    className="optionCounterButton"
                    onClick={() => handleOption("children", "d")}
                  >
                    -
                  </button>
                  <span className="optionCounterNumber">
                    {options.children}
                  </span>
                  <button
                    className="optionCounterButton"
                    onClick={() => handleOption("children", "i")}
                  >
                    +
                  </button>
                </div>
              </div>
              <div className="optionItem">
                <span className="optionText">Room</span>
                <div className="optionCounter">
                  <button
                    disabled={options.room <= 1}
                    className="optionCounterButton"
                    onClick={() => handleOption("room", "d")}
                  >
                    -
                  </button>
                  <span className="optionCounterNumber">{options.room}</span>
                  <button
                    className="optionCounterButton"
                    onClick={() => handleOption("room", "i")}
                  >
                    +
                  </button>
                </div>
              </div>
            </div>
          )}
        </div>
        <div className="homeSearchItem">
          <button
            disabled={
              !date[0].startDate ||
              !date[0].endDate ||
              !destination ||
              options.adult == 0
            }
            className="homeBtn"
            onClick={handleSearch}
          >
            Search
          </button>
        </div>
      </div>
    </div>
  );
};

export default HomePage;
