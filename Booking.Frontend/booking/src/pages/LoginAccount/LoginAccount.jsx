import React from "react";
import { useContext, useState } from "react";
import { UserContext } from "../../App";
import { useNavigate } from "react-router-dom";
import "font-awesome/css/font-awesome.min.css";
import Checkbox from "@mui/material/Checkbox";
import FormGroup from "@mui/material/FormGroup";
import FormControlLabel from "@mui/material/FormControlLabel";

const validationErrorInitialState = {
  login: "",
  password: "",
};

export default function LoginAccount() {
  const { login, setLogin, password, setPassword, userId, setUserId } =
    useContext(UserContext);
  const navigate = useNavigate();
  const [apiError, setApiError] = useState("");
  const [isChecked, setIsChecked] = useState(false);
  const [validationError, setValidationError] = useState({
    ...validationErrorInitialState,
  });
  const handleCheckBox = () => {
    setIsChecked((current) => !current);
  };
  const handleChangeLogin = (event) => {
    setLogin(event.target.value);
    setApiError("");
  };
  const handleChangePassword = (event) => {
    setPassword(event.target.value);
    setApiError("");
  };

  const checkEmpty = (str) => !!str.trim();

  const validate = () => {
    let err = validationErrorInitialState;

    if (!checkEmpty(login)) {
      err.login = "Поле Login должно быть заполнено";
    }

    if (!checkEmpty(password)) {
      err.password = "Поле Password должно быть заполнено";
    }
    setValidationError(err);
    return err;
  };

  const handleLogin = (event) => {
    event.preventDefault();
    let errLocal = validate();

    if (errLocal.login || errLocal.password || apiError) {
      return;
    }

    fetch(`/api/Auth/Login`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
      },
      body: JSON.stringify({
        Login: login,
        Password: password,
        IsManager: isChecked,
      }),
    })
      .then((res) => res.json())
      .then((res) => {
        localStorage.setItem("token", res.token);
        setUserId(res.id);
        localStorage.setItem("userId", res.id);
        if (isChecked) navigate("/managehotels");
        else navigate("/");
      })
      .catch(() => {
        setApiError("Что-то пошло не так. Попробуйте позже.");
      });
  };

  const error = <div className="error">{apiError}</div>;
  return (
    <div className="form_wrapper">
      <div className="form_container">
        <div className="title_container">
          <h2>Login</h2>
        </div>
        <div className="row clearfix">
          <div className="">
            <form>
              <div className="input_field">
                {" "}
                <span>
                  <i aria-hidden="true" className="fa fa-user"></i>
                </span>
                <input
                  type="login"
                  name="login"
                  placeholder="Login"
                  required
                  onChange={handleChangeLogin}
                />
                <div className="error">{validationError.login}</div>
              </div>
              <div className="input_field">
                {" "}
                <span>
                  <i aria-hidden="true" className="fa fa-lock"></i>
                </span>
                <input
                  type="password"
                  name="password"
                  placeholder="Password"
                  required
                  onChange={handleChangePassword}
                />
                <div className="error">{validationError.password}</div>
              </div>
              <FormGroup>
                <FormControlLabel
                  sx={{ fontSize: "120%" }}
                  control={
                    <Checkbox checked={isChecked} onChange={handleCheckBox} />
                  }
                  label="IsManager"
                />
              </FormGroup>
              <input
                className="button"
                type="submit"
                value="Login"
                onClick={handleLogin}
              />
              {error}
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}
