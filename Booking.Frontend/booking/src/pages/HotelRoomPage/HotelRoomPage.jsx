import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLocationDot } from "@fortawesome/free-solid-svg-icons";
import { useState, useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import Box from "@mui/material/Box";
import {
  imagesPath,
  defaultImage,
  defaultRoomImage,
} from "../../constants/paths";
import InputLabel from "@mui/material/InputLabel";
import { Stack } from "@mui/system";

const HotelRoomPage = () => {
  const navigate = useNavigate();
  const [hotel, setHotel] = useState(null);
  let { id } = useParams();

  useEffect(() => {
    fetch(`/api/Scheduler/GetHotelInfo/${id}`, {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
      },
    })
      .then((res) => res.json())
      .then((res) => setHotel(res));
  }, []);

  const handleMakeOrder = (roomId) => {
    navigate(`/order/${id}/${roomId}`);
  };

  return (
    <div>
      <div className="hotelContainer">
        <div className="hotelWrapper">
          <div className="hotelDetailsTexts">
            <h1 className="hotelTitle">{hotel?.name}</h1>
            <p className="hotelDesc">{hotel?.description}</p>
          </div>
          <div className="hotelAddress">
            <FontAwesomeIcon icon={faLocationDot} />
            <span>
              {hotel?.address?.countryName} {hotel?.address?.city}{" "}
              {hotel?.address?.street} {hotel?.address?.building}
            </span>
          </div>
          <div className="hotelImages">
            <div className="hotelImgWrapper" key={hotel?.id}>
              <img
                // src={`${imagesPath.val}/${
                //   hotel?.image ?? defaultImage.val
                // }?w=600&fit=crop&auto=format`}
                src={
                  hotel?.image
                    ? `${imagesPath.val}/HOTEL/${hotel.id}/${hotel?.image}?w=600&fit=crop&auto=format`
                    : `${imagesPath.val}/HOTEL/${defaultImage.val}?w=600&fit=crop&auto=format`
                }
                alt=""
                className="hotelImg"
              />
            </div>
          </div>
          <div className="hotelDetails">
            {hotel?.rooms?.length > 0 && (
              <Stack>
                <h1 className="hotelTitle">Rooms</h1>
                {hotel?.rooms?.map((room, index) => (
                  <div className="hotelImgWrapper" key={room.id}>
                    <Box
                      sx={{
                        display: "flex",
                        flexDirection: "row",
                        p: 1,
                        m: 1,
                        bgcolor: "background.paper",
                        borderRadius: 1,
                        width: "auto",
                      }}
                    >
                      <InputLabel id="demo-simple-select-label" sx={{ mt: 3 }}>
                        Room {index + 1} for {room?.peopleQuantity} people
                      </InputLabel>
                      <button
                        className="bookNow"
                        key={room.id}
                        onClick={() => handleMakeOrder(room.id)}
                      >
                        Reserve or Book Now!
                      </button>
                    </Box>
                    <Box
                      sx={{
                        display: "flex",
                        flexDirection: "row",
                        p: 1,
                        m: 1,
                        bgcolor: "background.paper",
                        borderRadius: 1,
                        width: "auto",
                      }}
                    >
                      {room?.images?.length > 0 &&
                        room.images.map((image, i) => (
                          <img
                            //src={`${imagesPath.val}/${
                            //   image ?? defaultRoomImage.val
                            // }`}
                            src={
                              image
                                ? `${imagesPath.val}/ROOM/${room.id}/${image}?w=248&fit=crop&auto=format`
                                : `${imagesPath.val}/ROOM/${defaultRoomImage.val}?w=248&fit=crop&auto=format`
                            }
                            alt=""
                            className="roomImg"
                          />
                        ))}
                      {!room?.images?.length && (
                        <img
                          //src={`${imagesPath.val}/${defaultRoomImage.val}`}
                          src={`${imagesPath.val}/ROOM/${defaultRoomImage.val}?w=248&fit=crop&auto=format`}
                          alt=""
                          className="roomImg"
                        />
                      )}
                    </Box>
                  </div>
                ))}
              </Stack>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};
export default HotelRoomPage;
