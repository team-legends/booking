import Box from "@mui/material/Box";
import Link from "@mui/material/Link";
import Header from "./Header/Header";
import Typography from "@mui/material/Typography";
import CssBaseline from "@mui/material/CssBaseline";
import useMediaQuery from "@mui/material/useMediaQuery";
import { Outlet } from "react-router-dom";
import { backgroundColorBase } from "../../constants/colors";
import { useTheme } from "@mui/material/styles";
import { React } from "react";

const Copyright = (props) => {
	const year = new Date().getFullYear();

	return (
		<>
			<Typography
				variant="body2"
				component={"div"}
				color="text.secondary"
				align="center"
				{...props}
			>
				{"Copyright © "}{" "}
				<Link color="inherit" href="https://www.booking.com/">
					Booking
				</Link>{" "}
				{year === 2023 ? 2023 : `2023 - ${year}`}
			</Typography>
		</>
	);
};

const LayoutPaperbase = () => {
	const theme = useTheme();

	const isSmall = useMediaQuery(theme.breakpoints.down("md"));
	const isMedium = useMediaQuery(theme.breakpoints.down("lg"));

	return (
		<Box sx={{ display: "flex", minHeight: "100vh" }}>
			<CssBaseline />
			<Box
				sx={{
					flex: 1,
					display: "flex",
					flexDirection: "column",
					minHeight: "100%",
				}}
			>
				<Box
					sx={{
						display: "flex",
						flexDirection: "column",
						minHeight: "100%",
						justifyContent: "space-between",
						bgcolor: backgroundColorBase.main,
					}}
				>
					<Box sx={{ display: "flex", flexDirection: "column" }}>
						<Header />
						<Box
							component="main"
							sx={{
								flex: 1,
								py: isSmall || isMedium ? 2 : 4,
								px: isSmall ? 1 : isMedium ? 2 : 4,
							}}
						>
							<Outlet />
						</Box>
					</Box>
					<Box component="footer">
						<Copyright sx={{ mt: 8, mb: 4 }} />
					</Box>
				</Box>
			</Box>
		</Box>
	);
};

export default LayoutPaperbase;
