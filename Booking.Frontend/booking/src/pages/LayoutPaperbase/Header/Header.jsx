import { faBed } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "./header.css";
import "react-date-range/dist/styles.css"; // main css file
import "react-date-range/dist/theme/default.css"; // theme css file
import Navbar from "../../../components/Navbar/Navbar";
import { useNavigate } from "react-router-dom";


const Header = ({ type }) => {
  const navigate = useNavigate();
  const navigateToStart = () => {
    navigate("/");
  };
  return (
    <>
    <Navbar/>
    <div className="header">
      <div
        className={
          type === "list" ? "headerContainer listMode" : "headerContainer"
        }
      >
        <div className="headerList">
          <div className="headerListItem active" onClick={navigateToStart}>
            <FontAwesomeIcon icon={faBed} />
            <span>Stays</span>
          </div>
        </div>
        {type !== "list" && (
          <>
            <h1 className="headerTitle">
            Find your next stay.
            </h1>
            <p className="headerDesc">
            Search low prices on hotels, homes and much more...
            </p>          
          </>
        )}
      </div>
    </div>
    </>
  );
};

export default Header;
