﻿namespace Booking.Backend.StaticData
{
    public static class RandomExtensions
    {
        static Random rnd = new Random();

        /// <summary>
        /// Returns 1 random item from the collection.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static T PickRandom<T>(this IList<T> source)
        {
            int randIndex = rnd.Next(source.Count);
            return source[randIndex];
        }

        /// <summary>
        /// Returns random items from the collection.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">Collection.</param>
        /// <param name="count">Count of items.</param>
        /// <returns></returns>
        public static IEnumerable<T> PickRandom<T>(this IList<T> source, int count)
        {
            return source.OrderBy(x => rnd.Next()).Take(count);
        }
    }
}
