﻿using System.Collections.Immutable;

namespace Booking.Backend.StaticData;

public static class Addresses
{
    public static readonly string RusCountry = "Российская Федерация";

    public static readonly ImmutableArray<string> Countries = ImmutableArray.Create
        (
         "CША",
         "Тайланд",
         "Египет",
         "Испания",
         "Италия"
        );

    public static readonly ImmutableArray<string> RusCities = ImmutableArray.Create
            (
             "Москва",
             "Санкт-Петербург",
             "Казань",
             "Сочи",
             "Анапа",
             "Туапсе",
             "Минеральные Воды",
             "Владивосток",
             "Иркутск",
             "Мурманск",
             "Дербент",
             "Самара"
            );

    public static readonly ImmutableArray<string> Cities = ImmutableArray.Create
        (
         "Эльхета",
         "Сан-Ферран",
         "Сантс-Монжуик",
         "Калелья",
         "Бадалона",
         "Буэнья",
         "Агуатон",
         "Архенте",
         "Лидон",
         "Панкрудо",
         "Фарафра",
         "Сен-Ло"
        );

    public static readonly ImmutableArray<string> RusStreets = ImmutableArray.Create
    (
    "Ленина",
    "Невский",
    "Парашютная",
    "Лиговский",
    "Достоевского",
    "Коломенская",
    "Магнитогорская",
    "Заневский",
    "Ленская",
    "Зеленая",
    "Науки",
    "Ушинского",
    "Воронцовский",
    "Художников",
    "Мариинский",
    "Сибирская"
    );

    public static readonly ImmutableArray<string> Streets = ImmutableArray.Create
    (
    "Бур",
    "Ла Бют",
    "Авеню",
    "Лиговский",
    "Андре Кото",
    "Пассаж",
    "Мориниере",
    "Коссен",
    "Суш",
    "Хартли",
    "Вудпарк",
    "Джефф Адамс",
    "Хайленд",
    "Трейлер Драйв",
    "Кенмонт",
    "Нортхейвен",
    "Колман",
    "Спринг",
    "Окорн-Окс",
    "Блейз-Манор",
    "Сатервит",
    "Роки-ривер",
    "Вандора",
    "Арбор-Дейл",
    "Гринвуд",
    "Бранч",
    "Миллен"
    );

}
