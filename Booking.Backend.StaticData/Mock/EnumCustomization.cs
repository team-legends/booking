﻿using AutoFixture.Kernel;
using System.Reflection;

namespace Booking.Backend.StaticData.Mock
{
    //
    // Summary:
    //     Кастомизация AutoFixture для enum.
    public sealed class EnumCustomization : ISpecimenBuilder
    {
        private static readonly Random Random = new Random();

        private readonly string[] _valuesToExclude;

        //
        // Summary:
        //     Инициализирует новый экземпляр.
        //
        // Parameters:
        //   valuesToExclude:
        //     Значения, которые будут исключены из случайной генерации.
        public EnumCustomization(string[] valuesToExclude)
        {
            _valuesToExclude = valuesToExclude;
        }

        public object Create(object request, ISpecimenContext context)
        {
            Type type = (request as Type) ?? (request as PropertyInfo)?.PropertyType ?? (request as ParameterInfo)?.ParameterType;
            if (type == null || !type.IsEnum)
            {
                return new NoSpecimen();
            }

            return GetFilteredEnumValueObject(type);
        }

        private object GetFilteredEnumValueObject(Type type)
        {
            Array values = Enum.GetValues(type);
            (string, int)[] array = (from _ in Enum.GetNames(type).Select((string name, int index) => (name, index))
                                     where _valuesToExclude.All((string value) => value != _.name)
                                     select _).ToArray();
            int num = Random.Next(0, array.Length);
            int item = array[num].Item2;
            return values.GetValue(item);
        }
    }
}
