﻿using AutoFixture.Kernel;
using System.Reflection;

namespace Booking.Backend.StaticData.Mock
{
    //
    // Summary:
    //     Кастомизация AutoFixture для типа T.
    //
    // Type parameters:
    //   T:
    //     Тип, для которого выполняется кастомизация.
    public abstract class TypeCustomization<T> : ISpecimenBuilder where T : notnull
    {
        public object Create(object request, ISpecimenContext context)
        {
            if (((request as Type) ?? (request as PropertyInfo)?.PropertyType ?? (request as ParameterInfo)?.ParameterType) != typeof(T))
            {
                return new NoSpecimen();
            }

            return CreateInstance(context);
        }

        //
        // Summary:
        //     Создает случайное значение типа T.
        //
        // Parameters:
        //   context:
        //     Контекст AutoFixture.
        //
        // Returns:
        //     Случайное значение типа T.
        protected abstract T CreateInstance(ISpecimenContext context);
    }
}
