﻿using AutoFixture.Kernel;
using AutoFixture;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Backend.StaticData.Mock
{
    //
    // Summary:
    //     Кастомизация AutoFixture для типа System.DateTime.
    public sealed class DateTimeCustomization : ISpecimenBuilder
    {
        private readonly ISpecimenBuilder _innerGenerator = new RandomDateTimeSequenceGenerator();

        public object Create(object request, ISpecimenContext context)
        {
            object obj = _innerGenerator.Create(request, context);
            if (!(obj is NoSpecimen))
            {
                return TruncateMilliseconds((DateTime)obj).ToUniversalTime();
            }

            return obj;
        }

        private static DateTime TruncateMilliseconds(DateTime dateTime)
        {
            return dateTime.AddTicks(-(dateTime.Ticks % 10000000));
        }
    }
}
