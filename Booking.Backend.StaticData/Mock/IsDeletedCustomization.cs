﻿using AutoFixture.Kernel;
using System.Reflection;

namespace Booking.Backend.StaticData.Mock
{
    //
    // Summary:
    //     Кастомизация AutoFixture для Samolet.Dp.Common.Domain.ILogicalDeletable.IsDeleted.
    public sealed class IsDeletedCustomization : ISpecimenBuilder
    {
        public object Create(object request, ISpecimenContext context)
        {
            PropertyInfo propertyInfo = request as PropertyInfo;
            if ((object)propertyInfo != null && propertyInfo.PropertyType == typeof(bool) && propertyInfo.Name == "IsDeleted")
            {
                return false;
            }

            return new NoSpecimen();
        }
    }
}
