﻿using AutoFixture;
using Booking.Backend.StaticData.Mock;

public static class AutoFixtureFactory
{
    private static readonly string[] EnumValuesToExclude =
    {
        "Default"
    };

    public static Fixture Create()
    {
        var fixture = new Fixture();

        fixture.Customizations.Add(new DateTimeCustomization());
        fixture.Customizations.Add(new IsDeletedCustomization());
        fixture.Customizations.Add(new EnumCustomization(EnumValuesToExclude));

        fixture.Behaviors.OfType<ThrowingRecursionBehavior>().ToList().ForEach(b => fixture.Behaviors.Remove(b));
        fixture.Behaviors.Add(new OmitOnRecursionBehavior());

        return fixture;
    }
}
