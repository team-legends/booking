﻿using AutoMapper;
using Booking.Backend.Reservations.Contracts;
using Booking.Backend.Reservations.Contracts.Enums;
using Booking.Backend.ReservationsWebApi.Controllers;
using Booking.Backend.ReservationsWebApi.Models.Order.CRUDModel;
using Booking.Backend.ReservationsWebApi.Repository;
using Booking.Backend.ReservationsWebApi.Repository.Entities;
using Booking.Backend.ReservationsWebApi.Services;
using FluentAssertions;
using FluentAssertions.Execution;
using Microsoft.AspNetCore.Mvc;
using Moq;

namespace Booking.Backend.ReservationsWebApi.Test
{
    public class ReservationsControllerTest
    {
        [Fact]
        public async Task RequestForListOfOrders_OkObjectResultWithListOfOrderReturned()
        {
            // arrange
            var filterFor2023 = new FilterOrderModel()
            {
                DateFrom = new DateTime(2023, 1, 1),
                DateTo = new DateTime(2023, 12, 31),
                Status = Status.APPROVED
            };
            var expectedOrderList = GetTestOrder();
            var repositoryMock = new Mock<IOrderRepository>();
            var mockMapperConfiguration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new ApplicationMappingProfile());
            });
            var mapperMock = mockMapperConfiguration.CreateMapper();
            repositoryMock.Setup(x => x.GetByFilter(filterFor2023)).Returns((Task<IEnumerable<OrderEntity>>)expectedOrderList);

            var orderService = new OrderService(repositoryMock.Object, mapperMock);

            // act
            var controller = new ReservationsController(orderService);

            var result = await controller.GetOrdersByFilter(filterFor2023);
            var okResult = result as OkObjectResult;
            var actualOrderList = okResult.Value as IEnumerable<OrderInfo>;

            // assert
            okResult.Should().NotBeNull("Should return OK response");
            using (new AssertionScope())
            {
                actualOrderList.Should().HaveSameCount(expectedOrderList);
                foreach (var expectedlOrder in expectedOrderList)
                {
                    actualOrderList.Should().Contain(actualOrder =>
                        actualOrder.Price == expectedlOrder.Price &&
                        actualOrder.IsDelete == expectedlOrder.IsDelete &&
                        actualOrder.Status == expectedlOrder.Status &&
                        actualOrder.Count == expectedlOrder.Count &&
                        actualOrder.HotelId == expectedlOrder.HotelId &&
                        actualOrder.RoomId == expectedlOrder.RoomId &&
                        actualOrder.DateFrom == expectedlOrder.DateFrom &&
                        actualOrder.DateTo == expectedlOrder.DateTo);
                }
            }
        }

        private IEnumerable<OrderEntity> GetTestOrder()
        {
            return new List<OrderEntity>()
            {
                new OrderEntity()
                {
                    Id = new Guid("17ae273b-693b-4283-8fc4-22e99e30dd18"),
                    RoomId = new Guid("96f36359-60ce-4705-b678-8a40a19a4ec8"),
                    Count = 4,
                    Price = 100,
                    DateFrom = new DateTime(2023, 1, 5),
                    DateTo = new DateTime(2023, 1, 13),
                    Status =  Status.APPROVED
                },
                new OrderEntity()
                {
                    Id = new Guid("03ed6e43-56df-460f-844b-b9b12ae360b9"),
                    RoomId = new Guid("69a4d3fc-ebc8-4878-8955-e4570c7a900a"),
                    Count = 3,
                    Price = 120,
                    DateFrom = new DateTime(2022, 4, 2),
                    DateTo = new DateTime(2022, 5, 3),
                    Status =Status.APPROVED
                },
                new OrderEntity()
                {
                    Id = new Guid("85723236-ab2a-4a16-97f7-78b9f7c694f0"),
                    RoomId = new Guid("ae8ff82d-6f14-455d-8a48-690798bbf6d9"),
                    Count = 1,
                    Price = 80,
                    DateFrom = new DateTime(2023, 2, 3),
                    DateTo = new DateTime(2023, 2, 5),
                    Status = Status.APPROVED
                },
                new OrderEntity()
                {
                    Id = new Guid("63f1f5bd-d116-49f2-a467-37075a7fac64"),
                    RoomId = new Guid("89554661-de90-44a8-8aca-3b49643c4367"),
                    Count = 8,
                    Price = 70,
                    DateFrom = new DateTime(2023, 9, 13),
                    DateTo = new DateTime(2023, 9, 20),
                    Status = Status.DECLINED
                }
            };
        }
    }
}
