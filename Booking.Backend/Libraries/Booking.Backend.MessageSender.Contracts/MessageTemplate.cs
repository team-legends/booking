﻿namespace Booking.Backend.MessageSender.Contracts
{
    public class MessageTemplate
    {
        /// <summary>
        /// Tempate code
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Title of message
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Body of message
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// Tempate provider
        /// </summary>
        public string ProviderName { get; set; }
    }
}
