﻿using Booking.Backend.Users.Contracts;

namespace Booking.Backend.MessageSender.Contracts
{
    [Serializable]
    public class SendMessageRequest
    {
        public string TemplateCode { get; set; }

        public User User { get; set; }

        public Dictionary<string, string> MessageParameters { get; set; }
    }
}
