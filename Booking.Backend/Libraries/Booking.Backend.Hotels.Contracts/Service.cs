﻿using Booking.Backend.Hotels.Contracts.Enums;

namespace Booking.Backend.Hotels.Contracts
{
    /// <summary>
    /// Hotel service
    /// </summary>
    public class Service
    {
        /// <summary>
        /// Service type
        /// </summary>
        public ServiceTypes Type { get; set; }

        /// <summary>
        /// Information about service
        /// </summary>
        public string InformationText { get; set; }
    }
}
