﻿namespace Booking.Backend.Hotels.Contracts
{
    /// <summary>
    /// Hotel geoposition for map
    /// </summary>
    public class Geoposition
    {
        /// <summary>
        /// Hotel latitude
        /// </summary>
        public double Latitude { get; set; }

        /// <summary>
        /// Hotel longitude
        /// </summary>
        public double Longitude { get; set; }
    }
}
