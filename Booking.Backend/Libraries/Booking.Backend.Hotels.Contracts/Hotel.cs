﻿namespace Booking.Backend.Hotels.Contracts
{
    public class Hotel : HotelInfo
    {
        /// <summary>
        /// Hotel id
        /// </summary>
        public Guid Id { get; set; }
    }
}
