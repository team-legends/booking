﻿namespace Booking.Backend.Hotels.Contracts
{
    public class HotelFilterOption
    {
        /// <summary>
        /// Amount of adult for booking
        /// </summary>
        public int Adult { get; set; }

        /// <summary>
        /// Abount of children for booking
        /// </summary>
        public int Children { get; set; }

        /// <summary>
        /// ???
        /// </summary>
        public int Room { get; set; }
    }
}
