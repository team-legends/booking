﻿namespace Booking.Backend.Hotels.Contracts
{
    public class Room
    {
        /// <summary>
        /// Room Id
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// Room People Quantity
        /// </summary>
        public int PeopleQuantity { get; set; }
        /// <summary>
        /// Room Price
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// Room Image
        /// </summary>
        public List<string> Images { get; set; }
    }
}
