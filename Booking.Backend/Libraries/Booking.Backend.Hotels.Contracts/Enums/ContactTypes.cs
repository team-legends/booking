﻿namespace Booking.Backend.Hotels.Contracts.Enums
{
    public enum ContactTypes
    {
        PHONE,
        EMAIL,
        FAX,
        WEBSITE,
        TELEGRAM,
        WHATSUPP,
        VK,
        FB
    }
}
