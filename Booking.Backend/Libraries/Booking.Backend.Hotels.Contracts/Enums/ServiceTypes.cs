﻿namespace Booking.Backend.Hotels.Contracts.Enums
{
    /// <summary>
    /// Service types
    /// </summary>
    public enum ServiceTypes
    {
        AIRCONDITIONER,
        PARKING,
        POOL,
        WIFI,
        BAR,
        KITCHEN
    }
}
