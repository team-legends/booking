﻿namespace Booking.Backend.Hotels.Contracts.Enums
{
    /// <summary>
    /// Hotel type
    /// </summary>
    public enum HotelTypes
    {
        HOTEL,
        APARTMENTS,
        GUESTHOUSE,
        MOTEL,
        HOSTEL,
        CAMPSITE,
        HOLIDAYHOME,
        RESORT
    }
}
