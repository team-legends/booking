﻿using Booking.Backend.Hotels.Contracts.Enums;

namespace Booking.Backend.Hotels.Contracts
{
    /// <summary>
    /// Hotel information
    /// </summary>
    public class HotelInfo
    {
        public Guid ManagerId { get; set; }
        /// <summary>
        /// Rooms
        /// </summary>
        public IEnumerable<Room> Rooms { get; set; }
        /// <summary>
        /// Hotel Image
        /// </summary>
        public string Image { get; set; }
        /// <summary>
        /// Hotel name (title)
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Hotel information
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Hotel type
        /// </summary>
        public HotelTypes Types { get; set; }

        /// <summary>
        /// Hotel address
        /// </summary>
        public Address Address { get; set; }

        /// <summary>
        /// Hotel rate (stars)
        /// </summary>        
        public int StarsRate { get; set; }

        /// <summary>
        /// Hotel contacts
        /// </summary>
        public IEnumerable<Contact> Contacts { get; set; }

        /// <summary>
        /// Hotel services
        /// </summary>
        public IEnumerable<Service> Services { get; set; }

        public bool IsDelete { get; set; }
    }
}
