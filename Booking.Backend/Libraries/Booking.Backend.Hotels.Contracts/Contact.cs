﻿using Booking.Backend.Hotels.Contracts.Enums;

namespace Booking.Backend.Hotels.Contracts
{
    /// <summary>
    /// Hotel contact
    /// </summary>
    public class Contact
    {
        /// <summary>
        /// Contact type
        /// </summary>
        public ContactTypes Type { get; set; }

        /// <summary>
        /// Contact value
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Contact information text
        /// </summary>
        public string InfoText { get; set; }
    }
}
