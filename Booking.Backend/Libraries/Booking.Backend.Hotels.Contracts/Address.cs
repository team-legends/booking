﻿namespace Booking.Backend.Hotels.Contracts
{
    /// <summary>
    /// Hotel address
    /// </summary>
    public class Address
    {
        /// <summary>
        /// Hotel geoposition for map point
        /// </summary>
        public Geoposition Geoposition { get; set; }

        /// <summary>
        /// Country name
        /// </summary>
        public string CountryName { get; set; }

        /// <summary>
        /// City
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Region
        /// </summary>
        public string Region { get; set; }

        /// <summary>
        /// Postal code
        /// </summary>
        public string PostalCode { get; set; }

        /// <summary>
        /// Country code
        /// </summary>
        public string CountryCode { get; set; }

        /// <summary>
        /// Street
        /// </summary>
        public string Street { get; set; }

        /// <summary>
        /// Building
        /// </summary>
        public string Building { get; set; }
    }
}
