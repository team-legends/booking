﻿namespace Booking.Backend.Promo.Contracts
{
    public class PromoInfo
    {
        public string Title { get; set; }

        public string Content { get; set; }

        public string LinkAddress { get; set; }
    }
}
