﻿using Booking.Backend.Reservations.Contracts.Base;
using Booking.Backend.Reservations.Contracts.Enums;

namespace Booking.Backend.Reservations.Contracts
{
    /// <summary>
    /// Order
    /// </summary>
    public class OrderInfo : BaseDateModel
    {
        /// <summary>
        /// User Id
        /// </summary>
        public Guid UserId { get; set; }
        /// <summary>
        /// Hotel Id
        /// </summary>
        public Guid HotelId { get; set; }

        /// <summary>
        /// Room Id
        /// </summary>
        public Guid RoomId { get; set; }

        /// <summary>
        /// Count
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// Price
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Date
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Status
        /// </summary>
        public Status Status { get; set; }

        /// <summary>
        /// Is deleted
        /// </summary>
        public bool IsDelete { get; set; }
    }
}
