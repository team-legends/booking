﻿namespace Booking.Backend.Reservations.Contracts.Base
{
    /// <summary>
    /// Base date model
    /// </summary>
    public class BaseDateModel
    {
        /// <summary>
        /// Date from
        /// </summary>
        public DateTime DateFrom { get; set; }

        /// <summary>
        /// Date to
        /// </summary>
        public DateTime DateTo { get; set; }
    }
}
