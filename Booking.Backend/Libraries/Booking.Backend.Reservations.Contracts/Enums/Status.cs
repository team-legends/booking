﻿namespace Booking.Backend.Reservations.Contracts.Enums
{
    /// <summary>
    /// Status
    /// </summary>
    public enum Status
    {
        NEW,
        APPROVED,
        PAID,
        DECLINED
    }
}
