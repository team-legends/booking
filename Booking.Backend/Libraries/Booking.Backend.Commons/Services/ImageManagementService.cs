﻿using Booking.Backend.Commons.Enums;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;

namespace Booking.Backend.Commons.Services
{
    public class ImageManagementService
    {
        public ImageManagementService(IConfiguration configuration)
        {
            _baseStoragePath =
                configuration.GetSection(BaseImageStorageDirectoryName)?.Value ?? throw new NullReferenceException(BaseImageStorageDirectoryName);
        }

        public async Task SaveImagesAsync(
            string rootContentPath,
            ObjectTypes parentObjectType,
            Guid parentObjectId,
            IFormFileCollection formFiles)
        {
            CreatePathIfNotExists(rootContentPath, parentObjectType, parentObjectId);

            if (formFiles is not null && formFiles.Any())
            {
                foreach (var file in formFiles)
                {
                    var pathToFile = Path.Combine(
                        GetImageDirectoryPath(rootContentPath, parentObjectType, parentObjectId),
                        $"{Guid.NewGuid()}.{_imageFileExtension}");

                    using (var fileStream = new FileStream(pathToFile, FileMode.Create))
                    {
                        await file.CopyToAsync(fileStream);
                    }
                }
            }
        }

        public string[] GetImages(string rootContentPath, ObjectTypes parentObjectType, Guid parentObjectId)
        {
            var pathToFileDirectory = GetImageDirectoryPath(rootContentPath, parentObjectType, parentObjectId);
            CreatePathIfNotExists(rootContentPath, parentObjectType, parentObjectId);
            var files = Directory.GetFiles(pathToFileDirectory, $"*.{_imageFileExtension}");
            return files;
        }

        public void DeleteImage(string rootContentPath, ObjectTypes parentObjectType, Guid parentObjectId, string fileName)
        {
            var pathToFile = Path.Combine(
                GetImageDirectoryPath(rootContentPath, parentObjectType, parentObjectId),
                $"{fileName}.{_imageFileExtension}");
            File.Delete(pathToFile);
        }

        public void DeleteImages(string rootContentPath, ObjectTypes parentObjectType, Guid parentObjectId)
        {
            var pathToFileDirectory = GetImageDirectoryPath(rootContentPath, parentObjectType, parentObjectId);
            Directory.Delete(pathToFileDirectory, true);
        }

        private void CreatePathIfNotExists(string rootContentPath, ObjectTypes parentObjectType, Guid parentObjectId)
        {
            var pathToFileDirectory = GetImageDirectoryPath(rootContentPath, parentObjectType, parentObjectId);
            if (!Directory.Exists(pathToFileDirectory))
            {
                Directory.CreateDirectory(pathToFileDirectory);
            }
        }

        private string GetImageDirectoryPath(string rootContentPath, ObjectTypes parentObjectType, Guid parentObjectId) =>
            Path.Combine(rootContentPath, _baseStoragePath, parentObjectType.ToString(), parentObjectId.ToString());

        private const string BaseImageStorageDirectoryName = "BaseImageStorageDirectoryName";

        private readonly string _baseStoragePath;
        private readonly string _imageFileExtension = "png";
    }
}
