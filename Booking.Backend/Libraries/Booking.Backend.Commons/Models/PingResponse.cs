﻿namespace Booking.Backend.Commons.Models
{
    public class PingResponse
    {
        public string ApplicationName { get; set; }

        public DateTime ServerDateTime { get; } = DateTime.Now;
    }
}
