﻿namespace Booking.Backend.Commons.Configuration
{
    /// <summary>
    /// App info
    /// </summary>
    public class ApplicationInfo
    {

        /// <summary>
        /// Name
        /// </summary>
        public string ApplicationName { get; set; }
    }
}
