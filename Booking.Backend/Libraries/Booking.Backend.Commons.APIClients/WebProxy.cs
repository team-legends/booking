﻿using Newtonsoft.Json;
using System.Text;

namespace Booking.Backend.Commons.APIClients
{
    public class WebProxy
    {
        private readonly HttpClient client;
        public WebProxy()
        {
            client = new HttpClient();
        }

        public async Task<TResponse?> SendAsync<TResponse>(HttpMethod methodType, Uri uri, object request = null)
        {
            try
            {
                var reqMessage = new HttpRequestMessage
                {
                    Method = methodType,
                    RequestUri = uri,
                    Content = RequestToStringContent(request)
                };

                var response = await client.SendAsync(reqMessage);
                response.EnsureSuccessStatusCode();

                var content = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<TResponse>(content);
            }
            catch (Exception ex)
            {
                throw new WebProxyException(ex);
            }
        }

        public async Task<TResponse?> SendAsync<TResponse>(HttpMethod methodType, string urlString, object request = null)
        {
            var requestUri = new Uri(urlString);
            return await SendAsync<TResponse>(methodType, requestUri, request);
        }

        private StringContent? RequestToStringContent(object request)
        {
            if (request == null)
                return null;

            var json = JsonConvert.SerializeObject(request);
            return new StringContent(json, Encoding.UTF8, "application/json");
        }
    }
}
