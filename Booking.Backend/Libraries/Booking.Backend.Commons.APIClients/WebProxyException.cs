﻿namespace Booking.Backend.Commons.APIClients
{
    public class WebProxyException : Exception
    {
        public WebProxyException(Exception ex = null) : base(null, ex)
        {

        }
    }
}
