﻿using Booking.Backend.Commons.Configuration;
using Booking.Backend.Commons.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Booking.Backend.Commons.Controllers.Controllers
{
    /// <summary>
    /// Helth check
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class HealthCheckController : ControllerBase
    {

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="applicationInfoOptions"></param>
        public HealthCheckController(
            ILogger<HealthCheckController> logger,
            IOptions<ApplicationInfo> applicationInfoOptions)
        {
            _logger = logger;
            _applicationInfo = applicationInfoOptions.Value;
        }

        /// <summary>
        /// Ping application
        /// </summary>
        /// <returns></returns>
        [HttpGet(Name = "Ping")]
        public ActionResult<PingResponse> GetPing()
        {
            _logger.LogDebug("Ping application");
            return Ok(new PingResponse() { ApplicationName = _applicationInfo.ApplicationName });
        }

        private readonly ILogger<HealthCheckController> _logger;

        private readonly ApplicationInfo _applicationInfo;
    }
}
