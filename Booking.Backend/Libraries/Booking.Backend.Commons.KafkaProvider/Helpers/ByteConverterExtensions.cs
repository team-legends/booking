﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Booking.Backend.Commons.KafkaProvider.Helpers
{
    internal static class ByteConverterExtensions
    {
        public static byte[] ObjectToByteArray(this object obj)
        {
            if (obj == null) return null;

            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream ms = new MemoryStream();
            bf.Serialize(ms, obj);

            return ms.ToArray();
        }

        public static T ByteArrayToObject<T>(this ReadOnlySpan<byte> arrBytes)
        {
            MemoryStream memStream = new MemoryStream();
            BinaryFormatter binForm = new BinaryFormatter();
            memStream.Write(arrBytes.ToArray(), 0, arrBytes.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            T obj = (T)binForm.Deserialize(memStream);

            return obj;
        }
    }
}
