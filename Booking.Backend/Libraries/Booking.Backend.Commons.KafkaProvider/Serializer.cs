﻿using Booking.Backend.Commons.KafkaProvider.Helpers;
using Confluent.Kafka;

namespace Booking.Backend.Commons.KafkaProvider
{
    public class Serializer<T> : ISerializer<T>
    {
        public byte[] Serialize(T data, SerializationContext context)
        {
            return data.ObjectToByteArray();
        }
    }
}
