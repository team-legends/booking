﻿using Confluent.Kafka;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Booking.Backend.Commons.KafkaProvider
{
    public abstract class RequestConsumer<K, V> : BackgroundService
    {
        public RequestConsumer(
            ILogger logger,
            IConfiguration config)
        {
            _logger = logger;
            var consumerConfig = new ConsumerConfig();
            config.GetSection("Kafka:ConsumerSettings").Bind(consumerConfig);
            _topic = config.GetValue<string>("Kafka:RequestTopic");
            var consumerBuilder = new ConsumerBuilder<K, V>(consumerConfig);
            consumerBuilder.SetValueDeserializer(new Deserializer<V>());
            _kafkaConsumer = consumerBuilder.Build();
        }

        protected abstract Task DoConsumeAction(ConsumeResult<K, V> consumeResult);

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            return Task.Run(() => StartConsumerLoop(stoppingToken), stoppingToken);
        }

        private void StartConsumerLoop(CancellationToken cancellationToken)
        {
            _kafkaConsumer.Subscribe(_topic);

            while (!cancellationToken.IsCancellationRequested)
            {
                try
                {
                    var consumeResult = _kafkaConsumer.Consume(cancellationToken);
                    DoConsumeAction(consumeResult);
                }
                catch (OperationCanceledException)
                {
                    break;
                }
                catch (ConsumeException ex)
                {
                    _logger.Error(ex, $"Consume error: {ex.Error.Reason}");

                    if (ex.Error.IsFatal)
                    {
                        break;
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error(ex, $"Unexpected error: {ex}");
                    break;
                }
            }
        }

        public override void Dispose()
        {
            _kafkaConsumer.Close();
            _kafkaConsumer.Dispose();

            base.Dispose();
        }

        private readonly ILogger _logger;
        private readonly string _topic;
        private readonly IConsumer<K, V> _kafkaConsumer;
    }
}
