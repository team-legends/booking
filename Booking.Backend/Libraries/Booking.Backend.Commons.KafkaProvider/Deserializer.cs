﻿using Booking.Backend.Commons.KafkaProvider.Helpers;
using Confluent.Kafka;
using System;

namespace Booking.Backend.Commons.KafkaProvider
{
    internal class Deserializer<T> : IDeserializer<T>
    {
        public T Deserialize(ReadOnlySpan<byte> data, bool isNull, SerializationContext context)
        {
            return data.ByteArrayToObject<T>();
        }
    }
}
