﻿using Confluent.Kafka;
using Microsoft.Extensions.Configuration;

namespace Booking.Backend.Commons.KafkaProvider
{
    public class KafkaClientHandle
    {
        public KafkaClientHandle(IConfiguration config)
        {
            var conf = new ProducerConfig();
            config.GetSection("Kafka:ProducerSettings").Bind(conf);
            _kafkaProducer = new ProducerBuilder<byte[], byte[]>(conf).Build();
        }

        public Handle Handle => _kafkaProducer.Handle;

        public void Dispose()
        {
            _kafkaProducer.Flush();
            _kafkaProducer.Dispose();
        }

        private readonly IProducer<byte[], byte[]> _kafkaProducer;
    }
}
