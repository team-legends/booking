﻿using Confluent.Kafka;
using System;
using System.Threading.Tasks;

namespace Booking.Backend.Commons.KafkaProvider
{
    public class RequestProducer<K, V>
    {
        public RequestProducer(KafkaClientHandle handle)
        {
            var producerBuilder = new DependentProducerBuilder<K, V>(handle.Handle);
            producerBuilder.SetValueSerializer(new Serializer<V>());
            _producer = producerBuilder.Build();
        }

        public Task<DeliveryResult<K, V>> ProduceAsync(string topic, Message<K, V> message)
            => _producer.ProduceAsync(topic, message);

        public void Produce(string topic, Message<K, V> message, Action<DeliveryReport<K, V>> deliveryHandler = null)
            => _producer.Produce(topic, message, deliveryHandler);

        public void Flush(int timeoutSeconds = 10) => _producer.Flush(TimeSpan.FromSeconds(timeoutSeconds));

        private readonly IProducer<K, V> _producer;
    }
}
