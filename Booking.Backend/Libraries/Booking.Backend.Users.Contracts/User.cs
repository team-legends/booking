﻿namespace Booking.Backend.Users.Contracts
{
    [Serializable]
    public class User
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public Role? Role { get; set; }
    }
}
