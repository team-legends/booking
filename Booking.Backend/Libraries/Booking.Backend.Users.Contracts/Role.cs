﻿namespace Booking.Backend.Users.Contracts
{
    public enum Role
    {
        User,
        Manager,
        Administrator
    }
}
