﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Diagnostics;

namespace Booking.Backend.Commons.Middlewares
{
    public class LogMiddleware
    {
        public LogMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(
            HttpContext httpContext,
            ILogger<ExceptionHandleMiddleware> logger,
            IWebHostEnvironment environment)
        {
            var requestLifeTime = await GetRequestLifeTime(httpContext);
            logger.Log(
                LogLevel.Information,
                "Request method: {0}; Request path: {1}; Request query: {2}; Request querystring: {3}; Response status code: {4}; Response time: {5}; Environment: {6}",
                httpContext.Request.Method,
                httpContext.Request.Path,
                httpContext.Request.Query,
                httpContext.Request.QueryString,
                httpContext.Response?.StatusCode,
                requestLifeTime,
                environment.EnvironmentName);
        }

        private async Task<TimeSpan> GetRequestLifeTime(HttpContext httpContext)
        {
            var timer = new Stopwatch();
            timer.Start();
            await _next(httpContext);
            timer.Stop();

            return timer.Elapsed;
        }

        private readonly RequestDelegate _next;
    }
}
