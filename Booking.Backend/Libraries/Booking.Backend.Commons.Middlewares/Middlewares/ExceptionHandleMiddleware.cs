﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.Net;

namespace Booking.Backend.Commons.Middlewares
{
    public class ExceptionHandleMiddleware
    {
        public ExceptionHandleMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(
            HttpContext httpContext,
            ILogger<ExceptionHandleMiddleware> logger,
            IWebHostEnvironment environment)
        {
            httpContext.Request.EnableBuffering();

            try
            {
                await _next(httpContext);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(httpContext, logger, environment, ex);
            }
        }

        private Task HandleExceptionAsync(
            HttpContext httpContext,
            ILogger<ExceptionHandleMiddleware> logger,
            IWebHostEnvironment environment,
            Exception ex)
        {
            var exceptionGuid = Guid.NewGuid();

            logger.LogError(
                ex,
                "ExceptionId: {0}; ErrorMessage:{1}; Environment: {2}",
                exceptionGuid,
                ex.Message,
                environment.EnvironmentName);

            var responseErrorMessage = environment.IsProduction()
                ? exceptionGuid.ToString()
                : ex.Message;

            var errorResponse = new { Error = responseErrorMessage };
            httpContext.Response.ContentType = "application/json";
            httpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;

            return httpContext.Response.WriteAsJsonAsync(errorResponse);
        }

        private readonly RequestDelegate _next;
    }
}
