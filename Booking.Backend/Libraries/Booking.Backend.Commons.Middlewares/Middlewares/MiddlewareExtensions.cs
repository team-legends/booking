﻿using Microsoft.AspNetCore.Builder;

namespace Booking.Backend.Commons.Middlewares
{
    public static class MiddlewareExtensions
    {
        public static void UseExceptionHandle(this WebApplication app)
        {
            app.UseMiddleware<ExceptionHandleMiddleware>();
        }

        public static void UseLogging(this WebApplication app)
        {
            app.UseMiddleware<LogMiddleware>();
        }
    }
}
