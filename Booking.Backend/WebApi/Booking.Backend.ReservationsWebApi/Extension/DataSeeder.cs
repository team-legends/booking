﻿using Booking.Backend.ReservationsWebApi.Repository.Entities;

namespace Booking.Backend.ReservationsWebApi.Extension
{
    public class DataSeeder
    {
        public static void Seed(ApplicationContext context)
        {
            if (!context.Orders.Any())
            {
                var ord1 = new OrderEntity()
                {
                    Id = new Guid("da9a67a6-a7ac-4591-a533-c5ec15170112"),
                    // DateFrom = DateTime.Now,
                    // DateTo = DateTime.Now.AddDays(10),
                    Count = 7,
                    HotelId = new Guid("cdfc133d-00e0-4c84-b044-fdb5560ed70b"),
                    IsDelete = false,
                    Price = 240,
                    RoomId = new Guid("1c79b5a1-0c82-40e4-b198-c67e58d43919"),
                    Status = Reservations.Contracts.Enums.Status.APPROVED,
                    UserId = new Guid("a4347744-82e9-4787-929d-e4f5c85ceea7")
                };
                var ord2 = new OrderEntity()
                {
                    Id = new Guid("3e8c8f04-1bf8-439a-8247-3a27a96563f7"),
                    // DateFrom = DateTime.Now,
                    // DateTo = DateTime.Now.AddDays(10),
                    Count = 14,
                    HotelId = new Guid("5e7332c2-c43b-42eb-8983-c026a67cfa97"),
                    IsDelete = false,
                    Price = 380,
                    RoomId = new Guid("629dbb61-d329-4f98-bd29-4630d5cc37ab"),
                    Status = Reservations.Contracts.Enums.Status.APPROVED,
                    UserId = new Guid("2fb44345-0ac7-4c75-8391-4872d71aec19")
                };
                var ord3 = new OrderEntity()
                {
                    Id = new Guid("ba57a460-e6d5-4697-b88d-8233a3de4b23"),
                    // DateFrom = DateTime.Now,
                    // DateTo = DateTime.Now.AddDays(10),
                    Count = 10,
                    HotelId = new Guid("4a39c092-d0ab-46a8-a61d-baf9198f43fd"),
                    IsDelete = false,
                    Price = 170,
                    RoomId = new Guid("99216050-31c5-437f-b3a6-2b6a6344c7ec"),
                    Status = Reservations.Contracts.Enums.Status.NEW,
                    UserId = new Guid("db439baa-ab4f-4df8-a597-58142b13b9af")
                };

                var ord = new List<OrderEntity>() { ord1, ord2, ord3 };
                context.AddRange(ord);
                context.SaveChanges();
            }
        }
    }
}
