﻿using Booking.Backend.Reservations.Contracts.Base;

namespace Booking.Backend.ReservationsWebApi.Extension
{
    /// <summary>
    /// extension: date search model
    /// </summary>
    public static class BaseDateModelSearchExtension
    {
        /// <summary>
        /// where if greater
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static IEnumerable<T> WhereIfGreater<T>(this IEnumerable<T> list, DateTime? dateTime) where T : BaseDateModel
        {
            return dateTime != null ?
                list.Where(x => x.DateFrom >= dateTime.Value.Date) : list;
        }

        /// <summary>
        /// where if less
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static IEnumerable<T> WhereIfLess<T>(this IEnumerable<T> list, DateTime? dateTime) where T : BaseDateModel
        {
            return dateTime != null ?
                list.Where(x => x.DateTo < dateTime.Value.Date.AddDays(1)) : list;
        }
    }
}
