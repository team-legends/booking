﻿using Booking.Backend.ReservationsWebApi.Models.Order.CRUDModel;
using FluentValidation;

namespace Booking.Backend.ReservationsWebApi.Validation.Order
{
    /// <summary>
    /// Edit order model validation
    /// </summary>
    public class EditOrderModelValidation : AbstractValidator<EditOrderModel>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public EditOrderModelValidation()
        {
            RuleFor(x => x.DateTo).GreaterThanOrEqualTo(y => y.DateFrom).WithMessage("Date To must be great or equal Date From!");
            RuleFor(x => x.RoomId).NotEmpty().WithMessage("RoomId is a required field");
            RuleFor(x => x.Price).GreaterThan(0).When(p => p.Count > 0).WithMessage("Price must be great zero");
            RuleFor(x => x.Count).GreaterThan(0).When(p => p.Price > 0).WithMessage("Price must be great zero");
        }
    }
}
