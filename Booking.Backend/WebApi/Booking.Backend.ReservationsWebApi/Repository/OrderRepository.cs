﻿using Booking.Backend.ReservationsWebApi.Models.Order.CRUDModel;
using Booking.Backend.ReservationsWebApi.Repository.Entities;

namespace Booking.Backend.ReservationsWebApi.Repository
{
    /// <summary>
    /// Order repository
    /// </summary>
    public class OrderRepository : IOrderRepository
    {
        public OrderRepository(ApplicationContext applicationContext)
        {
            _context = applicationContext;
        }

        /// <summary>
        /// get all
        /// </summary>
        public IEnumerable<OrderEntity> GetAll()
        {
            return _context.Orders.ToList();
        }

        /// <summary>
        /// get by filter
        /// </summary>
        /// <param name="model">filter model</param>
        /// <returns></returns>
        public async Task<IEnumerable<OrderEntity>> GetByFilter(FilterOrderModel model)
        {
            var result = _context.Orders.AsEnumerable();

            if (model?.Hotels?.Count() > 0)
                result = result.Where(o => model.Hotels.Contains(o.HotelId));

            if (model?.UserId != null)
                result = result.Where(o => o.UserId == model.UserId);

            if (model?.Status != null)
                result = result.Where(o => o.Status == model.Status);


            result = result.Where(o => o.DateTo < model.DateFrom.ToUniversalTime() || 
            (o.DateFrom > model.DateTo.ToUniversalTime()));

            return result.ToList();
        }

        /// <summary>
        /// create item
        /// </summary>
        /// <param name="item">order</param>
        public async Task Create(OrderEntity item)
        {
            await _context.Orders.AddAsync(item);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// update
        /// </summary>
        /// <param name="item">order</param>
        public void Update(OrderEntity item)
        {
            _context.Orders.Update(item);
            _context.SaveChanges();
        }

        /// <summary>
        /// delete
        /// </summary>
        /// <param name="id"></param>
        public void SetDelete(Guid id)
        {
            var orderToDelete = _context.Orders.Find(id);
            if (orderToDelete is not null)
            {
                orderToDelete.IsDelete = true;
                _context.SaveChanges();
            }
        }

        public OrderEntity GetById(Guid id)
        {
            return _context.Orders.Find(id);
        }

        private readonly ApplicationContext _context;
    }
}
