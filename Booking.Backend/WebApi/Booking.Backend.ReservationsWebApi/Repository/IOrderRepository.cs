﻿using Booking.Backend.ReservationsWebApi.Models.Order.CRUDModel;
using Booking.Backend.ReservationsWebApi.Repository.Entities;

namespace Booking.Backend.ReservationsWebApi.Repository
{
    /// <summary>
    /// Order repository interface
    /// </summary>
    public interface IOrderRepository
    {
        /// <summary>
        /// get all
        /// </summary>
        IEnumerable<OrderEntity> GetAll();


        OrderEntity GetById(Guid id);

        /// <summary>
        /// get by filter
        /// </summary>
        /// <param name="model">filter model</param>
        /// <returns></returns>
        Task<IEnumerable<OrderEntity>> GetByFilter(FilterOrderModel model);

        /// <summary>
        /// create
        /// </summary>
        /// <param name="model"></param>
        Task Create(OrderEntity model);

        /// <summary>
        /// update
        /// </summary>
        /// <param name="model"></param>
        void Update(OrderEntity model);

        /// <summary>
        /// delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        void SetDelete(Guid id);
    }
}
