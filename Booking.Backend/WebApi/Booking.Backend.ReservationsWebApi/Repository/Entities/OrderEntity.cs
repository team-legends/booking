﻿using Booking.Backend.Commons.Models.Base;
using Booking.Backend.Reservations.Contracts.Enums;

namespace Booking.Backend.ReservationsWebApi.Repository.Entities
{
    public class OrderEntity : BaseEntity
    {
        /// <summary>
        /// Date from
        /// </summary>
        public DateTime DateFrom { get; set; }

        /// <summary>
        /// Date to
        /// </summary>
        public DateTime DateTo { get; set; }

        /// <summary>
        /// Hotel Id
        /// </summary>
        public Guid HotelId { get; set; }

        /// <summary>
        /// User Id
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// Room Id
        /// </summary>
        public Guid RoomId { get; set; }

        /// <summary>
        /// Count
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// Price
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Status
        /// </summary>
        public Status Status { get; set; }

        /// <summary>
        /// Is deleted
        /// </summary>
        public bool IsDelete { get; set; }
    }
}
