﻿using Booking.Backend.ReservationsWebApi.Models.Order.CRUDModel;
using Booking.Backend.ReservationsWebApi.Services.Interface;
using Microsoft.AspNetCore.Mvc;

namespace Booking.Backend.ReservationsWebApi.Controllers
{
    /// <summary>
    /// Reservations management
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class ReservationsController : ControllerBase
    {
        /// <summary>
        /// service
        /// </summary>
        private readonly IOrderService _orderService;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="orderService"></param>
        public ReservationsController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        /// <summary>
        /// Get order
        /// </summary>
        /// <param name="id">Order id</param>
        /// <returns></returns>
        [HttpGet("GetOrder/{id:guid}")]
        public IActionResult GetOrder(Guid id)
        {
            var searchedOrder = _orderService.GetById(id);
            return searchedOrder is null
                ? NotFound()
                : Ok(searchedOrder);
        }

        /// <summary>
        /// Get orders by filter
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("GetOrdersByFilter")]
        public async Task<IActionResult> GetOrdersByFilter(FilterOrderModel model)
        {
            var searchedOrders = await _orderService.GetByFilter(model);
            return Ok(searchedOrders);
        }

        /// <summary>
        /// Create new order
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromBody] CreateOrderModel model)
        {
            if (ModelState.IsValid)
            {
                var createdOrderId = await _orderService.Create(model);
                return Ok(createdOrderId);
            }

            return BadRequest();
        }

        /// <summary>
        /// Edit order
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut("Update/{id:guid}")]
        public IActionResult Update(Guid id, [FromBody] EditOrderModel model)
        {
            if (ModelState.IsValid)
            {
                _orderService.Update(model);
                return CreatedAtRoute(
                    "GetOrdersByFilter",
                    new { id });
            }

            return NotFound();
        }

        /// <summary>
        /// Delete order
        /// </summary>
        /// <param name="id">Order id</param>
        /// <returns></returns>
        [HttpDelete("Delete/{id:guid}")]
        public IActionResult Delete(Guid id)
        {
            _orderService.Delete(id);
            return Ok();
        }
    }
}
