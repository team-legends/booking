﻿using Booking.Backend.Reservations.Contracts.Base;
using Booking.Backend.Reservations.Contracts.Enums;

namespace Booking.Backend.ReservationsWebApi.Models.Order.CRUDModel
{
    /// <summary>
    /// Orders filter
    /// </summary>
    public class FilterOrderModel : BaseDateModel
    {
        /// <summary>
        /// Hotels
        /// </summary>
        public IEnumerable<Guid> Hotels { get; set; } = new List<Guid>();

        /// <summary>
        /// Hotels
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// Order status type
        /// </summary>
        public Status? Status { get; set; }
    }
}
