﻿using OrderModel = Booking.Backend.Reservations.Contracts.OrderInfo;

namespace Booking.Backend.ReservationsWebApi.Models.Order.CRUDModel
{
    /// <summary>
    /// Order info
    /// </summary>
    public class CreateOrderModel : OrderModel
    {
    }
}
