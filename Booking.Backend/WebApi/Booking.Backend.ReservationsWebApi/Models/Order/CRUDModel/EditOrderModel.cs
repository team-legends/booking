﻿using OrderModel = Booking.Backend.Reservations.Contracts.OrderInfo;

namespace Booking.Backend.ReservationsWebApi.Models.Order.CRUDModel
{
    /// <summary>
    /// Updated order info
    /// </summary>
    public class EditOrderModel : OrderModel
    {
    }
}
