#See https://aka.ms/customizecontainer to learn how to customize your debug container and how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["Booking.Backend/WebApi/Booking.Backend.ReservationsWebApi/Booking.Backend.ReservationsWebApi.csproj", "Booking.Backend/WebApi/Booking.Backend.ReservationsWebApi/"]
COPY ["Booking.Backend/Libraries/Booking.Backend.Commons.Controller/Booking.Backend.Commons.Controllers.csproj", "Booking.Backend/Libraries/Booking.Backend.Commons.Controller/"]
COPY ["Booking.Backend/Libraries/Booking.Backend.Commons.Middlewares/Booking.Backend.Commons.Middlewares.csproj", "Booking.Backend/Libraries/Booking.Backend.Commons.Middlewares/"]
COPY ["Booking.Backend/Libraries/Booking.Backend.Commons/Booking.Backend.Commons.csproj", "Booking.Backend/Libraries/Booking.Backend.Commons/"]
COPY ["Booking.Backend/Libraries/Booking.Backend.Reservations.Contracts/Booking.Backend.Reservations.Contracts.csproj", "Booking.Backend/Libraries/Booking.Backend.Reservations.Contracts/"]
RUN dotnet restore "Booking.Backend/WebApi/Booking.Backend.ReservationsWebApi/Booking.Backend.ReservationsWebApi.csproj"
COPY . .
WORKDIR "/src/Booking.Backend/WebApi/Booking.Backend.ReservationsWebApi"
RUN dotnet build "Booking.Backend.ReservationsWebApi.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "Booking.Backend.ReservationsWebApi.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Booking.Backend.ReservationsWebApi.dll"]