﻿namespace Booking.Backend.ReservationsWebApi.Services.Interface
{
    /// <summary>
    /// Service interface
    /// </summary>
    /// <typeparam name="T">base entity</typeparam>
    public interface IService<T>
    {
    }
}
