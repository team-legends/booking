﻿using Booking.Backend.Reservations.Contracts;
using Booking.Backend.ReservationsWebApi.Models.Order.CRUDModel;

namespace Booking.Backend.ReservationsWebApi.Services.Interface
{
    public interface IOrderService : IService<OrderInfo>
    {
        /// <summary>
        /// get all
        /// </summary>
        /// <returns></returns>
        public List<Order> GetAll();

        /// <summary>
        /// Get order by id
        /// </summary>
        /// <param name="id">Order id</param>
        /// <returns></returns>
        OrderInfo GetById(Guid id);

        /// <summary>
        /// get by filter
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
       Task<List<Order>> GetByFilter(FilterOrderModel model);

        /// <summary>
        /// create
        /// </summary>
        /// <param name="model"></param>
        Task<Guid> Create(CreateOrderModel model);

        /// <summary>
        /// update
        /// </summary>
        /// <param name="model"></param>
        void Update(EditOrderModel model);

        /// <summary>
        /// delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        void Delete(Guid id);
    }
}
