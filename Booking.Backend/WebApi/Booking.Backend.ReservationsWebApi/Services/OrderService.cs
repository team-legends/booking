﻿using AutoMapper;
using Booking.Backend.Reservations.Contracts;
using Booking.Backend.ReservationsWebApi.Models.Order.CRUDModel;
using Booking.Backend.ReservationsWebApi.Repository;
using Booking.Backend.ReservationsWebApi.Repository.Entities;
using Booking.Backend.ReservationsWebApi.Services.Interface;

namespace Booking.Backend.ReservationsWebApi.Services
{
    /// <summary>
    /// order: service
    /// </summary>
    public class OrderService : IOrderService
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IMapper _mapper;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="orderRepository"></param>
        /// <param name="mapper"></param>
        public OrderService(IOrderRepository orderRepository, IMapper mapper)
        {
            _orderRepository = orderRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// order: get all
        /// </summary>
        /// <returns></returns>
        public List<Order> GetAll()
        {
            var orderEntityList = _orderRepository.GetAll().ToList();
            var orders = orderEntityList
                .Select(orderEntity => _mapper.Map<Order>(orderEntity))
                .ToList();

            return orders;
        }

        /// <summary>
        /// get by filter
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<List<Order>> GetByFilter(FilterOrderModel model)
        {
            var orderEntityList = await _orderRepository.GetByFilter(model);
            var orders = orderEntityList
                .Select(orderEntity => _mapper.Map<Order>(orderEntity))
                .ToList();

            return orders;
        }

        /// <summary>
        /// create item
        /// </summary>
        /// <param name="model"></param>
        public async Task<Guid> Create(CreateOrderModel model)
        {
            model.Date = DateTime.UtcNow;
            var orderEntity = _mapper.Map<OrderEntity>(model);
            await _orderRepository.Create(orderEntity);
            return orderEntity.Id;
        }

        /// <summary>
        /// update
        /// </summary>
        /// <param name="model"></param>
        public void Update(EditOrderModel model)
        {
            var orderEntity = _mapper.Map<OrderEntity>(model);
            _orderRepository.Update(orderEntity);
        }

        /// <summary>
        /// delete
        /// </summary>
        /// <param name="id"></param>
        public void Delete(Guid id)
        {
            _orderRepository.SetDelete(id);
        }

        public OrderInfo GetById(Guid id)
        {
            var orderEntity = _orderRepository.GetById(id);
            var order = orderEntity is not null
                ? _mapper.Map<OrderInfo>(orderEntity)
                : null;

            return order;
        }
    }
}
