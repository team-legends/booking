﻿using AutoMapper;
using Booking.Backend.Reservations.Contracts;
using Booking.Backend.ReservationsWebApi.Models.Order.CRUDModel;
using Booking.Backend.ReservationsWebApi.Repository.Entities;

namespace Booking.Backend.ReservationsWebApi
{
    /// <summary>
    /// Order map config
    /// </summary>
    public class ApplicationMappingProfile : Profile
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public ApplicationMappingProfile() : base()
        {
            CreateMap<CreateOrderModel, OrderInfo>()
                .ForMember(dest => dest.Status, opt => new Guid("8f5e00ee-8f6d-42ba-beba-6d5ae522e47b"));
            CreateMap<EditOrderModel, OrderInfo>();
            CreateMap<OrderInfo, OrderEntity>().ReverseMap();
            CreateMap<Order, OrderEntity>().ReverseMap();
        }
    }
}
