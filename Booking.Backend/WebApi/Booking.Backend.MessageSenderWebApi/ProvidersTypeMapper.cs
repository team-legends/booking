﻿using Booking.Backend.MessageSenderWebApi.Attributes;
using Booking.Backend.MessageSenderWebApi.Providers;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Booking.Backend.MessageSenderWebApi
{
    public class ProvidersTypeMapper
    {

        public ProvidersTypeMapper(ILogger logger)
        {
            _logger = logger;
            _currentProjectAssembly = Assembly.GetExecutingAssembly();
            lock (_lockObject)
            {
                LoadTypesWithNameAttributeStore(_currentProjectAssembly);
            }
        }

        public Dictionary<string, Type> ProvidersTypesStore => _providersTypesStore;

        public IMessageSenderProvider GetProvider(string providerName)
        {
            if (ProvidersTypesStore.ContainsKey(providerName))
            {
                var providerType = ProvidersTypesStore[providerName];
                var providerInstance = Activator.CreateInstance(providerType, _logger);

                return (IMessageSenderProvider)providerInstance;
            }

            throw new ArgumentOutOfRangeException($"Provider with name {providerName} do not exist.");
        }


        private void LoadTypesWithNameAttributeStore(Assembly assembly)
        {
            foreach (Type type in assembly.GetTypes())
            {
                if (type.GetCustomAttributes(typeof(ProviderNameAttribute), true).Length > 0)
                {
                    var customAttributes = type.GetCustomAttributes(typeof(ProviderNameAttribute), true);
                    var nameAttribute = customAttributes.First() as ProviderNameAttribute;
                    _providersTypesStore.Add(nameAttribute.Name, type);
                }
            }
        }

        private readonly ILogger _logger;

        private readonly Assembly _currentProjectAssembly;

        private readonly object _lockObject = new();

        private volatile Dictionary<string, Type> _providersTypesStore = new Dictionary<string, Type>();
    }
}
