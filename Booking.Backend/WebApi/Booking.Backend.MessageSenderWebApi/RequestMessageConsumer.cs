﻿using Booking.Backend.Commons.KafkaProvider;
using Booking.Backend.MessageSender.Contracts;
using Booking.Backend.MessageSenderWebApi.Services;
using Confluent.Kafka;
using Microsoft.Extensions.Configuration;
using Serilog;
using System.Threading.Tasks;

namespace Booking.Backend.MessageSenderWebApi
{
    public class RequestMessageConsumer : RequestConsumer<string, SendMessageRequest>
    {
        public RequestMessageConsumer(
            MessageSenderService messageSenderService,
            ILogger logger,
            IConfiguration config)
            : base(logger, config)
        {
            _messageSenderService = messageSenderService;
        }

        protected override async Task DoConsumeAction(ConsumeResult<string, SendMessageRequest> consumeResult)
        {
            await _messageSenderService.SendAsync(consumeResult.Value);
        }

        private readonly MessageSenderService _messageSenderService;
    }
}
