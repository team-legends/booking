﻿using Booking.Backend.MessageSenderWebApi.Repositories.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Booking.Backend.MessageSenderWebApi.Repositories
{
    public class MessageTemplateRepository
    {
        public MessageTemplateRepository(ApplicationContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<MessageTemplateEntity>> GetList()
        {
            var searchedTemplateList =
                await _context.MessageTemplate.ToListAsync();

            return searchedTemplateList;
        }

        public async Task<MessageTemplateEntity> GetTemplate(string code)
        {
            var searchedTemplate =
                await _context.MessageTemplate.SingleAsync(messageTemplateEntity => messageTemplateEntity.Code == code);
            return searchedTemplate;
        }

        public async Task<Guid> AddTemplate(MessageTemplateEntity messageTemplateEntity)
        {
            await _context.MessageTemplate.AddAsync(messageTemplateEntity);
            await _context.SaveChangesAsync();
            return messageTemplateEntity.Id;
        }

        public async Task DeleteTemplate(Guid messageTemplateId)
        {
            var templateToDelete = _context.MessageTemplate.Find(messageTemplateId);
            _context.MessageTemplate.Remove(templateToDelete);
            await _context.SaveChangesAsync();
        }

        public async Task EditTemplate(MessageTemplateEntity messageTemplateEntity)
        {
            _context.MessageTemplate.Update(messageTemplateEntity);
            await _context.SaveChangesAsync();
        }

        private readonly ApplicationContext _context;
    }
}
