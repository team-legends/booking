﻿using Booking.Backend.Commons.Models.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace Booking.Backend.MessageSenderWebApi.Repositories.Entities
{
    [Table("MessageTemplates")]
    public class MessageTemplateEntity : BaseEntity
    {
        public string Code { get; set; }

        public string Title { get; set; }

        public string Body { get; set; }

        public string ProviderName { get; set; }
    }
}
