﻿using Booking.Backend.Commons.KafkaProvider;
using Booking.Backend.MessageSender.Contracts;
using Booking.Backend.MessageSenderWebApi.Repositories;
using Booking.Backend.MessageSenderWebApi.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Booking.Backend.MessageSenderWebApi.LoadingData
{
    public static class LoadingDataDIHelper
    {
        public static void LoadData(this IServiceCollection services, IConfiguration configuration)
        {
            var dbcs = configuration.GetConnectionString("DataBaseConnectionStrings");

            if (string.IsNullOrEmpty(dbcs))
                throw new Exception("Connection string does not match the format");

            var connectionString = string.Format(
                dbcs,
                Environment.GetEnvironmentVariable("DB_SERVER"),
                Environment.GetEnvironmentVariable("DB_PORT"),
                Environment.GetEnvironmentVariable("DB_USERNAME"),
                Environment.GetEnvironmentVariable("DB_PASSWORD"),
                Environment.GetEnvironmentVariable("DB_NAME"));

            services.AddDbContext<ApplicationContext>(optionBuilder =>
                optionBuilder.UseNpgsql(connectionString),
                contextLifetime: ServiceLifetime.Singleton);

            services.AddSingleton<MessageTemplateRepository>();
            services.AddSingleton<MessageTemplateService>();
            services.AddSingleton<MessageSenderService>();

            services.AddAutoMapper(
                typeof(ApplicationMappingProfile));

            services.AddSingleton<ProvidersTypeMapper>();

            services.AddSingleton<KafkaClientHandle>();
            services.AddSingleton<RequestProducer<string, SendMessageRequest>>();

            services.AddHostedService<RequestMessageConsumer>();
        }
    }
}
