﻿using Booking.Backend.MessageSenderWebApi.Repositories.Entities;
using Microsoft.EntityFrameworkCore;

namespace Booking.Backend.MessageSenderWebApi
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
        }

        public DbSet<MessageTemplateEntity> MessageTemplate { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MessageTemplateEntity>(entity =>
            {
                entity.HasIndex(e => e.Code).IsUnique();
            });
        }
    }
}
