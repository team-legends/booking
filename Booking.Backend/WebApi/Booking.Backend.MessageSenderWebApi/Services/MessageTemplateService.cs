﻿using AutoMapper;
using Booking.Backend.MessageSender.Contracts;
using Booking.Backend.MessageSenderWebApi.Repositories;
using Booking.Backend.MessageSenderWebApi.Repositories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Booking.Backend.MessageSenderWebApi.Services
{
    public class MessageTemplateService
    {
        public MessageTemplateService(
            IMapper mapper,
            MessageTemplateRepository messageTemplateRepository)
        {
            _messageTemplateRepository = messageTemplateRepository;
            _mapper = mapper;
        }

        public async Task<MessageTemplate> GetTemplateAsync(string code)
        {
            var templateEntity = await _messageTemplateRepository.GetTemplate(code);
            var template = _mapper.Map<MessageTemplate>(templateEntity);
            return template;
        }

        public async Task<IEnumerable<MessageTemplate>> GetTemplateListAsync()
        {
            var templateEntities = await _messageTemplateRepository.GetList();
            var templates = templateEntities
                .Select(templateEntity => _mapper.Map<MessageTemplate>(templateEntity));
            return templates;
        }

        public async Task<Guid> AddTemplateAsync(MessageTemplate messageTemplate)
        {
            var message = _mapper.Map<MessageTemplateEntity>(messageTemplate);
            var createMessageId = await _messageTemplateRepository.AddTemplate(message);
            return createMessageId;
        }

        public async Task DeleteTemplateAsync(Guid id)
        {
            await _messageTemplateRepository.DeleteTemplate(id);
        }

        public async Task EditTemplateAsync(Guid id, MessageTemplate messageTemplate)
        {
            var message = _mapper.Map<MessageTemplateEntity>(messageTemplate);
            message.Id = id;
            await _messageTemplateRepository.EditTemplate(message);
        }

        private readonly MessageTemplateRepository _messageTemplateRepository;
        private readonly IMapper _mapper;
    }
}
