﻿using Booking.Backend.Commons.KafkaProvider;
using Booking.Backend.MessageSender.Contracts;
using Booking.Backend.MessageSenderWebApi.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Booking.Backend.MessageSenderWebApi.Services
{
    public class MessageSenderService
    {
        public MessageSenderService(
            IConfiguration config,
            RequestProducer<string, SendMessageRequest> requestProducer,
            MessageTemplateService messageTemplateService,
            ProvidersTypeMapper providersTypeMapper)
        {
            _messageTemplateService = messageTemplateService;
            _providersTypeMapper = providersTypeMapper;
            _requestProducer = requestProducer;
            _topic = config.GetValue<string>("Kafka:RequestTopic");
        }

        public async Task SendAsync(SendMessageRequest sendMessageRequest)
        {
            var template = await _messageTemplateService.GetTemplateAsync(sendMessageRequest.TemplateCode);
            var provider = _providersTypeMapper.GetProvider(template.ProviderName);
            var message = new Message()
            {
                User = sendMessageRequest.User,
                Title = GenerateResultTempateValue(template.Title, sendMessageRequest.MessageParameters),
                Body = GenerateResultTempateValue(template.Body, sendMessageRequest.MessageParameters)
            };

            var wasSent = await provider.TrySendAsync(message);
            if (!wasSent)
            {
                await RescheduleTask(sendMessageRequest);
            }
        }

        private async Task RescheduleTask(SendMessageRequest sendMessageRequest)
        {
            await _requestProducer.ProduceAsync(
                _topic,
                new Confluent.Kafka.Message<string, SendMessageRequest>() { Key = Guid.NewGuid().ToString(), Value = sendMessageRequest });
        }

        private string GenerateResultTempateValue(string templateString, Dictionary<string, string> parameters)
        {
            var resultTempate = templateString;
            foreach (var parameter in parameters)
            {
                resultTempate = resultTempate.Replace($"[{parameter.Key}]", parameter.Value);
            }

            return resultTempate;
        }

        private readonly string _topic;
        private readonly MessageTemplateService _messageTemplateService;
        private readonly ProvidersTypeMapper _providersTypeMapper;
        private readonly RequestProducer<string, SendMessageRequest> _requestProducer;
    }
}
