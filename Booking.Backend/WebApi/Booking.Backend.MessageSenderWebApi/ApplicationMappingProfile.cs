﻿using AutoMapper;
using Booking.Backend.MessageSender.Contracts;
using Booking.Backend.MessageSenderWebApi.Repositories.Entities;

namespace Booking.Backend.MessageSenderWebApi
{
    public class ApplicationMappingProfile : Profile
    {
        public ApplicationMappingProfile()
        {
            CreateMap<MessageTemplateEntity, MessageTemplate>().ReverseMap();
        }
    }
}
