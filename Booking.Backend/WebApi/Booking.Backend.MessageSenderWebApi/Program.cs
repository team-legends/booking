using Booking.Backend.Commons.Configuration;
using Booking.Backend.Commons.Controllers.Controllers;
using Booking.Backend.Commons.Middlewares;
using Booking.Backend.Commons.Models;
using Booking.Backend.MessageSenderWebApi.LoadingData;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json.Converters;
using Serilog;
using System;
using System.IO;
using System.Reflection;

namespace Booking.Backend.MessageSenderWebApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            var logger = new LoggerConfiguration()
                .ReadFrom.Configuration(builder.Configuration)
                .CreateLogger();

            builder.Host.UseSerilog(logger);

            builder.Services.LoadData(builder.Configuration);
            builder.Services
                .AddControllers()
                .AddApplicationPart(typeof(HealthCheckController).GetTypeInfo().Assembly)
                .AddNewtonsoftJson(opts => opts.SerializerSettings.Converters.Add(new StringEnumConverter()));
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen(c =>
            {
                c.IncludeXmlComments(
                    Path.Combine(AppContext.BaseDirectory, $"{Assembly.GetExecutingAssembly().GetName().Name}.xml"));
                c.IncludeXmlComments(
                    Path.Combine(AppContext.BaseDirectory, $"{typeof(HealthCheckController).Assembly.GetName().Name}.xml"));
                c.IncludeXmlComments(
                    Path.Combine(AppContext.BaseDirectory, $"{typeof(PingResponse).Assembly.GetName().Name}.xml"));
            });
            builder.Services.AddSwaggerGenNewtonsoftSupport();

            builder.Services.AddOptions<ApplicationInfo>()
                .Bind(builder.Configuration.GetSection("ApplicationInfo"));

            var app = builder.Build();

            app.UseLogging();
            app.UseExceptionHandle();

            if (!app.Environment.IsProduction())
            {
                app.UseSwagger();
                app.UseSwaggerUI(options =>
                {
                    options.DocumentTitle = "Booking.Backend.MessageSenderWebApi API";
                });
            }

            app.UseHttpsRedirection();

            app.UseAuthorization();


            app.MapControllers();

            app.Run();
        }
    }
}