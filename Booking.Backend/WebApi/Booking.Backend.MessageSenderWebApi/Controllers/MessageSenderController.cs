﻿using Booking.Backend.MessageSender.Contracts;
using Booking.Backend.MessageSenderWebApi.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Booking.Backend.MessageSenderWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MessageSenderController : ControllerBase
    {
        public MessageSenderController(MessageSenderService messageSenderService)
        {
            _messageSenderService = messageSenderService;
        }

        [HttpPost("SendMessage")]
        public async Task<IActionResult> SendMessage(
            [FromBody] SendMessageRequest sendMessageRequest)
        {
            await _messageSenderService.SendAsync(sendMessageRequest);
            return Ok();
        }

        private readonly MessageSenderService _messageSenderService;
    }
}
