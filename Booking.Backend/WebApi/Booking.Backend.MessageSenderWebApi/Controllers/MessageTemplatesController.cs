﻿using Booking.Backend.MessageSender.Contracts;
using Booking.Backend.MessageSenderWebApi.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Booking.Backend.MessageSenderWebApi.Controllers
{
    /// <summary>
    /// Message template management
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class MessageTemplatesController : ControllerBase
    {
        public MessageTemplatesController(MessageTemplateService messageTemplateService)
        {
            _messageTemplateService = messageTemplateService;
        }

        /// <summary>
        /// Get tempate by code
        /// </summary>
        /// <param name="templateCode">Template code</param>
        /// <returns></returns>
        [HttpGet("GetTempate/{templateCode}")]
        public async Task<IActionResult> GetTempateAsync(string templateCode)
        {
            var tempate = await _messageTemplateService.GetTemplateAsync(templateCode);
            return Ok(tempate);
        }

        /// <summary>
        /// Get tempate list
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetTempates")]
        public async Task<IActionResult> GetTempatesAsync()
        {
            var tempate = await _messageTemplateService.GetTemplateListAsync();
            return Ok(tempate);
        }

        /// <summary>
        /// Add new template
        /// </summary>
        /// <param name="messageTemplate"></param>
        /// <returns></returns>
        [HttpPost("AddTemplate")]
        public async Task<IActionResult> AddTempateAsync([FromBody] MessageTemplate messageTemplate)
        {
            var createdMessageId = await _messageTemplateService.AddTemplateAsync(messageTemplate);
            return Ok(createdMessageId);
        }

        /// <summary>
        /// Edit tempate
        /// </summary>
        /// <param name="id">Template ID</param>
        /// <param name="messageTemplate"></param>
        /// <returns></returns>
        [HttpPost("EditTempate/{id:guid}")]
        public async Task<IActionResult> EditTempateAsync(Guid id, [FromBody] MessageTemplate messageTemplate)
        {
            await _messageTemplateService.EditTemplateAsync(id, messageTemplate);
            return Ok();
        }


        /// <summary>
        /// Delete tempate
        /// </summary>
        /// <param name="id">Tempate ID</param>
        /// <returns></returns>
        [HttpDelete("DeleteTemplate/{id:guid}")]
        public async Task<IActionResult> DeleteTempateAsync(Guid id)
        {
            await _messageTemplateService.DeleteTemplateAsync(id);
            return Ok();
        }

        private readonly MessageTemplateService _messageTemplateService;
    }
}
