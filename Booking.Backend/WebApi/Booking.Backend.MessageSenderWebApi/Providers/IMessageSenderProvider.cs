﻿using Booking.Backend.MessageSenderWebApi.Models;
using System.Threading.Tasks;

namespace Booking.Backend.MessageSenderWebApi.Providers
{
    public interface IMessageSenderProvider
    {
        Task<bool> TrySendAsync(Message message);
    }
}
