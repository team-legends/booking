﻿using Booking.Backend.MessageSenderWebApi.Attributes;
using Booking.Backend.MessageSenderWebApi.Models;
using Serilog;
using System;
using System.Threading.Tasks;

namespace Booking.Backend.MessageSenderWebApi.Providers
{
    [ProviderName("Log")]
    public class LogMessageSenderProvider : IMessageSenderProvider
    {
        public LogMessageSenderProvider(ILogger logger)
        {
            _logger = logger;
        }

        public async Task<bool> TrySendAsync(Message message)
        {
            await Task.Run(() =>
            {
                _logger.Information(
                    $"New message was sent {Environment.NewLine}" +
                    $"Address: {message.User.Email}{Environment.NewLine}" +
                    $"Title: {message.Title}{Environment.NewLine}" +
                    $"Content: {message.Body}");
            });

            return true;
        }

        private readonly ILogger _logger;
    }
}
