﻿using Booking.Backend.Users.Contracts;

namespace Booking.Backend.MessageSenderWebApi.Models
{
    public class Message
    {
        public User User { get; set; }

        public string Title { get; set; }

        public string Body { get; set; }
    }
}
