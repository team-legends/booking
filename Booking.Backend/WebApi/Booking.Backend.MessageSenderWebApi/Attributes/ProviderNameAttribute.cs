﻿using System;

namespace Booking.Backend.MessageSenderWebApi.Attributes
{
    public class ProviderNameAttribute : Attribute
    {
        public ProviderNameAttribute(string name)
        {
            Name = name;
        }

        public string Name { get; }
    }
}
