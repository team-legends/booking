﻿namespace Booking.Backend.UsersWebApi.Exceptions
{
    public class UserAlreadyExistsException : Exception
    {
        public UserAlreadyExistsException(string message, Exception ex = null) : base(message, ex)
        {

        }
    }
}
