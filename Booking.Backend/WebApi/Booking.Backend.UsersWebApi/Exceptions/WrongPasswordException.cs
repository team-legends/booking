﻿namespace Booking.Backend.UsersWebApi.Exceptions
{
    public class WrongPasswordException : Exception
    {
        public WrongPasswordException(string message, Exception ex = null) : base(message, ex)
        {

        }
    }
}
