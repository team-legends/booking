﻿using AutoMapper;
using Booking.Backend.Users.Contracts;
using DbUser = Booking.Backend.UsersWebApi.Repositories.Entities.User;

namespace Booking.Backend.UsersWebApi.Mapper
{
    public class UsersMappingConfig : Profile
    {
        public UsersMappingConfig()
        {
            CreateMap<User, DbUser>().ReverseMap();
        }
    }
}
