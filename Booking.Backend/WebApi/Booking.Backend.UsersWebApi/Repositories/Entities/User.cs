﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Booking.Backend.UsersWebApi.Repositories.Entities
{
    public class User
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("login")]
        public string Login { get; set; }

        [BsonElement("email")]
        public string Email { get; set; }

        [BsonElement("role")]
        public string Role { get; set; }

        [BsonElement("passwordHash")]
        public byte[] PasswordHash { get; set; } = { };

        [BsonElement("passwordSalt")]
        public byte[] PasswordSalt { get; set; } = { };

        [BsonElement("startDate")]
        public DateTime StartDate { get; set; }
        [BsonElement("endDate")]
        public DateTime? EndDate { get; set; }
    }
}
