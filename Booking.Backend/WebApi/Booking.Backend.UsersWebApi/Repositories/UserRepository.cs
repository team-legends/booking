﻿using Booking.Backend.UsersWebApi.Repositories.Entities;
using MongoDB.Driver;

namespace Booking.Backend.UsersWebApi.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly MongoDBContext _context;

        public UserRepository(MongoDBContext context) => _context = context;

        public async Task Create(User user) => await _context.Users.InsertOneAsync(user);

        public async Task<User> GetByLogin(string login) => await _context.Users.Find(user => user.Login == login).FirstOrDefaultAsync();

        public async Task<User> GetById(Guid id) => await _context.Users.Find(user => user.Id == id.ToString()).FirstOrDefaultAsync();
    }
}
