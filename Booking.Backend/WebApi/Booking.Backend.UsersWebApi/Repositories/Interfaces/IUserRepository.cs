﻿using Booking.Backend.UsersWebApi.Repositories.Entities;

namespace Booking.Backend.UsersWebApi.Repositories
{
    public interface IUserRepository
    {
        public Task<User> GetByLogin(string login);
        public Task<User> GetById(Guid id);
        public Task Create(User user);
    }
}
