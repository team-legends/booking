﻿using AutoMapper;
using Booking.Backend.Users.Contracts;
using Booking.Backend.UsersWebApi.Exceptions;
using Booking.Backend.UsersWebApi.Repositories;
using Booking.Backend.UsersWebApi.Services.Interfaces;
using System.Security.Cryptography;
using System.Text;
using DbUser = Booking.Backend.UsersWebApi.Repositories.Entities.User;

namespace Booking.Backend.UsersWebApi.Services
{
    public class UsersService : IUsersService
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        public UsersService(IUserRepository userRepository, IMapper mapper, IConfiguration configuration)
        {
            _userRepository = userRepository;
            _mapper = mapper;
            _configuration = configuration;
        }
        public async Task<User> Login(string login, string password)
        {
            var dbUser = await _userRepository.GetByLogin(login);

            if (dbUser == null && dbUser.EndDate.HasValue)
            {
                throw new UserNotFoundException("User not found.");
            }

            if (!VerifyPasswordHash(password, dbUser.PasswordHash, dbUser.PasswordSalt))
            {
                throw new WrongPasswordException("Wrong password.");
            }

            User user = _mapper.Map<User>(dbUser);

            return user;
        }

        public async Task<User> Register(string login, string password, string name, string email, bool isManager)
        {
            var dbUser = await _userRepository.GetByLogin(login);
            if (dbUser != null)
            {
                throw new UserAlreadyExistsException("User already exists");
            }

            CreatePasswordHash(password, out byte[] passwordHash, out byte[] passwordSalt);

            var userId = Guid.NewGuid().ToString();

            dbUser = new DbUser()
            {
                Id = userId,
                Login = login,
                Name = name,
                Email = email,
                PasswordHash = passwordHash,
                PasswordSalt = passwordSalt,
                Role = isManager ? Role.Manager.ToString() : Role.User.ToString(),
                StartDate = DateTime.UtcNow
            };

            await _userRepository.Create(dbUser);

            User user = _mapper.Map<User>(dbUser);
            return user;
        }

        public async Task<User> GetUserById(Guid id)
        {
            var dbUser = await _userRepository.GetById(id);

            User user = _mapper.Map<User>(dbUser);

            return user;
        }

        private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hmac = new HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
            }
        }

        private bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using (var hmac = new HMACSHA512(passwordSalt))
            {
                var computedHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
                return computedHash.SequenceEqual(passwordHash);
            }
        }
    }
}
