﻿using Booking.Backend.Users.Contracts;

namespace Booking.Backend.UsersWebApi.Services.Interfaces
{
    public interface IUsersService
    {
        public Task<User> Login(string login, string password);
        public Task<User> Register(string login, string password, string name, string email, bool isManager);
        public Task<User> GetUserById(Guid id);
    }
}
