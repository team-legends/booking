﻿using Booking.Backend.UsersWebApi.Repositories.Entities;
using MongoDB.Driver;

namespace Booking.Backend.UsersWebApi
{
    public class MongoDBContext
    {
        private readonly IMongoDatabase _database;

        public MongoDBContext(string connectionString, string databaseName)
        {
            var client = new MongoClient(connectionString);
            _database = client.GetDatabase(databaseName);
        }

        public IMongoCollection<User> Users => _database.GetCollection<User>("users");
    }
}
