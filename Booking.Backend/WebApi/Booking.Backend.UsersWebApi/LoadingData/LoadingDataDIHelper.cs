﻿using Booking.Backend.UsersWebApi.Mapper;
using Booking.Backend.UsersWebApi.Repositories;
using Booking.Backend.UsersWebApi.Services;
using Booking.Backend.UsersWebApi.Services.Interfaces;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace Booking.Backend.UsersWebApi.LoadingData
{
    internal static class LoadingDataDIHelper
    {
        public static void LoadData(this IServiceCollection services, IConfiguration configuration)
        {
            var dbcs = configuration.GetConnectionString("DataBaseConnectionStrings");

            if (string.IsNullOrEmpty(dbcs))
                throw new Exception("Connection string does not match the format");

            var connectionString = string.Format(
                dbcs,
                Environment.GetEnvironmentVariable("DB_SERVER"),
                Environment.GetEnvironmentVariable("DB_PORT"));

            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IUsersService, UsersService>();

            services.AddSingleton(new MongoClient(connectionString));
            services.AddSingleton<MongoDBContext>(serviceProvider =>
            {
                return new MongoDBContext(connectionString, Environment.GetEnvironmentVariable("DB_NAME"));
            });

            services.AddAutoMapper(
                typeof(UsersMappingConfig));

        }
    }
}
