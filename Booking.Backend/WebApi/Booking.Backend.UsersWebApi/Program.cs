﻿using Booking.Backend.Commons.Configuration;
using Booking.Backend.Commons.Controllers.Controllers;
using Booking.Backend.Commons.Middlewares;
using Booking.Backend.Commons.Models;
using Booking.Backend.UsersWebApi.LoadingData;
using FluentValidation.AspNetCore;
using Newtonsoft.Json.Converters;
using Serilog;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);
var configuration = builder.Configuration;

var logger = new LoggerConfiguration()
    .ReadFrom.Configuration(builder.Configuration)
    .CreateLogger();

builder.Host.UseSerilog(logger);

builder.Services.LoadData(builder.Configuration);
builder.Services
    .AddControllers()
    .AddApplicationPart(typeof(HealthCheckController).GetTypeInfo().Assembly)
    .AddNewtonsoftJson(opts => opts.SerializerSettings.Converters.Add(new StringEnumConverter()));

builder.Services.AddFluentValidation(options =>
{
    options.ImplicitlyValidateChildProperties = true;
    options.ImplicitlyValidateRootCollectionElements = true;

    options.RegisterValidatorsFromAssembly(Assembly.GetExecutingAssembly());
});

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(c =>
{
    c.IncludeXmlComments(
        Path.Combine(AppContext.BaseDirectory, $"{Assembly.GetExecutingAssembly().GetName().Name}.xml"));
    c.IncludeXmlComments(
        Path.Combine(AppContext.BaseDirectory, $"{typeof(HealthCheckController).Assembly.GetName().Name}.xml"));
    c.IncludeXmlComments(
        Path.Combine(AppContext.BaseDirectory, $"{typeof(PingResponse).Assembly.GetName().Name}.xml"));
});

builder.Services.AddSwaggerGenNewtonsoftSupport();

builder.Services.AddOptions<ApplicationInfo>()
    .Bind(builder.Configuration.GetSection("ApplicationInfo"));

var app = builder.Build();

app.UseLogging();
app.UseExceptionHandle();

if (!app.Environment.IsProduction())
{
    app.UseSwagger();
    app.UseSwaggerUI(options =>
    {
        options.DocumentTitle = "Booking.Backend.UsersWebApi API";
    });
}

app.UseHttpsRedirection();

app.MapControllers();

app.Run();
