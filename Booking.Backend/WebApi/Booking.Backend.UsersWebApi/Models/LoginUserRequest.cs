﻿namespace Booking.Backend.UsersWebApi.Models
{
    public class LoginUserRequest
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
