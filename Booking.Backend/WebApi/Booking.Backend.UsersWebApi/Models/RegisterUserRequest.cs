﻿namespace Booking.Backend.UsersWebApi.Models
{
    public class RegisterUserRequest
    {
        public string Name { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public bool IsManager { get; set; }
    }
}
