﻿using Booking.Backend.UsersWebApi.Models;
using Booking.Backend.UsersWebApi.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Booking.Backend.UsersWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUsersService _usersService;
        public UsersController(IUsersService usersService)
        {
            _usersService = usersService;
        }

        [HttpPost("Register")]
        [AllowAnonymous]
        public async Task<ActionResult> Register([FromBody] RegisterUserRequest request)
        {
            var user = await _usersService.Register(request.Login, request.Password, request.Name, request.Email, request.IsManager);

            return Ok(user);
        }

        [HttpPost("Login")]
        [AllowAnonymous]
        public async Task<ActionResult> Login([FromBody] LoginUserRequest request)
        {
            var user = await _usersService.Login(request.Login, request.Password);

            return Ok(user);
        }

        [HttpGet("GetUserById/{id:guid}")]
        [AllowAnonymous]
        public async Task<ActionResult> GetUserById(Guid id)
        {
            var user = await _usersService.GetUserById(id);

            return Ok(user);
        }
    }
}
