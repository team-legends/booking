﻿using Booking.Backend.UsersWebApi.Models;
using FluentValidation;

namespace Booking.Backend.UsersWebApi.Validation
{
    public class LoginUserRequestValidation : AbstractValidator<LoginUserRequest>
    {
        public LoginUserRequestValidation()
        {
            RuleFor(x => x.Login).NotEmpty().WithMessage("Login is a required field");
            RuleFor(x => x.Password).NotEmpty().WithMessage("Password is a required field");
            RuleFor(x => x.Password).MinimumLength(8).WithMessage("The length of the text must be at least 8 characters");
        }
    }
}
