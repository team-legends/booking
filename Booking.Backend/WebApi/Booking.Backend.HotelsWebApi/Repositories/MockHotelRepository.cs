﻿using AutoFixture;
using Booking.Backend.HotelsWebApi.Repositories.Enitities;
using Booking.Backend.StaticData;

namespace Booking.Backend.HotelsWebApi.Repositories
{
    public class MockHotelRepository : IMockHotelRepository
    {
        private readonly int _count = 5;
        private static readonly Fixture Fixture = AutoFixtureFactory.Create();
        private readonly ApplicationContext _context;
        private readonly List<GeopositionEntity> _geopositionEntities = new();
        private readonly List<AddressEntity> _addressEntities = new();
        private readonly List<HotelInfoEntity> _hotelInfoEntities = new();
        private readonly List<ContactEntity> _contactEntities = new();
        private readonly List<RoomEntity> _roomEntities = new();
        private readonly Random _rnd = new Random();

        public MockHotelRepository(ApplicationContext context)
        {
            _context = context;
        }

        public void GenerateHotelEntities()
        {
            GenerateGeopositions();
            GenerateAddresses();
            GenerateHotelInfos();
            GenerateContacts();
            GenerateRooms();

            _context.SaveChanges();
            return;
        }

        public void RemoveHotelEntities() 
        {
            //var allAddresses = _context.Addresses.ToList();
            //_context.Addresses.RemoveRange(allAddresses);
            //_context.SaveChanges();
            throw new NotImplementedException();
        }

        private void GenerateGeopositions()
        {
            for (int i = 0; i < _count; ++i)
            {
                var geoposition = Fixture
                    .Build<GeopositionEntity>()
                    .With(x => x.Latitude, _rnd.Next(1, 60))
                    .With(x => x.Longitude, _rnd.Next(0, 180))
                    .Create();

                _geopositionEntities.Add(geoposition);
                _context.Geopositions.Add(geoposition);
            }
        }

        private void GenerateAddresses()
        {
            for (int i = 0; i < _count; ++i)
            {
                var postaCode = string.Format("{0}{1}{2}{3}{4}{5}", _rnd.Next(0, 9), _rnd.Next(0, 9), _rnd.Next(0, 9),
                        _rnd.Next(0, 9), _rnd.Next(0, 9), _rnd.Next(0, 9));

                var address = Fixture
                        .Build<AddressEntity>()
                        .With(x => x.CountryName, Addresses.RusCountry)
                        .With(x => x.City, Addresses.RusCities.PickRandom())
                        .With(x => x.Street, Addresses.RusStreets.PickRandom())
                        .With(x => x.Building, _rnd.Next(1, 500).ToString())
                        .With(x => x.PostalCode, postaCode)
                        .Create();

                _addressEntities.Add(address);
                _context.Addresses.AddRange(address);
            }

            for (int i = 0; i < _count; ++i)
            {
                var postaCode = string.Format("{0}{1}{2}{3}{4}", _rnd.Next(0, 9), _rnd.Next(0, 9), _rnd.Next(0, 9),
                        _rnd.Next(0, 9), _rnd.Next(0, 9));

                var address = Fixture
                        .Build<AddressEntity>()
                        .With(x => x.CountryName, Addresses.Countries.PickRandom())
                        .With(x => x.City, Addresses.Cities.PickRandom())
                        .With(x => x.Street, Addresses.Streets.PickRandom())
                        .With(x => x.Building, _rnd.Next(1, 500).ToString())
                        .With(x => x.Region, string.Empty)
                        .With(x => x.GeopositionId, _geopositionEntities.PickRandom().Id)
                        .Create();

                _addressEntities.Add(address);
                _context.Addresses.AddRange(address);
            }
        }

        private void GenerateHotelInfos()
        {
            for (int i = 0; i < _count; ++i)
            {
                var hotelInfo = Fixture
                    .Build<HotelInfoEntity>()
                    .With(x => x.IsDelete, false)
                    .With(x => x.AddressId, _addressEntities.PickRandom().Id)
                    .With(x => x.StarsRate, _rnd.Next(2, 5))
                    .Create();

                _hotelInfoEntities.Add(hotelInfo);
                _context.HotelInfo.Add(hotelInfo);
            }
        }

        private void GenerateContacts()
        {
            for (int i = 0; i < _count; ++i)
            {
                var contact = Fixture
                    .Build<ContactEntity>()
                    .With(x => x.CurrentHotelId, _hotelInfoEntities.PickRandom().Id)
                    .Create();

                _contactEntities.Add(contact);
                _context.Contacts.Add(contact);
            }
        }

        private void GenerateRooms()
        {
            for (int i = 0; i < _count; ++i)
            {
                var room = Fixture
                    .Build<RoomEntity>()
                    .With(x => x.CurrentHotelId, _hotelInfoEntities.PickRandom().Id)
                    .With(x => x.Price, _rnd.Next(1000, 25000))
                    .With(x => x.PeopleQuantity, _rnd.Next(1, 4))
                    .Create();

                _roomEntities.Add(room);
                _context.Rooms.Add(room);
            }
        }
    }
}
