﻿namespace Booking.Backend.HotelsWebApi.Repositories
{
    public interface IMockHotelRepository
    {
        void GenerateHotelEntities();
        void RemoveHotelEntities();
    }
}
