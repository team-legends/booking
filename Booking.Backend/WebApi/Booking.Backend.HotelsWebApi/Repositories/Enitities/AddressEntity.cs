﻿using Booking.Backend.Commons.Models.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace Booking.Backend.HotelsWebApi.Repositories.Enitities
{
    [Table("Addresses")]
    public class AddressEntity : BaseEntity
    {
        public Guid GeopositionId { get; set; }
        public GeopositionEntity Geoposition { get; set; }

        public string CountryName { get; set; }

        public string City { get; set; }

        public string Region { get; set; }

        public string PostalCode { get; set; }

        public string CountryCode { get; set; }

        public string Street { get; set; }

        public string Building { get; set; }
    }
}
