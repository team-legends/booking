﻿using Booking.Backend.Commons.Models.Base;
using Booking.Backend.Hotels.Contracts.Enums;
using System.ComponentModel.DataAnnotations.Schema;

namespace Booking.Backend.HotelsWebApi.Repositories.Enitities
{
    [Table("Services")]
    public class ServiceEntity : BaseEntity
    {
        public ServiceEntity()
        {
            Hotels = new HashSet<HotelInfoEntity>();
        }
        public ServiceTypes Type { get; set; }

        public string InformationText { get; set; }
        public ICollection<HotelInfoEntity> Hotels { get; set; }
    }
}
