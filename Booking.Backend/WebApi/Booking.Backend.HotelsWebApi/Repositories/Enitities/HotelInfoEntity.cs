﻿using Booking.Backend.Commons.Models.Base;
using Booking.Backend.Hotels.Contracts.Enums;
using System.ComponentModel.DataAnnotations.Schema;

namespace Booking.Backend.HotelsWebApi.Repositories.Enitities
{
    [Table("HotelInfo")]
    public class HotelInfoEntity : BaseEntity
    {
        public HotelInfoEntity()
        {
            Services = new HashSet<ServiceEntity>();
        }
        public Guid ManagerId { get; set; }
        public ICollection<RoomEntity> Rooms { get; set; }
        public string Name { get; set; }

        public string Description { get; set; }

        public HotelTypes Type { get; set; }

        public Guid AddressId { get; set; }

        public AddressEntity Address { get; set; }

        public int StarsRate { get; set; }

        public ICollection<ContactEntity> Contacts { get; set; }

        public ICollection<ServiceEntity> Services { get; set; }
        public bool IsDelete { get; set; }
    }
}
