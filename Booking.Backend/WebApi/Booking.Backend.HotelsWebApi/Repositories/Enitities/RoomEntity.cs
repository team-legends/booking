﻿using Booking.Backend.Commons.Models.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace Booking.Backend.HotelsWebApi.Repositories.Enitities
{
    [Table("Rooms")]
    public class RoomEntity : BaseEntity
    {
        public int PeopleQuantity { get; set; }

        /// <summary>
        /// Room Price
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Room Image
        /// </summary>
        public ICollection<ImagesEntity> Images { get; set; }

        public HotelInfoEntity CurrentHotel { get; set; }
        public Guid CurrentHotelId { get; set; }
    }
}
