﻿using Booking.Backend.Commons.Models.Base;
using Booking.Backend.Hotels.Contracts.Enums;
using System.ComponentModel.DataAnnotations.Schema;

namespace Booking.Backend.HotelsWebApi.Repositories.Enitities
{
    [Table("Contacts")]
    public class ContactEntity : BaseEntity
    {
        public ContactTypes Type { get; set; }

        public string Value { get; set; }

        public string InfoText { get; set; }
   
        public HotelInfoEntity CurrentHotel { get; set; }
        public Guid CurrentHotelId { get; set; }
    }
}
