﻿using Booking.Backend.Commons.Models.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace Booking.Backend.HotelsWebApi.Repositories.Enitities
{
    [Table("Hotels")]
    public class HotelEntity : BaseEntity
    {
        public HotelInfoEntity HotelInfo { get; set; }
    }
}
