﻿using Booking.Backend.Commons.Models.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace Booking.Backend.HotelsWebApi.Repositories.Enitities
{
    [Table("Geopositions")]
    public class GeopositionEntity : BaseEntity
    {
        public double Latitude { get; set; }

        public double Longitude { get; set; }
    }
}
