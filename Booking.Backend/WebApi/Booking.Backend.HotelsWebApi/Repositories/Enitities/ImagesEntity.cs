﻿using Booking.Backend.Commons.Models.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace Booking.Backend.HotelsWebApi.Repositories.Enitities
{
    [Table("Images")]
    public class ImagesEntity : BaseEntity
    {
        public string Image { get; set; }
    }
}
