﻿using Booking.Backend.Hotels.Contracts;
using Booking.Backend.HotelsWebApi.Repositories.Enitities;
using Microsoft.EntityFrameworkCore;

namespace Booking.Backend.HotelsWebApi.Repositories
{
    public class HotelRepository : IHotelRepository
    {
        public HotelRepository(ApplicationContext context)
        {
            _context = context;
        }

        public List<HotelInfoEntity> GetFilteredHotels(string destination, HotelFilterOption option)
        {
            var hotelEntity = _context.HotelInfo.Include(h => h.Contacts)
                .Include(h => h.Rooms)
                .Include(h => h.Services)
                .Include(h => h.Address).Include(h => h.Address.Geoposition).ToList();

            var hotels = hotelEntity
                .Where(h => h.Address.CountryName == destination).ToList();

            foreach (var hotel in hotels)
            {
                hotel.Rooms = hotel.Rooms.Where(r => r.PeopleQuantity >= option.Room).ToList();
            }

            return hotels;
        }

        public HotelInfoEntity GetHotelInfo(Guid id)
        {
            var hotelEntity = _context.HotelInfo
                .Include(h => h.Contacts)
                .Include(h => h.Rooms)
                .Include(h => h.Services)
                .Include(h => h.Address);
            var result = hotelEntity.FirstOrDefault(h => h.Id == id);
            return result;
        }

        public void AddHotel(HotelInfoEntity hotelInfoEntity)
        {
            _context.HotelInfo.Add(hotelInfoEntity);
            _context.Contacts.AddRange(hotelInfoEntity.Contacts);
            _context.Rooms.AddRange(hotelInfoEntity.Rooms);
            _context.Services.AddRange(hotelInfoEntity.Services);
            _context.SaveChanges();
        }

        public void UpdateHotel(HotelInfoEntity hotelInfoEntity)
        {
            _context.HotelInfo.Update(hotelInfoEntity);
            foreach (var contact in hotelInfoEntity.Contacts)
            {
                var cont = _context.Contacts.FirstOrDefault(c => c.Id == contact.Id);
                if(cont == null)
                {
                    _context.Contacts.Add(contact);
                }
                else
                {
                    cont = contact;
                }
            }

            foreach (var room in hotelInfoEntity.Rooms)
            {
                var roomdb = _context.Rooms.FirstOrDefault(c => c.Id == room.Id);
                if (roomdb == null)
                {
                    _context.Rooms.Add(room);
                }
                else
                {
                    roomdb = room;
                }
            }

            foreach (var service in hotelInfoEntity.Services)
            {
                var servicedb = _context.Services.FirstOrDefault(c => c.Id == service.Id);
                if (servicedb == null)
                {
                    _context.Services.Add(service);
                }
                else
                {
                    servicedb = service;
                }
            }
            _context.SaveChanges();
        }

        private readonly ApplicationContext _context;
    }
}
