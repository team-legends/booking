﻿using Booking.Backend.Hotels.Contracts;
using Booking.Backend.HotelsWebApi.Repositories.Enitities;

namespace Booking.Backend.HotelsWebApi.Repositories
{
    public interface IHotelRepository
    {
        HotelInfoEntity GetHotelInfo(Guid id);

        List<HotelInfoEntity> GetFilteredHotels(string destination, HotelFilterOption option);

        void AddHotel(HotelInfoEntity hotelInfo);

        void UpdateHotel(HotelInfoEntity hotelInfo);
    }
}
