﻿using AutoMapper;
using Booking.Backend.Hotels.Contracts;
using Booking.Backend.HotelsWebApi.Repositories.Enitities;
using System.Collections.ObjectModel;

namespace Booking.Backend.HotelsWebApi
{
    public class ApplicationMappingProfile : Profile
    {
        private HotelInfoEntity _hotelEntity;
        public ApplicationMappingProfile()
        {
            CreateMap<HotelInfoEntity, Hotel>().ReverseMap();
            CreateMap<HotelInfo, HotelInfoEntity>().ReverseMap();
            CreateMap<Address, AddressEntity>().ReverseMap();
            CreateMap<Contact, ContactEntity>().ReverseMap();
            CreateMap<Service, ServiceEntity>().ReverseMap();
            CreateMap<Room, RoomEntity>()
                .ForMember(dest => dest.Images,
                opt => opt.MapFrom(src => src.Images.Select(x => new ImagesEntity { Image = x }).ToList()));
            CreateMap<RoomEntity, Room>()
               .ForMember(dest => dest.Images,
               opt => opt.MapFrom(src => src.Images.Select(x => x.Image).ToList()));
            CreateMap<Geoposition, GeopositionEntity>().ReverseMap();
        }
    }
}
