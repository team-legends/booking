﻿using Booking.Backend.Hotels.Contracts;
using Booking.Backend.HotelsWebApi.Extension;
using Booking.Backend.HotelsWebApi.Repositories.Enitities;
using Microsoft.EntityFrameworkCore;

namespace Booking.Backend.HotelsWebApi
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
        }

        public DbSet<GeopositionEntity> Geopositions { get; set; }
        public DbSet<AddressEntity> Addresses { get; set; }
        public DbSet<HotelInfoEntity> HotelInfo { get; set; }
        public DbSet<HotelEntity> Hotels { get; set; }
        public DbSet<ContactEntity> Contacts { get; set; }
        public DbSet<RoomEntity> Rooms { get; set; }
        public DbSet<ServiceEntity> Services { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<ContactEntity>()
            //    .HasOne<HotelInfoEntity>(s => s.CurrentHotel)
            //    .WithMany(g => g.Contacts)
            //    .HasForeignKey(s => s.CurrentHotelId);

            //modelBuilder.Entity<RoomEntity>()
            //    .HasOne<HotelInfoEntity>(s => s.CurrentHotel)
            //    .WithMany(g => g.Rooms)
            //    .HasForeignKey(s => s.CurrentHotelId);

            //modelBuilder.Entity<HotelInfoEntity>()
            //    .HasMany<ServiceEntity>(s => s.Services)
            //    .WithMany(c => c.Hotels);
        }
    }
}
