﻿using Booking.Backend.HotelsWebApi.Repositories.Enitities;

namespace Booking.Backend.HotelsWebApi.Extension
{
    public class DataSeeder
    {
        public static void Seed(ApplicationContext context)
        {
            if (!context.Geopositions.Any())
            {
                var geo1 = new GeopositionEntity()
                {
                    Id = new Guid("c577c7ec-b925-4b69-a797-753728222e5f"),
                    Latitude = 25.1415843,
                    Longitude = 55.1912777
                };
                var geo2 = new GeopositionEntity()
                {
                    Id = new Guid("71183710-8ab5-45e1-9185-a6d70beea0b2"),
                    Latitude = 25.1411914,
                    Longitude = 55.1852468
                };
                var geo3 = new GeopositionEntity()
                {
                    Id = new Guid("e05d3e77-c4c4-4e0c-9184-3dbcf82a4192"),
                    Latitude = 25.1366368,
                    Longitude = 55.1870237
                };

                var geo = new List<GeopositionEntity>() { geo1, geo2, geo3 };
                context.AddRange(geo);
                context.SaveChanges();

                var add1 = new AddressEntity()
                {
                    Id = new Guid("4ae50770-4e6a-41c8-9c18-756c1b5cea2f"),
                    City = "Dubai",
                    Building = "House",
                    CountryCode = "OAE",
                    CountryName = "United Arab Emirates",
                    GeopositionId = geo1.Id,
                    PostalCode = "9914",
                    Region = "Jumeirah",
                    Street = "Umm Suqeim 3"
                };
                var add2 = new AddressEntity()
                {
                    Id = new Guid("6fbd2060-e582-4ca1-a5dc-08db9de8389e"),
                    City = "Dubai",
                    Building = "House",
                    CountryCode = "OAE",
                    CountryName = "United Arab Emirates",
                    GeopositionId = geo2.Id,
                    PostalCode = "9914",
                    Region = "Jumeirah",
                    Street = "Umm Suqeim 3"
                };
                var add3 = new AddressEntity()
                {
                    Id = new Guid("1c9a0e09-3279-407b-b01f-9d032d208202"),
                    City = "Dubai",
                    Building = "House",
                    CountryCode = "OAE",
                    CountryName = "United Arab Emirates",
                    GeopositionId = geo3.Id,
                    PostalCode = "9914",
                    Region = "Jumeirah",
                    Street = "Umm Suqeim 3"
                };

                var addr = new List<AddressEntity>() { add1, add2, add3 };
                context.AddRange(addr);
                context.SaveChanges();

                var hie1 = new HotelInfoEntity()
                {
                    Id = new Guid("cdfc133d-00e0-4c84-b044-fdb5560ed70b"),
                    Name = "JUMEIRAH BEACH HOTEL",
                    Description = "JUMEIRAH BEACH HOTEL",
                    Type = Hotels.Contracts.Enums.HotelTypes.RESORT,
                    AddressId = add1.Id,
                    StarsRate = 5,
                    IsDelete = false
                };
                var hie2 = new HotelInfoEntity()
                {
                    Id = new Guid("5e7332c2-c43b-42eb-8983-c026a67cfa97"),
                    Name = "BURJ AL ARAB JUMEIRAH",
                    Description = "BURJ AL ARAB JUMEIRAH",
                    Type = Hotels.Contracts.Enums.HotelTypes.HOTEL,
                    AddressId = add2.Id,
                    StarsRate = 5,
                    IsDelete = false
                };
                var hie3 = new HotelInfoEntity()
                {
                    Id = new Guid("4a39c092-d0ab-46a8-a61d-baf9198f43fd"),
                    Name = "Jumeirah Al Naseem",
                    Description = "Jumeirah Al Naseem",
                    Type = Hotels.Contracts.Enums.HotelTypes.APARTMENTS,
                    AddressId = add3.Id,
                    StarsRate = 5,
                    IsDelete = false
                };

                var hie = new List<HotelInfoEntity>() { hie1, hie2, hie3 };
                context.AddRange(hie);
                context.SaveChanges();

                var ctn1 = new ContactEntity()
                {
                    Id = new Guid("665d89b2-b43d-4a8d-acf9-d1ae2dde137c"),
                    Type = Hotels.Contracts.Enums.ContactTypes.PHONE,
                    Value = "+97143480000",
                    InfoText = "+97143480000",
                    CurrentHotelId = hie1.Id
                };
                var ctn2 = new ContactEntity()
                {
                    Id = new Guid("4eae1e82-5275-4eaa-a951-03a4415f0e55"),
                    Type = Hotels.Contracts.Enums.ContactTypes.WHATSUPP,
                    Value = "+97143017777",
                    InfoText = "+97143017777",
                    CurrentHotelId = hie2.Id
                };
                var ctn3 = new ContactEntity()
                {
                    Id = new Guid("2d977a5a-ee72-433a-a7c8-17fb71bd3209"),
                    Type = Hotels.Contracts.Enums.ContactTypes.TELEGRAM,
                    Value = "+9718006234628",
                    InfoText = "+9718006234628",
                    CurrentHotelId = hie3.Id
                };

                var ctn = new List<ContactEntity>() { ctn1, ctn2, ctn3 };
                context.AddRange(ctn);
                context.SaveChanges();

                var srv1 = new ServiceEntity()
                {
                    Id = new Guid("fbbbe53b-3e44-4199-a234-8144d22c794e"),
                    Type = Hotels.Contracts.Enums.ServiceTypes.WIFI,
                    InformationText = "WIFI"
                };
                var srv2 = new ServiceEntity()
                {
                    Id = new Guid("7a51b18d-1c12-410a-acfa-5ee195ceae26"),
                    Type = Hotels.Contracts.Enums.ServiceTypes.AIRCONDITIONER,
                    InformationText = "AIRCONDITIONER"
                };
                var srv3 = new ServiceEntity()
                {
                    Id = new Guid("233d19b3-a713-4ff3-a4da-af806e6798ca"),
                    Type = Hotels.Contracts.Enums.ServiceTypes.KITCHEN,
                    InformationText = "KITCHEN"
                };

                var srv = new List<ServiceEntity>() { srv1, srv2, srv3 };
                context.AddRange(srv);
                context.SaveChanges();

                var rm1 = new RoomEntity()
                {
                    Id = new Guid("1c79b5a1-0c82-40e4-b198-c67e58d43919"),
                    PeopleQuantity = 2,
                    Price = 50,
                    CurrentHotelId = hie1.Id
                };
                var rm2 = new RoomEntity()
                {
                    Id = new Guid("629dbb61-d329-4f98-bd29-4630d5cc37ab"),
                    PeopleQuantity = 4,
                    Price = 100,
                    CurrentHotelId = hie2.Id
                };
                var rm3 = new RoomEntity()
                {
                    Id = new Guid("99216050-31c5-437f-b3a6-2b6a6344c7ec"),
                    PeopleQuantity = 1,
                    Price = 40,
                    CurrentHotelId = hie3.Id
                };

                var rm = new List<RoomEntity>() { rm1, rm2, rm3 };
                context.AddRange(rm);
                context.SaveChanges();

                /*
                var htl1 = new HotelEntity()
                {
                    Id = new Guid("3028324d-b284-463b-b7b8-182f8198a183"),
                    ManagerId = ctn1.Id,
                    HotelInfoId = hie1.Id
                };
                var htl2 = new HotelEntity()
                {
                    Id = new Guid("1c4879f6-d2f3-41f7-a7ec-a171959b68c2"),
                    ManagerId = ctn2.Id,
                    HotelInfoId = hie2.Id
                };
                var htl3 = new HotelEntity()
                {
                    Id = new Guid("ea8abe4c-507a-4990-98e6-9de7e631b0be"),
                    HotelInfoId = hie3.Id,
                    ManagerId = ctn3.Id
                };

                var htl = new List<HotelEntity>() { htl1, htl2, htl3 };
                context.AddRange(htl);
                context.SaveChanges();
                */
            }
        }
    }
}
