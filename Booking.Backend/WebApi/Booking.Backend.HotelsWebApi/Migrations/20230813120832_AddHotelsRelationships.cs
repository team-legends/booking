﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Booking.Backend.HotelsWebApi.Migrations
{
    /// <inheritdoc />
    public partial class AddHotelsRelationships : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Rooms_HotelInfo_HotelInfoEntityId",
                table: "Rooms");

            migrationBuilder.DropForeignKey(
                name: "FK_Services_HotelInfo_HotelInfoEntityId",
                table: "Services");

            migrationBuilder.DropIndex(
                name: "IX_Services_HotelInfoEntityId",
                table: "Services");

            migrationBuilder.DropIndex(
                name: "IX_Rooms_HotelInfoEntityId",
                table: "Rooms");

            migrationBuilder.DropColumn(
                name: "HotelInfoEntityId",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "HotelInfoEntityId",
                table: "Rooms");

            migrationBuilder.AddColumn<Guid>(
                name: "CurrentHotelId",
                table: "Rooms",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateTable(
                name: "HotelInfoEntityServiceEntity",
                columns: table => new
                {
                    HotelsId = table.Column<Guid>(type: "uuid", nullable: false),
                    ServicesId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HotelInfoEntityServiceEntity", x => new { x.HotelsId, x.ServicesId });
                    table.ForeignKey(
                        name: "FK_HotelInfoEntityServiceEntity_HotelInfo_HotelsId",
                        column: x => x.HotelsId,
                        principalTable: "HotelInfo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_HotelInfoEntityServiceEntity_Services_ServicesId",
                        column: x => x.ServicesId,
                        principalTable: "Services",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Rooms_CurrentHotelId",
                table: "Rooms",
                column: "CurrentHotelId");

            migrationBuilder.CreateIndex(
                name: "IX_HotelInfoEntityServiceEntity_ServicesId",
                table: "HotelInfoEntityServiceEntity",
                column: "ServicesId");

            migrationBuilder.AddForeignKey(
                name: "FK_Rooms_HotelInfo_CurrentHotelId",
                table: "Rooms",
                column: "CurrentHotelId",
                principalTable: "HotelInfo",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Rooms_HotelInfo_CurrentHotelId",
                table: "Rooms");

            migrationBuilder.DropTable(
                name: "HotelInfoEntityServiceEntity");

            migrationBuilder.DropIndex(
                name: "IX_Rooms_CurrentHotelId",
                table: "Rooms");

            migrationBuilder.DropColumn(
                name: "CurrentHotelId",
                table: "Rooms");

            migrationBuilder.AddColumn<Guid>(
                name: "HotelInfoEntityId",
                table: "Services",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "HotelInfoEntityId",
                table: "Rooms",
                type: "uuid",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Services_HotelInfoEntityId",
                table: "Services",
                column: "HotelInfoEntityId");

            migrationBuilder.CreateIndex(
                name: "IX_Rooms_HotelInfoEntityId",
                table: "Rooms",
                column: "HotelInfoEntityId");

            migrationBuilder.AddForeignKey(
                name: "FK_Rooms_HotelInfo_HotelInfoEntityId",
                table: "Rooms",
                column: "HotelInfoEntityId",
                principalTable: "HotelInfo",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Services_HotelInfo_HotelInfoEntityId",
                table: "Services",
                column: "HotelInfoEntityId",
                principalTable: "HotelInfo",
                principalColumn: "Id");
        }
    }
}
