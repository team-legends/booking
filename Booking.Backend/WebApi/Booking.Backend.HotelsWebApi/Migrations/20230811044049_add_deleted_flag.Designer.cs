﻿// <auto-generated />
using System;
using System.Collections.Generic;
using Booking.Backend.HotelsWebApi;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace Booking.Backend.HotelsWebApi.Migrations
{
    [DbContext(typeof(ApplicationContext))]
    [Migration("20230811044049_add_deleted_flag")]
    partial class add_deleted_flag
    {
        /// <inheritdoc />
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.5")
                .HasAnnotation("Relational:MaxIdentifierLength", 63);

            NpgsqlModelBuilderExtensions.UseIdentityByDefaultColumns(modelBuilder);

            modelBuilder.Entity("Booking.Backend.Hotels.Contracts.Room", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uuid");

                    b.Property<Guid?>("HotelInfoEntityId")
                        .HasColumnType("uuid");

                    b.Property<List<string>>("Images")
                        .IsRequired()
                        .HasColumnType("text[]");

                    b.Property<int>("PeopleQuantity")
                        .HasColumnType("integer");

                    b.Property<decimal>("Price")
                        .HasColumnType("numeric");

                    b.HasKey("Id");

                    b.HasIndex("HotelInfoEntityId");

                    b.ToTable("Room");
                });

            modelBuilder.Entity("Booking.Backend.HotelsWebApi.Repositories.Enitities.AddressEntity", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uuid");

                    b.Property<string>("Building")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string>("City")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string>("CountryCode")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string>("CountryName")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<Guid>("GeopositionId")
                        .HasColumnType("uuid");

                    b.Property<string>("PostalCode")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string>("Region")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string>("Street")
                        .IsRequired()
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.HasIndex("GeopositionId");

                    b.ToTable("Addresses");
                });

            modelBuilder.Entity("Booking.Backend.HotelsWebApi.Repositories.Enitities.ContactEntity", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uuid");

                    b.Property<Guid?>("HotelInfoEntityId")
                        .HasColumnType("uuid");

                    b.Property<string>("InfoText")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<int>("Type")
                        .HasColumnType("integer");

                    b.Property<string>("Value")
                        .IsRequired()
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.HasIndex("HotelInfoEntityId");

                    b.ToTable("Contacts");
                });

            modelBuilder.Entity("Booking.Backend.HotelsWebApi.Repositories.Enitities.GeopositionEntity", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uuid");

                    b.Property<double>("Latitude")
                        .HasColumnType("double precision");

                    b.Property<double>("Longitude")
                        .HasColumnType("double precision");

                    b.HasKey("Id");

                    b.ToTable("Geopositions");
                });

            modelBuilder.Entity("Booking.Backend.HotelsWebApi.Repositories.Enitities.HotelInfoEntity", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uuid");

                    b.Property<Guid>("AddressId")
                        .HasColumnType("uuid");

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<bool>("IsDelete")
                        .HasColumnType("boolean");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<int>("StarsRate")
                        .HasColumnType("integer");

                    b.Property<int>("Type")
                        .HasColumnType("integer");

                    b.HasKey("Id");

                    b.HasIndex("AddressId");

                    b.ToTable("HotelInfo");
                });

            modelBuilder.Entity("Booking.Backend.HotelsWebApi.Repositories.Enitities.ServiceEntity", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uuid");

                    b.Property<Guid?>("HotelInfoEntityId")
                        .HasColumnType("uuid");

                    b.Property<string>("InformationText")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<int>("Type")
                        .HasColumnType("integer");

                    b.HasKey("Id");

                    b.HasIndex("HotelInfoEntityId");

                    b.ToTable("Services");
                });

            modelBuilder.Entity("Booking.Backend.Hotels.Contracts.Room", b =>
                {
                    b.HasOne("Booking.Backend.HotelsWebApi.Repositories.Enitities.HotelInfoEntity", null)
                        .WithMany("Rooms")
                        .HasForeignKey("HotelInfoEntityId");
                });

            modelBuilder.Entity("Booking.Backend.HotelsWebApi.Repositories.Enitities.AddressEntity", b =>
                {
                    b.HasOne("Booking.Backend.HotelsWebApi.Repositories.Enitities.GeopositionEntity", "Geoposition")
                        .WithMany()
                        .HasForeignKey("GeopositionId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Geoposition");
                });

            modelBuilder.Entity("Booking.Backend.HotelsWebApi.Repositories.Enitities.ContactEntity", b =>
                {
                    b.HasOne("Booking.Backend.HotelsWebApi.Repositories.Enitities.HotelInfoEntity", null)
                        .WithMany("Contacts")
                        .HasForeignKey("HotelInfoEntityId");
                });

            modelBuilder.Entity("Booking.Backend.HotelsWebApi.Repositories.Enitities.HotelInfoEntity", b =>
                {
                    b.HasOne("Booking.Backend.HotelsWebApi.Repositories.Enitities.AddressEntity", "Address")
                        .WithMany()
                        .HasForeignKey("AddressId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Address");
                });

            modelBuilder.Entity("Booking.Backend.HotelsWebApi.Repositories.Enitities.ServiceEntity", b =>
                {
                    b.HasOne("Booking.Backend.HotelsWebApi.Repositories.Enitities.HotelInfoEntity", null)
                        .WithMany("Services")
                        .HasForeignKey("HotelInfoEntityId");
                });

            modelBuilder.Entity("Booking.Backend.HotelsWebApi.Repositories.Enitities.HotelInfoEntity", b =>
                {
                    b.Navigation("Contacts");

                    b.Navigation("Rooms");

                    b.Navigation("Services");
                });
#pragma warning restore 612, 618
        }
    }
}
