﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Booking.Backend.HotelsWebApi.Migrations
{
    /// <inheritdoc />
    public partial class FixContacts : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Contacts_HotelInfo_HotelInfoEntityId",
                table: "Contacts");

            migrationBuilder.DropIndex(
                name: "IX_Contacts_HotelInfoEntityId",
                table: "Contacts");

            migrationBuilder.DropColumn(
                name: "HotelInfoEntityId",
                table: "Contacts");

            migrationBuilder.AddColumn<Guid>(
                name: "CurrentHotelId",
                table: "Contacts",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_Contacts_CurrentHotelId",
                table: "Contacts",
                column: "CurrentHotelId");

            migrationBuilder.AddForeignKey(
                name: "FK_Contacts_HotelInfo_CurrentHotelId",
                table: "Contacts",
                column: "CurrentHotelId",
                principalTable: "HotelInfo",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Contacts_HotelInfo_CurrentHotelId",
                table: "Contacts");

            migrationBuilder.DropIndex(
                name: "IX_Contacts_CurrentHotelId",
                table: "Contacts");

            migrationBuilder.DropColumn(
                name: "CurrentHotelId",
                table: "Contacts");

            migrationBuilder.AddColumn<Guid>(
                name: "HotelInfoEntityId",
                table: "Contacts",
                type: "uuid",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Contacts_HotelInfoEntityId",
                table: "Contacts",
                column: "HotelInfoEntityId");

            migrationBuilder.AddForeignKey(
                name: "FK_Contacts_HotelInfo_HotelInfoEntityId",
                table: "Contacts",
                column: "HotelInfoEntityId",
                principalTable: "HotelInfo",
                principalColumn: "Id");
        }
    }
}
