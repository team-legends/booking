﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Booking.Backend.HotelsWebApi.Migrations
{
    /// <inheritdoc />
    public partial class add_deleted_flag : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Contacts_Hotels_HotelInfoEntityId",
                table: "Contacts");

            migrationBuilder.DropForeignKey(
                name: "FK_Hotels_Addresses_AddressId",
                table: "Hotels");

            migrationBuilder.DropForeignKey(
                name: "FK_Services_Hotels_HotelInfoEntityId",
                table: "Services");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Hotels",
                table: "Hotels");

            migrationBuilder.RenameTable(
                name: "Hotels",
                newName: "HotelInfo");

            migrationBuilder.RenameIndex(
                name: "IX_Hotels_AddressId",
                table: "HotelInfo",
                newName: "IX_HotelInfo_AddressId");

            migrationBuilder.AddColumn<bool>(
                name: "IsDelete",
                table: "HotelInfo",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddPrimaryKey(
                name: "PK_HotelInfo",
                table: "HotelInfo",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "Room",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    PeopleQuantity = table.Column<int>(type: "integer", nullable: false),
                    Price = table.Column<decimal>(type: "numeric", nullable: false),
                    Images = table.Column<List<string>>(type: "text[]", nullable: false),
                    HotelInfoEntityId = table.Column<Guid>(type: "uuid", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Room", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Room_HotelInfo_HotelInfoEntityId",
                        column: x => x.HotelInfoEntityId,
                        principalTable: "HotelInfo",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_Room_HotelInfoEntityId",
                table: "Room",
                column: "HotelInfoEntityId");

            migrationBuilder.AddForeignKey(
                name: "FK_Contacts_HotelInfo_HotelInfoEntityId",
                table: "Contacts",
                column: "HotelInfoEntityId",
                principalTable: "HotelInfo",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_HotelInfo_Addresses_AddressId",
                table: "HotelInfo",
                column: "AddressId",
                principalTable: "Addresses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Services_HotelInfo_HotelInfoEntityId",
                table: "Services",
                column: "HotelInfoEntityId",
                principalTable: "HotelInfo",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Contacts_HotelInfo_HotelInfoEntityId",
                table: "Contacts");

            migrationBuilder.DropForeignKey(
                name: "FK_HotelInfo_Addresses_AddressId",
                table: "HotelInfo");

            migrationBuilder.DropForeignKey(
                name: "FK_Services_HotelInfo_HotelInfoEntityId",
                table: "Services");

            migrationBuilder.DropTable(
                name: "Room");

            migrationBuilder.DropPrimaryKey(
                name: "PK_HotelInfo",
                table: "HotelInfo");

            migrationBuilder.DropColumn(
                name: "IsDelete",
                table: "HotelInfo");

            migrationBuilder.RenameTable(
                name: "HotelInfo",
                newName: "Hotels");

            migrationBuilder.RenameIndex(
                name: "IX_HotelInfo_AddressId",
                table: "Hotels",
                newName: "IX_Hotels_AddressId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Hotels",
                table: "Hotels",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Contacts_Hotels_HotelInfoEntityId",
                table: "Contacts",
                column: "HotelInfoEntityId",
                principalTable: "Hotels",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Hotels_Addresses_AddressId",
                table: "Hotels",
                column: "AddressId",
                principalTable: "Addresses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Services_Hotels_HotelInfoEntityId",
                table: "Services",
                column: "HotelInfoEntityId",
                principalTable: "Hotels",
                principalColumn: "Id");
        }
    }
}
