﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Booking.Backend.HotelsWebApi.Migrations
{
    /// <inheritdoc />
    public partial class Updatetablenames : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AddressEntity_GeopositionEntity_GeopositionId",
                table: "AddressEntity");

            migrationBuilder.DropForeignKey(
                name: "FK_ContactEntity_HotelInfo_HotelInfoEntityId",
                table: "ContactEntity");

            migrationBuilder.DropForeignKey(
                name: "FK_HotelInfo_AddressEntity_AddressId",
                table: "HotelInfo");

            migrationBuilder.DropForeignKey(
                name: "FK_ServiceEntity_HotelInfo_HotelInfoEntityId",
                table: "ServiceEntity");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ServiceEntity",
                table: "ServiceEntity");

            migrationBuilder.DropPrimaryKey(
                name: "PK_HotelInfo",
                table: "HotelInfo");

            migrationBuilder.DropPrimaryKey(
                name: "PK_GeopositionEntity",
                table: "GeopositionEntity");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ContactEntity",
                table: "ContactEntity");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AddressEntity",
                table: "AddressEntity");

            migrationBuilder.RenameTable(
                name: "ServiceEntity",
                newName: "Services");

            migrationBuilder.RenameTable(
                name: "HotelInfo",
                newName: "Hotels");

            migrationBuilder.RenameTable(
                name: "GeopositionEntity",
                newName: "Geopositions");

            migrationBuilder.RenameTable(
                name: "ContactEntity",
                newName: "Contacts");

            migrationBuilder.RenameTable(
                name: "AddressEntity",
                newName: "Addresses");

            migrationBuilder.RenameIndex(
                name: "IX_ServiceEntity_HotelInfoEntityId",
                table: "Services",
                newName: "IX_Services_HotelInfoEntityId");

            migrationBuilder.RenameIndex(
                name: "IX_HotelInfo_AddressId",
                table: "Hotels",
                newName: "IX_Hotels_AddressId");

            migrationBuilder.RenameIndex(
                name: "IX_ContactEntity_HotelInfoEntityId",
                table: "Contacts",
                newName: "IX_Contacts_HotelInfoEntityId");

            migrationBuilder.RenameIndex(
                name: "IX_AddressEntity_GeopositionId",
                table: "Addresses",
                newName: "IX_Addresses_GeopositionId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Services",
                table: "Services",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Hotels",
                table: "Hotels",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Geopositions",
                table: "Geopositions",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Contacts",
                table: "Contacts",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Addresses",
                table: "Addresses",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Addresses_Geopositions_GeopositionId",
                table: "Addresses",
                column: "GeopositionId",
                principalTable: "Geopositions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Contacts_Hotels_HotelInfoEntityId",
                table: "Contacts",
                column: "HotelInfoEntityId",
                principalTable: "Hotels",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Hotels_Addresses_AddressId",
                table: "Hotels",
                column: "AddressId",
                principalTable: "Addresses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Services_Hotels_HotelInfoEntityId",
                table: "Services",
                column: "HotelInfoEntityId",
                principalTable: "Hotels",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Addresses_Geopositions_GeopositionId",
                table: "Addresses");

            migrationBuilder.DropForeignKey(
                name: "FK_Contacts_Hotels_HotelInfoEntityId",
                table: "Contacts");

            migrationBuilder.DropForeignKey(
                name: "FK_Hotels_Addresses_AddressId",
                table: "Hotels");

            migrationBuilder.DropForeignKey(
                name: "FK_Services_Hotels_HotelInfoEntityId",
                table: "Services");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Services",
                table: "Services");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Hotels",
                table: "Hotels");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Geopositions",
                table: "Geopositions");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Contacts",
                table: "Contacts");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Addresses",
                table: "Addresses");

            migrationBuilder.RenameTable(
                name: "Services",
                newName: "ServiceEntity");

            migrationBuilder.RenameTable(
                name: "Hotels",
                newName: "HotelInfo");

            migrationBuilder.RenameTable(
                name: "Geopositions",
                newName: "GeopositionEntity");

            migrationBuilder.RenameTable(
                name: "Contacts",
                newName: "ContactEntity");

            migrationBuilder.RenameTable(
                name: "Addresses",
                newName: "AddressEntity");

            migrationBuilder.RenameIndex(
                name: "IX_Services_HotelInfoEntityId",
                table: "ServiceEntity",
                newName: "IX_ServiceEntity_HotelInfoEntityId");

            migrationBuilder.RenameIndex(
                name: "IX_Hotels_AddressId",
                table: "HotelInfo",
                newName: "IX_HotelInfo_AddressId");

            migrationBuilder.RenameIndex(
                name: "IX_Contacts_HotelInfoEntityId",
                table: "ContactEntity",
                newName: "IX_ContactEntity_HotelInfoEntityId");

            migrationBuilder.RenameIndex(
                name: "IX_Addresses_GeopositionId",
                table: "AddressEntity",
                newName: "IX_AddressEntity_GeopositionId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ServiceEntity",
                table: "ServiceEntity",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_HotelInfo",
                table: "HotelInfo",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_GeopositionEntity",
                table: "GeopositionEntity",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ContactEntity",
                table: "ContactEntity",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AddressEntity",
                table: "AddressEntity",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_AddressEntity_GeopositionEntity_GeopositionId",
                table: "AddressEntity",
                column: "GeopositionId",
                principalTable: "GeopositionEntity",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ContactEntity_HotelInfo_HotelInfoEntityId",
                table: "ContactEntity",
                column: "HotelInfoEntityId",
                principalTable: "HotelInfo",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_HotelInfo_AddressEntity_AddressId",
                table: "HotelInfo",
                column: "AddressId",
                principalTable: "AddressEntity",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceEntity_HotelInfo_HotelInfoEntityId",
                table: "ServiceEntity",
                column: "HotelInfoEntityId",
                principalTable: "HotelInfo",
                principalColumn: "Id");
        }
    }
}
