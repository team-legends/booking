﻿using Booking.Backend.HotelsWebApi.Services.IServices;
using Microsoft.AspNetCore.Mvc;

namespace Booking.Backend.HotelsWebApi.Controllers
{
    public class TestDataController : ControllerBase
    {
        private readonly IMockService _mockService;

        public TestDataController(IMockService mockService)
        {
            _mockService = mockService;
        }

        /// <summary>
        /// Generate new test data
        /// </summary>
        /// <returns></returns>
        [HttpPut("generate")]
        public ActionResult GenerateData()
        {
            _mockService.GenerateMockHotels();
            return Ok();
        }

        /// <summary>
        /// Generate new hotels
        /// </summary>
        /// <returns></returns>
        [HttpDelete("removeAll")]
        public ActionResult DeleteAllHotels()
        {
            _mockService.DeleteMockHotels();
            return Ok();
        }
    }
}
