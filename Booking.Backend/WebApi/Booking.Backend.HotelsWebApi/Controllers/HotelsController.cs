﻿using Booking.Backend.Hotels.Contracts;
using Booking.Backend.HotelsWebApi.Models.Hotels;
using Booking.Backend.HotelsWebApi.Services.IServices;
using Microsoft.AspNetCore.Mvc;

namespace Booking.Backend.HotelsWebApi.Controllers
{
    /// <summary>
    /// Hotels management
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class HotelsController : ControllerBase
    {
        private readonly IHotelService _hotelService;

        public HotelsController(IHotelService hotelService)
        {
            _hotelService = hotelService;
        }

        /// <summary>
        /// Filtered list of hotels
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("GetFilteredHotels")]
        public ActionResult<List<HotelInfo>> GetFilteredHotels([FromBody] GetFilteredHotelsRequest request)
        {
            var result = _hotelService.GetFilteredHotels(request);

            return Ok(result);
        }

        /// <summary>
        /// Create new hotel
        /// </summary>
        /// <param name="request">Request data</param>
        [HttpPost("Create")]
        public ActionResult<Guid> Create([FromBody] AddHotelRequestBody request)
        {
            var createdId = _hotelService.CreateHotel(request.HotelInfo);
            return Ok(createdId);
        }

        /// <summary>
        /// Get hotel information
        /// </summary>
        /// <param name="id">Hotel id</param>
        //[ResponseCache(Location = ResponseCacheLocation.Client, Duration = 60)]
        [HttpGet("GetHotelInfo/{id:guid}", Name = "GetHotelInfo")]
        async public Task<ActionResult> GetHotelInfo(Guid id)
        {
            var hotel = _hotelService.GetHotelInfo(id);
            return Ok(hotel);
        }

        /// <summary>
        /// Edit Hotel
        /// </summary>
        /// <returns></returns>
        [HttpPut("EditHotel/{id:guid}")]
        async public Task<ActionResult> EditHotel([FromBody] Hotel hotel)
        {
            _hotelService.UpdateHotel(hotel);
            return Ok();
        }

        /// <summary>
        /// Mark hotes as deleted
        /// </summary>
        /// <returns></returns>
        [HttpPut("MarkDeleteHotel/{id:guid}")]
        public ActionResult MarkDeleted(Guid id)
        {
            _hotelService.DeleteHotel(id);
            return Ok();
        }
    }
}
