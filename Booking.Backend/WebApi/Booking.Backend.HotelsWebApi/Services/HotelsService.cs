﻿using AutoMapper;
using Booking.Backend.Hotels.Contracts;
using Booking.Backend.HotelsWebApi.Models.Hotels;
using Booking.Backend.HotelsWebApi.Repositories;
using Booking.Backend.HotelsWebApi.Repositories.Enitities;
using Booking.Backend.HotelsWebApi.Services.IServices;

namespace Booking.Backend.HotelsWebApi.Services
{
    public class HotelsService : IHotelService
    {
        private readonly IMapper _mapper;

        private readonly IHotelRepository _hotelRepository;

        public HotelsService(
            IMapper mapper,
            IHotelRepository hotelRepository)
        {
            _mapper = mapper;
            _hotelRepository = hotelRepository;
        }

        public Hotel GetHotelInfo(Guid id)
        {
            var hotelInfoEntity = _hotelRepository.GetHotelInfo(id);
            var hotelInfo = _mapper.Map<Hotel>(hotelInfoEntity);
            return hotelInfo;
        }

        public List<Hotel> GetFilteredHotels(GetFilteredHotelsRequest request)
        {
            var hotelInfoEntities = _hotelRepository.GetFilteredHotels(request.Destination, request.Option);
            var hotelInfoList = hotelInfoEntities
                .Select(hotelInfoEntity => _mapper.Map<Hotel>(hotelInfoEntity))
                .ToList();
            return hotelInfoList;
        }

        public Guid CreateHotel(HotelInfo hotelInfo)
        {
            hotelInfo.IsDelete = false;
            var hotelInfoEntity = _mapper.Map<HotelInfoEntity>(hotelInfo);
            _hotelRepository.AddHotel(hotelInfoEntity);
            return hotelInfoEntity.Id;
        }

        public void UpdateHotel(Hotel hotel)
        {
            var hotelInfoEntity = _hotelRepository.GetHotelInfo(hotel.Id);
            if (hotelInfoEntity == null)
                throw new Exception("Hotel not found");

            hotel.IsDelete = true;
            var hotelEntity = _mapper.Map<HotelInfoEntity>(hotel);
            _hotelRepository.UpdateHotel(hotelEntity);
        }

        public void DeleteHotel(Guid id)
        {
            var hotelInfoEntity = _hotelRepository.GetHotelInfo(id);
            var hotelInfo = _mapper.Map<Hotel>(hotelInfoEntity);
            hotelInfo.IsDelete = false;
            var hotelEntity = _mapper.Map<HotelInfoEntity>(hotelInfo);
            _hotelRepository.UpdateHotel(hotelEntity);
        }
    }
}
