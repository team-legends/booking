﻿using Booking.Backend.HotelsWebApi.Repositories;
using Booking.Backend.HotelsWebApi.Services.IServices;

namespace Booking.Backend.HotelsWebApi.Services
{
    public class MockService : IMockService
    {
        private readonly IMockHotelRepository _mockHotelRepository;

        public MockService(IMockHotelRepository mockHotelRepository)
        {
            _mockHotelRepository = mockHotelRepository;
        }

        public void GenerateMockHotels()
        {
            _mockHotelRepository.GenerateHotelEntities();
        }

        public void DeleteMockHotels()
        {
            _mockHotelRepository.RemoveHotelEntities();
        }
    }
}
