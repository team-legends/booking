﻿namespace Booking.Backend.HotelsWebApi.Services.IServices
{
    public interface IMockService
    {
        void DeleteMockHotels();
        void GenerateMockHotels();
    }
}
