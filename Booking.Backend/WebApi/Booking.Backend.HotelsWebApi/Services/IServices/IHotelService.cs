﻿using Booking.Backend.Hotels.Contracts;
using Booking.Backend.HotelsWebApi.Models.Hotels;

namespace Booking.Backend.HotelsWebApi.Services.IServices
{
    public interface IHotelService
    {
        Hotel GetHotelInfo(Guid id);

        List<Hotel> GetFilteredHotels(GetFilteredHotelsRequest request);

        Guid CreateHotel(HotelInfo hotel);

        void UpdateHotel(Hotel hotelInfo);

        void DeleteHotel(Guid id);
    }
}
