﻿using Booking.Backend.HotelsWebApi.Models.Hotels;
using FluentValidation;

namespace Booking.Backend.HotelsWebApi.Validation
{
    public class AddHotelRequestBodyValidation : AbstractValidator<AddHotelRequestBody>
    {
        public AddHotelRequestBodyValidation()
        {
            RuleFor(x => x.HotelInfo.ManagerId).NotEmpty().WithMessage("ManagerId is a required field");
        }
    }
}
