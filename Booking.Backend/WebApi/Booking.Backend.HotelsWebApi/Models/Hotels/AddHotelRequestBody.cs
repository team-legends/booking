﻿using Booking.Backend.Hotels.Contracts;

namespace Booking.Backend.HotelsWebApi.Models.Hotels
{
    /// <summary>
    /// Add new hotel request body
    /// </summary>
    public class AddHotelRequestBody
    {
        /// <summary>
        /// Hotel information
        /// </summary>
        public HotelInfo HotelInfo { get; set; }
    }
}
