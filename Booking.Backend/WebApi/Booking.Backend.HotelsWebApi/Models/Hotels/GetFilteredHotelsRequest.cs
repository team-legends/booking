﻿using Booking.Backend.Hotels.Contracts;

namespace Booking.Backend.HotelsWebApi.Models.Hotels
{
    public class GetFilteredHotelsRequest
    {
        /// <summary>
        /// Hotel location
        /// </summary>
        public string Destination { get; set; }

        /// <summary>
        /// Hotel options
        /// </summary>
        public HotelFilterOption Option { get; set; }
    }
}
