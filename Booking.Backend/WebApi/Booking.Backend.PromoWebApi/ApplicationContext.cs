﻿using Booking.Backend.PromoWebApi.Repositories.Enitities;
using Microsoft.EntityFrameworkCore;

namespace Booking.Backend.PromoWebApi
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
        }

        public DbSet<PromoEntity> PromoInfo { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

        }
    }
}
