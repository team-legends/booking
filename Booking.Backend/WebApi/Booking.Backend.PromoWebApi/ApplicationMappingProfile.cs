﻿using AutoMapper;
using Booking.Backend.PromoWebApi.Repositories.Enitities;

namespace Booking.Backend.PromoWebApi
{
    public class ApplicationMappingProfile : Profile
    {
        public ApplicationMappingProfile()
        {
            CreateMap<PromoEntity, GetPromoReply>().ReverseMap();
            CreateMap<PromoEntity, AddPromoRequest>().ReverseMap();
        }
    }
}
