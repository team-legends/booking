﻿using Booking.Backend.Commons.Middlewares;
using Booking.Backend.PromoWebApi.LoadingData;
using Booking.Backend.PromoWebApi.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Serilog;

namespace Booking.Backend.PromoWebApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);
            var logger = new LoggerConfiguration()
                .ReadFrom.Configuration(builder.Configuration)
                .CreateLogger();
            builder.Host.UseSerilog(logger);

            builder.Services.LoadData(builder.Configuration);

            builder.Services.AddGrpc();

            var app = builder.Build();

            app.UseLogging();
            app.UseExceptionHandle();

            app.MapGrpcService<PromoService>();

            app.Run();
        }
    }
}
