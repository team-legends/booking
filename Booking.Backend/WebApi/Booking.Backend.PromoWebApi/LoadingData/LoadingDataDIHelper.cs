﻿using Booking.Backend.PromoWebApi.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Booking.Backend.PromoWebApi.LoadingData
{
    /// <summary>
    /// 
    /// </summary>
    public static class LoadingDataDIHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        public static void LoadData(this IServiceCollection services, IConfiguration configuration)
        {
            var dbcs = configuration.GetConnectionString("DataBaseConnectionStrings");

            if (string.IsNullOrEmpty(dbcs))
                throw new Exception("Connection string does not match the format");

            var connectionString = string.Format(
                dbcs,
                Environment.GetEnvironmentVariable("DB_SERVER"),
                Environment.GetEnvironmentVariable("DB_PORT"),
                Environment.GetEnvironmentVariable("DB_USERNAME"),
                Environment.GetEnvironmentVariable("DB_PASSWORD"),
                Environment.GetEnvironmentVariable("DB_NAME"));

            services.AddDbContext<ApplicationContext>(optionBuilder =>
                optionBuilder.UseNpgsql(connectionString));

            services.AddScoped<IPromoRepository, PromoRepository>();

            services.AddAutoMapper(
                typeof(ApplicationMappingProfile));
        }
    }
}
