﻿using AutoMapper;
using Booking.Backend.PromoWebApi.Repositories;
using Booking.Backend.PromoWebApi.Repositories.Enitities;
using Grpc.Core;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Booking.Backend.PromoWebApi.Services
{
    public class PromoService : PromoProvider.PromoProviderBase
    {
        public PromoService(IPromoRepository promoRepository, IMapper mapper)
        {
            _repository = promoRepository;
            _mapper = mapper;
        }

        public override async Task<GetPromoReply> GetPromo(GetPromoRequest request, ServerCallContext context)
        {
            var promoesInRegion =
                await _repository.GetPromoByRegionAsync(request.Region);

            if (promoesInRegion.Any())
            {
                var random = Random.Shared.Next(0, promoesInRegion.Count - 1);
                var randomPromo = promoesInRegion[random];
                return _mapper.Map<GetPromoReply>(randomPromo);
            }
            return new GetPromoReply();
        }

        public override async Task<AddPromoReply> AddPromo(AddPromoRequest request, ServerCallContext context)
        {
            var addPromoEntity = _mapper.Map<PromoEntity>(request);
            addPromoEntity.Id = Guid.NewGuid();
            await _repository.AddPromoAsync(addPromoEntity);
            return new AddPromoReply() { Id = addPromoEntity.Id.ToString() };
        }

        private readonly IPromoRepository _repository;
        private readonly IMapper _mapper;
    }
}
