﻿using Booking.Backend.Commons.Models.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace Booking.Backend.PromoWebApi.Repositories.Enitities
{
    [Table("Promo")]
    public class PromoEntity : BaseEntity
    {
        public string Title { get; set; }

        public string Content { get; set; }

        public string LinkAddress { get; set; }

        public string Region { get; set; }
    }
}
