﻿using Booking.Backend.PromoWebApi.Repositories.Enitities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Booking.Backend.PromoWebApi.Repositories
{
    public class PromoRepository : IPromoRepository
    {
        public PromoRepository(ApplicationContext context)
        {
            _context = context;
        }

        public async Task AddPromoAsync(PromoEntity promoEntity)
        {
            _context.PromoInfo.Add(promoEntity);
            await _context.SaveChangesAsync();
        }

        public void DeletePromo(Guid promoId)
        {
            var promo = _context.PromoInfo.Find(promoId);
            if (promo != null)
            {
                _context.PromoInfo.Remove(promo);
                _context.SaveChanges();
            }
        }

        public Task<List<PromoEntity>> GetPromoByRegionAsync(string region)
        {
            return _context.PromoInfo
                .Where(promo => promo.Region == region)
                .ToListAsync();
        }

        private readonly ApplicationContext _context;
    }
}
