﻿using Booking.Backend.PromoWebApi.Repositories.Enitities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Booking.Backend.PromoWebApi.Repositories
{
    public interface IPromoRepository
    {
        Task AddPromoAsync(PromoEntity promoEntity);

        Task<List<PromoEntity>> GetPromoByRegionAsync(string region);

        void DeletePromo(Guid promoId);
    }
}
