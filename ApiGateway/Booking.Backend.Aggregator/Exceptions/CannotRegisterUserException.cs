﻿using System;

namespace Booking.Backend.Aggregator.Exceptions
{
    public class CannotRegisterUserException : Exception
    {
        public CannotRegisterUserException(string message, Exception ex = null) : base(message, ex)
        {

        }
    }
}
