﻿using System;

namespace Booking.Backend.Aggregator.Exceptions
{
    public class CannotLoginUserException : Exception
    {
        public CannotLoginUserException(string message, Exception ex = null) : base(message, ex)
        {

        }
    }
}
