﻿using Booking.Backend.Hotels.Contracts;
using System;
using Status = Booking.Backend.Reservations.Contracts.Enums.Status;
namespace Booking.Backend.Aggregator.Responses
{
    public class GetOrdersByUserIdItem
    {
        public Guid OrderId { get; set; }
        public HotelInfo Hotel { get; set; }
        public Room Room { get; set; }
        public Status Status { get; set; }
        public DateTime OrderDate { get; set; }
    }
}
