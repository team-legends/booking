﻿using System.Collections.Generic;

namespace Booking.Backend.Aggregator.Responses
{
    public class GetOrdersByUserIdResponse
    {
       public List<GetOrdersByUserIdItem> Items { get; set; }
    }
}
