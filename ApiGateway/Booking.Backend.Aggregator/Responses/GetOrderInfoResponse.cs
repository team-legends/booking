﻿namespace Booking.Backend.Aggregator.Responses
{
    public class GetOrderInfoResponse
    {
        public string HotelCountry { get; set; }
        public string HotelName { get; set; }
        public decimal RoomPrice { get; set; }
        public int RoomPersonQuantity { get; set; }
        public string UserName { get; set; }
        public string UserEmail { get; set; }
    }
}
