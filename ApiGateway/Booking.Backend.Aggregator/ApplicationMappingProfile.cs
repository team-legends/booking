﻿using AutoMapper;
using Booking.Backend.Aggregator.Requests;
using Booking.Backend.Reservations.Contracts;

namespace Booking.Backend.Aggregator
{
    public class ApplicationMappingProfile : Profile
    {
        public ApplicationMappingProfile()
        {
            CreateMap<CreateOrderRequest, Order>().ReverseMap();
        }
    }
}
