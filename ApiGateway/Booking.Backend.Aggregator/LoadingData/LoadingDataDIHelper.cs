﻿using Booking.Backend.Aggregator.Services;
using Booking.Backend.Aggregator.Services.Interfaces;
using Booking.Backend.Commons.KafkaProvider;
using Booking.Backend.Commons.Services;
using Booking.Backend.MessageSender.Contracts;
using Microsoft.Extensions.DependencyInjection;

namespace Booking.Backend.Aggregator.LoadingData
{
    public static class LoadingDataDIHelper
    {
        public static void LoadData(this IServiceCollection services)
        {
            services.AddSingleton<ImageManagementService>();
            services.AddSingleton<IAggregationService, AggregationService>();
            services.AddSingleton<IUsersService, UsersService>();
            services.AddSingleton<IAuthService, AuthService>();
            services.AddSingleton<IHotelsService, HotelsService>();
            services.AddSingleton<IReservationsService, ReservationsService>();
            services.AddSingleton<PromoService>();

            services.AddSingleton<KafkaClientHandle>();
            services.AddSingleton<RequestProducer<string, SendMessageRequest>>();

            services.AddAutoMapper(
               typeof(ApplicationMappingProfile));
        }
    }
}
