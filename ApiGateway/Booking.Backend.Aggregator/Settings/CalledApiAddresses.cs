﻿namespace Booking.Backend.Aggregator.Settings
{
    public class CalledApiAddresses
    {
        public const string ApiAddresses = "CalledApiAddresses";
        public string HotelsApi { get; set; }
        public string ReservationsApi { get; set; }
        public string UsersApi { get; set; }
        public string PromoApi { get; set; }
    }
}
