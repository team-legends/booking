﻿using Booking.Backend.Hotels.Contracts;

namespace Booking.Backend.Aggregator.Models
{
    public class AggregatedHotel : Hotel
    {
        /// <summary>
        /// Returns hotel state, booked or free.
        /// </summary>
        public bool IsBooked { get; set; }
    }
}
