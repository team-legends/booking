﻿using Booking.Backend.Aggregator.LoadingData;
using Booking.Backend.Aggregator.Settings;
using Booking.Backend.Commons.Configuration;
using Booking.Backend.Commons.Controllers.Controllers;
using Booking.Backend.Commons.Middlewares;
using Booking.Backend.Commons.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Converters;
using Serilog;
using System;
using System.IO;
using System.Reflection;
using System.Text;

var builder = WebApplication.CreateBuilder(args);
var configuration = builder.Configuration;

var logger = new LoggerConfiguration()
    .ReadFrom.Configuration(configuration)
    .CreateLogger();

builder.Host.UseSerilog(logger);

builder.Services.Configure<CalledApiAddresses>(
    builder.Configuration.GetSection(CalledApiAddresses.ApiAddresses));

builder.Services.LoadData();
builder.Services
    .AddControllers(options => options.SuppressImplicitRequiredAttributeForNonNullableReferenceTypes = true)
    .AddApplicationPart(typeof(HealthCheckController).GetTypeInfo().Assembly)
    .AddNewtonsoftJson(opts => opts.SerializerSettings.Converters.Add(new StringEnumConverter()));
builder.Services.AddEndpointsApiExplorer();

builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(options =>
    {
        options.RequireHttpsMetadata = false;
        options.SaveToken = true;
        options.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuer = false,
            ValidateAudience = false,
            //ValidAudience = configuration["Jwt:Audience"],
            //ValidIssuer = configuration["Jwt:Issuer"],
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Jwt:Key"]))
        };
    });

builder.Services.AddAuthorization();

builder.Services.AddSwaggerGen(c =>
{
    c.IncludeXmlComments(
        Path.Combine(AppContext.BaseDirectory, $"{Assembly.GetExecutingAssembly().GetName().Name}.xml"));
    c.IncludeXmlComments(
        Path.Combine(AppContext.BaseDirectory, $"{typeof(HealthCheckController).Assembly.GetName().Name}.xml"));
    c.IncludeXmlComments(
        Path.Combine(AppContext.BaseDirectory, $"{typeof(PingResponse).Assembly.GetName().Name}.xml"));
});
builder.Services.AddSwaggerGenNewtonsoftSupport();

builder.Services.AddOptions<ApplicationInfo>()
    .Bind(builder.Configuration.GetSection("ApplicationInfo"));

builder.Services.AddResponseCaching();
builder.Services.AddDistributedMemoryCache();
builder.Services.AddStackExchangeRedisCache(options =>
{
    options.Configuration = builder.Configuration.GetConnectionString("Redis");
});

var app = builder.Build();

app.UseStaticFiles();

app.UseLogging();
app.UseExceptionHandle();

if (!app.Environment.IsProduction())
{
    app.UseSwagger();
    app.UseSwaggerUI(options =>
    {
        options.DocumentTitle = "Booking.Backend.AggregatorWebApi API";
    });
}

app.UseHttpsRedirection();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();
