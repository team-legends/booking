﻿using Booking.Backend.Commons.Enums;
using Booking.Backend.Commons.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Booking.Backend.Aggregator.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImageController : ControllerBase
    {
        public ImageController(ImageManagementService imageManagementService, IWebHostEnvironment webHostEnvironment)
        {
            _imageManagementService = imageManagementService;
            _rootContentPath = webHostEnvironment.WebRootPath;
        }


        [HttpGet("GetImages/{parentObjectType}/{parentObjectId}")]
        public IActionResult GetImages(ObjectTypes parentObjectType, Guid parentObjectId)
        {
            var baseUrl = $"{HttpContext.Request.Scheme}://{HttpContext.Request.Host}";

            var images = _imageManagementService.GetImages(_rootContentPath, parentObjectType, parentObjectId)
                .Select(path => new Uri(new Uri(baseUrl), ConvertToQueryString(path)));
            return Ok(images);
        }

        [HttpPost("SaveImages/{parentObjectType}/{parentObjectId}")]
        public async Task<IActionResult> SaveImagesAsync(
            ObjectTypes parentObjectType, Guid parentObjectId, IFormFileCollection formFiles)
        {
            await _imageManagementService.SaveImagesAsync(_rootContentPath, parentObjectType, parentObjectId, formFiles);
            return Ok();
        }

        [HttpDelete("DeleteImage/{parentObjectType}/{parentObjectId}/{imageName}")]
        public IActionResult DeleteImage(ObjectTypes parentObjectType, Guid parentObjectId, string imageFileName)
        {
            _imageManagementService.DeleteImage(_rootContentPath, parentObjectType, parentObjectId, imageFileName);
            return Ok();
        }

        [HttpDelete("DeleteImages/{parentObjectType}/{parentObjectId}")]
        public IActionResult DeleteImages(ObjectTypes parentObjectType, Guid parentObjectId)
        {
            _imageManagementService.DeleteImages(_rootContentPath, parentObjectType, parentObjectId);
            return Ok();
        }

        private string ConvertToQueryString(string path)
        {
            var parhQuery = path.Replace(_rootContentPath, string.Empty);
            var urlQuery = string.Join("/", parhQuery.Split("\\"));
            return urlQuery;
        }

        private readonly ImageManagementService _imageManagementService;
        private readonly string _rootContentPath;
    }
}
