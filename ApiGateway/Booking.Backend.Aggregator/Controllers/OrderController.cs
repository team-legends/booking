﻿using Booking.Backend.Aggregator.Requests;
using Booking.Backend.Aggregator.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Booking.Backend.Aggregator.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class OrderController : ControllerBase
    {
        private readonly IReservationsService _reservationsService;
        public OrderController(IReservationsService reservationsService)
        {
            _reservationsService = reservationsService;
        }

        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromBody] CreateOrderRequest request)
        {
            await _reservationsService.CreateOrder(request);
            return Ok();
        }

        [HttpPost("GetOrderInfo")]
        public async Task<IActionResult> GetOrderInfo([FromBody] GetOrderInfoRequest request)
        {
            var newOrderInfo = await _reservationsService.GetOrderInfo(request);
            return Ok(newOrderInfo);
        }

        [HttpGet("GetOrdersByUserId/{userId:guid}")]
        public async Task<IActionResult> GetOrdersByUserId(Guid userId)
        {
            var orders = await _reservationsService.GetOrdersByUserId(userId);
            return Ok(orders);
        }
    }
}
