﻿using Booking.Backend.Aggregator.Models;
using Booking.Backend.Aggregator.Requests;
using Booking.Backend.Aggregator.Services;
using Booking.Backend.Hotels.Contracts;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Booking.Backend.Aggregator.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SchedulerController : ControllerBase
    {
        private readonly IAggregationService _agregatorService;

        public SchedulerController(IAggregationService agregatorService)
        {
            _agregatorService = agregatorService;
        }

        [HttpPost("GetFilteredHotels")]
        public async Task<ActionResult<List<AggregatedHotel>>> GetFilteredHotels([FromBody] GetFilteredHotelsRequest request)
        {
            //TODO:
            var result = await _agregatorService.GetFilteredHotels(request.Destination, request.Option, request.DateFrom, request.DateTo);
            foreach (var hotel in result)
            {
                hotel.Rooms = new List<Room> { new Room { Id = Guid.NewGuid() } };
            }
            
            return Ok(result);
        }

        [HttpGet("GetHotelInfo/{id:guid}", Name = "GetHotelInfo")]
        public async Task<ActionResult> GetHotelInfo(Guid id)
        {
            var result = await _agregatorService.GetHotelInfo(id);
            return Ok(result);
        }
    }
}
