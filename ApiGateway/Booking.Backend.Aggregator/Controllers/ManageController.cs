﻿using Booking.Backend.Aggregator.Requests;
using Booking.Backend.Aggregator.Services;
using Booking.Backend.Hotels.Contracts;
using Booking.Backend.Users.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading.Tasks;

namespace Booking.Backend.Aggregator.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize (Roles = "Manager")]
    public class ManageController : Controller
    {
        private readonly IAggregationService _agregatorService;

        public ManageController(IAggregationService agregatorService)
        {
            _agregatorService = agregatorService;
        }

        [HttpPost("GetHotelsByManagerId")]
        public ActionResult GetHotelsByManagerId([FromBody] GetHotelsByManagerIdRequest request)
        {
            //TODO:
            var hotels = new List<Hotel>
            {
                new Hotel
                {
                    Id = Guid.NewGuid(),
                    Rooms = new List<Room>{
                        new Room
                        {
                        Id = Guid.NewGuid(),
                        PeopleQuantity = 2,
                            Price = 200,
                            Images = new List<string>{
                            "https://images.unsplash.com/photo-1551782450-a2132b4ba21d",
                            "https://images.unsplash.com/photo-1522770179533-24471fcdba45",
                            "https://images.unsplash.com/photo-1444418776041-9c7e33cc5a9c"
                            }
                        }
                    },
                    Image = "https://images.unsplash.com/photo-1551963831-b3b1ca40c98e",
                    Name = "Hotel Name",
                    Description = "Hotel Description",
                    Types = Hotels.Contracts.Enums.HotelTypes.RESORT,
                    Address = new Address
                    {
                        CountryName = "CountryName"
                    },
                    StarsRate = 5,
                    Contacts = new List<Contact>
                    {
                        new Contact
                        {
                           Type = Hotels.Contracts.Enums.ContactTypes.VK,
                           Value = "Contract value",
                           InfoText = "InfoText"
                        },
                         new Contact
                        {
                           Type = Hotels.Contracts.Enums.ContactTypes.TELEGRAM,
                           Value = "TELEGRAM value",
                           InfoText = "TELEGRAM InfoText"
                        }
                    },
                    Services = new List<Service>
                    {
                        new Service
                        {
                             Type = Hotels.Contracts.Enums.ServiceTypes.PARKING,
                             InformationText = "InformationText"
                        }
                    }
                },
                new Hotel
                {
                    Id = Guid.NewGuid(),
                    Rooms = new List<Room>{
                        new Room
                        {
                        Id = Guid.NewGuid(),
                        PeopleQuantity = 2,
                            Price = 200,
                            Images = new List<string>{
                            "https://images.unsplash.com/photo-1551782450-a2132b4ba21d",
                            "https://images.unsplash.com/photo-1522770179533-24471fcdba45",
                            "https://images.unsplash.com/photo-1444418776041-9c7e33cc5a9c"
                            }
                        }
                    },
                    Image = "https://images.unsplash.com/photo-1551963831-b3b1ca40c98e",
                    Name = "Hotel Name2",
                    Description = "Hotel Description2",
                    Types = Hotels.Contracts.Enums.HotelTypes.RESORT,
                    Address = new Address
                    {
                        CountryName = "CountryName2"
                    },
                    StarsRate = 5,
                    Contacts = new List<Contact>
                    {
                        new Contact
                        {
                           Type = Hotels.Contracts.Enums.ContactTypes.VK,
                           Value = "Contract value2",
                           InfoText = "InfoText2"
                        }
                    },
                    Services = new List<Service>
                    {
                        new Service
                        {
                             Type = Hotels.Contracts.Enums.ServiceTypes.PARKING,
                             InformationText = "InformationText2"
                        }
                    }
                }
            };
            return Ok(hotels);
        }

        [HttpPut("EditHotel")]
        public async Task<ActionResult> EditHotel(Hotel hotel)
        {
            await _agregatorService.EditHotel(hotel);
            return Ok();
        }

        [HttpPost("CreateHotel")]
        public async Task<ActionResult<Guid>> CreateHotel([FromBody] CreateHotelRequest request)
        {
            var hotelsId = await _agregatorService.CreateHotel(request);
            return Ok(hotelsId);
        }

        [HttpPut("MarkDeleteHotel/{id:guid}")]
        public async Task<ActionResult> MarkDeleted(Guid id)
        {
            await _agregatorService.DeleteHotel(id);
            return Ok();
        }
    }
}
