﻿using Booking.Backend.Aggregator.Requests;
using Booking.Backend.Aggregator.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Booking.Backend.Aggregator.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService _authService;
        public AuthController(IAuthService authService)
        {
            _authService = authService;
        }

        [HttpPost("Register")]
        [AllowAnonymous]
        public async Task<ActionResult> Register([FromBody] RegisterRequest request)
        {
            (Guid id, string token) = await _authService.Register(request.Login, request.Password, request.Name, request.Email, request.IsManager);
            return Ok(new { id, token });
        }

        [HttpPost("Login")]
        [AllowAnonymous]
        public async Task<ActionResult> Login([FromBody] LoginRequest request)
        {
            (Guid id, string token) = await _authService.Login(request.Login, request.Password);
            return Ok(new { id, token });
        }
    }
}
