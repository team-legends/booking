﻿using Booking.Backend.Aggregator.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Booking.Backend.Aggregator.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PromoController : ControllerBase
    {
        public PromoController(PromoService promoService)
        {
            _promoService = promoService;
        }

        [HttpPost("AddPromo")]
        [AllowAnonymous]
        public async Task<ActionResult> AddPromoAsync([FromBody] AddPromoRequest request)
        {
            var promoInfo = await _promoService.AddPromo(request);
            return Ok(promoInfo);
        }

        [HttpGet("GetPromo")]
        public async Task<IActionResult> GetPromoAsync(string regionName)
        {
            var promoInfo = await _promoService.GetPromoInfo(regionName);
            return Ok(promoInfo);
        }

        private readonly PromoService _promoService;
    }
}
