﻿using Booking.Backend.Commons.KafkaProvider;
using Booking.Backend.MessageSender.Contracts;
using Confluent.Kafka;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Serilog;
using System;
using System.Threading.Tasks;

namespace Booking.Backend.Aggregator.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SendMessageController : ControllerBase
    {
        public SendMessageController(
            ILogger logger,
            IConfiguration config,
            RequestProducer<string, SendMessageRequest> requestProducer)
        {
            _logger = logger;
            _topic = config.GetValue<string>("Kafka:RequestTopic");
            _requestProducer = requestProducer;
        }

        [HttpPost("SendMessageAsync")]
        public async Task<IActionResult> SendMessageAsync(SendMessageRequest sendMessageRequest)
        {
            var result = await _requestProducer.ProduceAsync(
                _topic,
                new Message<string, SendMessageRequest>() { Key = Guid.NewGuid().ToString(), Value = sendMessageRequest });

            return Ok(result);
        }

        [HttpPost("SendMessage")]
        public async Task<IActionResult> SendMessage(SendMessageRequest sendMessageRequest)
        {
            PersistenceStatus status = PersistenceStatus.PossiblyPersisted;
            _requestProducer.Produce(
                _topic,
                new Message<string, SendMessageRequest>() { Key = Guid.NewGuid().ToString(), Value = sendMessageRequest },
                request =>
                {
                    if (request.Error == null)
                    {
                        _logger.Information($"Send message to {_topic} topic.");
                    }
                    else
                    {
                        _logger.Error(request.Error.Reason);
                    }
                    status = request.Status;
                });

            return Ok(new { Status = status.ToString() });
        }

        private readonly string _topic;
        private readonly RequestProducer<string, SendMessageRequest> _requestProducer;
        private readonly ILogger _logger;
    }
}
