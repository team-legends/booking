﻿using Booking.Backend.Aggregator.Requests;
using Booking.Backend.Aggregator.Responses;
using Booking.Backend.Reservations.Contracts;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Booking.Backend.Aggregator.Services
{
    public interface IReservationsService
    {
        public Task<List<OrderInfo>> GetFreeHotelsStateOnDefiniteDate(
            List<Guid> hotels, DateTime dateFrom, DateTime dateTo);

        public Task CreateOrder(CreateOrderRequest request);
        public Task<GetOrderInfoResponse> GetOrderInfo(GetOrderInfoRequest request);
        public Task<GetOrdersByUserIdResponse> GetOrdersByUserId(Guid userId);
    }
}
