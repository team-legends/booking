﻿using Booking.Backend.Users.Contracts;
using System.Threading.Tasks;

namespace Booking.Backend.Aggregator.Services
{
    public interface IUsersService
    {
        public Task<User> Login(string login, string password);
        public Task<User> Register(string login, string password, string name, string email, bool isManager);
    }
}
