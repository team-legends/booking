﻿using Booking.Backend.Aggregator.Models;
using Booking.Backend.Aggregator.Requests;
using Booking.Backend.Hotels.Contracts;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Booking.Backend.Aggregator.Services
{
    public interface IAggregationService
    {
        public Task<List<AggregatedHotel>> GetFilteredHotels(
            string destination, HotelFilterOption option, DateTime dateFrom, DateTime dateTo);

        public Task<Hotel> GetHotelInfo(Guid id);

        public Task<Guid> CreateHotel(CreateHotelRequest request);
        public Task EditHotel(Hotel hotel);
        public Task DeleteHotel(Guid id);
    }
}
