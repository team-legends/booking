﻿using Booking.Backend.Aggregator.Models;
using Booking.Backend.Aggregator.Requests;
using Booking.Backend.Hotels.Contracts;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Booking.Backend.Aggregator.Services
{
    public interface IHotelsService
    {
        public Task<IEnumerable<AggregatedHotel>> GetHotels(string destination, HotelFilterOption option);
        public Task<Hotel> GetHotelInfo(Guid id);
        public Task<Guid> CreateHotel(CreateHotelRequest hotel);
        public Task EditHotel(Hotel hotel);
        public Task DeleteHotel(Guid id);
    }
}
