﻿using System;
using System.Threading.Tasks;

namespace Booking.Backend.Aggregator.Services.Interfaces
{
    public interface IAuthService
    {
        public Task<(Guid, string)> Login(string login, string password);
        public Task<(Guid, string)> Register(string login, string password, string name, string email, bool isManager);
    }
}
