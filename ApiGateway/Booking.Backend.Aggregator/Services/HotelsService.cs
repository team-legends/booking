﻿using Booking.Backend.Aggregator.Models;
using Booking.Backend.Aggregator.Requests;
using Booking.Backend.Aggregator.Settings;
using Booking.Backend.Commons.APIClients;
using Booking.Backend.Hotels.Contracts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Booking.Backend.Aggregator.Services
{
    public class HotelsService : IHotelsService
    {
        private readonly WebProxy _proxy;
        private readonly string _url;

        public HotelsService(IOptions<CalledApiAddresses> options)
        {
            _proxy = new WebProxy();
            _url = options.Value.HotelsApi;
        }

        public async Task<Hotel> GetHotelInfo(Guid id)
        {
            try
            {
                string query = _url + $"GetHotelInfo/{id}";
                var hotel = await _proxy.SendAsync<Hotel>(HttpMethod.Get, query);
                return hotel;
            }
            catch (HttpRequestException e)
            {
                throw new WebProxyException(e);
            }
        }

        public async Task<IEnumerable<AggregatedHotel>> GetHotels(string destination, HotelFilterOption option)
        {
            try
            {
                string query = _url + "GetFilteredHotels";
                var hotels = await _proxy.SendAsync<List<AggregatedHotel>>(HttpMethod.Post, query, new { destination, option });
                return hotels;
            }
            catch (HttpRequestException e)
            {
                throw new WebProxyException(e);
            }
        }

        public async Task<Guid> CreateHotel(CreateHotelRequest request)
        {
            try
            {
                string query = _url + "Create";
                var hotelId = await _proxy.SendAsync<Guid>(HttpMethod.Post, query, request);
                return hotelId;
            }
            catch (HttpRequestException e)
            {
                throw new WebProxyException(e);
            }
        }

        public async Task EditHotel(Hotel hotel)
        {
            try
            {
                string query = _url + $"EditHotel";
                await _proxy.SendAsync<Guid>(HttpMethod.Post, query, hotel);
            }
            catch (HttpRequestException e)
            {
                throw new WebProxyException(e);
            }
        }

        public async Task DeleteHotel(Guid id)
        {
            try
            {
                string query = _url + $"MarkDeleteHotel/{id}";
                await _proxy.SendAsync<Guid>(HttpMethod.Put, query);
            }
            catch (HttpRequestException e)
            {
                throw new WebProxyException(e);
            }
        }
    }
}
