﻿using AutoMapper;
using Booking.Backend.Aggregator.Requests;
using Booking.Backend.Aggregator.Settings;
using Booking.Backend.Commons.APIClients;
using Booking.Backend.Reservations.Contracts;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Booking.Backend.Commons.KafkaProvider;
using Booking.Backend.MessageSender.Contracts;
using Confluent.Kafka;
using Booking.Backend.Users.Contracts;
using Serilog;
using Booking.Backend.Aggregator.Responses;
using Booking.Backend.Hotels.Contracts;
using System.Linq;

namespace Booking.Backend.Aggregator.Services
{
    public class ReservationsService : IReservationsService
    {
        private readonly WebProxy _proxy;
        private readonly string _reservationsUrl;
        private readonly string _usersUrl;
        private readonly string _hotelsUrl;
        private readonly IMapper _mapper;
        private readonly string _topic;
        private readonly RequestProducer<string, SendMessageRequest> _requestProducer;
        private readonly ILogger _logger;
        public ReservationsService(IOptions<CalledApiAddresses> options, IMapper mapper, IConfiguration config,
            RequestProducer<string, SendMessageRequest> requestProducer, ILogger logger)
        {
            _proxy = new WebProxy();
            _reservationsUrl = options.Value.ReservationsApi;
            _usersUrl = options.Value.UsersApi;
            _hotelsUrl = options.Value.HotelsApi;
            _mapper = mapper;
            _topic = config.GetValue<string>("Kafka:RequestTopic");
            _requestProducer = requestProducer;
            _logger = logger;
        }
        public async Task<List<OrderInfo>> GetFreeHotelsStateOnDefiniteDate(List<Guid> hotelRooms, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                var uri = new Uri(new Uri(_reservationsUrl), "GetOrdersByFilter");
                var orders = await _proxy.SendAsync<List<OrderInfo>>(HttpMethod.Post, uri, new { hotelRooms, dateFrom, dateTo });
                return orders;
            }
            catch (HttpRequestException e)
            {
                throw new WebProxyException(e);
            }
        }

        public async Task CreateOrder(CreateOrderRequest request)
        {
            var order = _mapper.Map<Order>(request);
            try
            {
                string ordersQuery = _reservationsUrl + "Create";
                string usersQuery = _usersUrl + $"GetUserById/{request.UserId}";

                var userCall = _proxy.SendAsync<User>(HttpMethod.Get, usersQuery);
                var orderCall = _proxy.SendAsync<string>(HttpMethod.Post, ordersQuery, order);
                var user = await userCall;
                var orderId = await orderCall;

                var sendMessageRequest = new SendMessageRequest
                {
                    User = user,
                    MessageParameters = new Dictionary<string, string> { { "orderId", $"{orderId}" } }
                };

                PersistenceStatus status = PersistenceStatus.PossiblyPersisted;
                _requestProducer.Produce(
                    _topic,
                    new Message<string, SendMessageRequest>() { Key = Guid.NewGuid().ToString(), Value = sendMessageRequest },
                    request =>
                    {
                        if (request.Error == null)
                        {
                            _logger.Information($"Send message to {_topic} topic.");
                        }
                        else
                        {
                            _logger.Error(request.Error.Reason);
                        }
                        status = request.Status;
                    });

            }
            catch (Exception ex)
            {
                throw new WebProxyException(ex);
            }

        }

        public async Task<GetOrderInfoResponse> GetOrderInfo(GetOrderInfoRequest request)
        {
            var result = new GetOrderInfoResponse { };
            try
            {
                string hotelsQuery = _hotelsUrl + $"GetHotelInfo/{request.HotelId}";
                string usersQuery = _usersUrl + $"GetUserById/{request.UserId}";
                var userCall = _proxy.SendAsync<User>(HttpMethod.Get, usersQuery);
                var hotelsCall = _proxy.SendAsync<HotelInfo>(HttpMethod.Get, hotelsQuery);
                var user = await userCall;
                var hotel = await hotelsCall;
                var room = hotel?.Rooms?.FirstOrDefault(r => r.Id == request.RoomId);
                if (user == null || hotel == null || room == null)
                    return result;

                result = new GetOrderInfoResponse {
                    HotelCountry = hotel.Address?.CountryName,
                    HotelName = hotel.Name,
                    RoomPrice = room.Price,
                    RoomPersonQuantity = room.PeopleQuantity,
                    UserName = user.Name,
                    UserEmail = user.Email
                };
                return result;
            }
            catch (Exception ex)
            {
                throw new WebProxyException(ex);
            }
        }

        public async Task<GetOrdersByUserIdResponse> GetOrdersByUserId(Guid userId)
        {
            string ordersQuery = _reservationsUrl + "GetOrdersByFilter";
            var request = new { UserId = userId };

            var result = new GetOrdersByUserIdResponse { Items = new List<GetOrdersByUserIdItem>() };
            try
            {
                var orders = await _proxy.SendAsync<List<Order>>(HttpMethod.Post, ordersQuery, request);
                if(orders == null)
                    return result;

                foreach (var order in orders)
                {
                    string hotelsQuery = _hotelsUrl + $"GetHotelInfo/{order.HotelId}";
                    var hotel = await _proxy.SendAsync<HotelInfo>(HttpMethod.Get, hotelsQuery);
                    var item = new GetOrdersByUserIdItem
                    {
                        OrderId = order.Id,
                        Hotel = hotel,
                        Room = hotel.Rooms.Single(r => r.Id == order.RoomId),
                        Status = order.Status,
                        OrderDate = order.Date
                    };
                    result.Items.Add(item);
                }
                return result;
                
            }
            catch (Exception ex)
            {
                throw new WebProxyException(ex);
            }
        }
    }
}
