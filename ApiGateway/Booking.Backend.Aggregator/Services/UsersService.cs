﻿using Booking.Backend.Aggregator.Settings;
using Booking.Backend.Commons.APIClients;
using Booking.Backend.Users.Contracts;
using Microsoft.Extensions.Options;
using System.Net.Http;
using System.Threading.Tasks;

namespace Booking.Backend.Aggregator.Services
{
    public class UsersService : IUsersService
    {
        private readonly WebProxy _proxy;
        private readonly string _url;
        public UsersService(IOptions<CalledApiAddresses> options)
        {
            _proxy = new WebProxy();
            _url = options.Value.UsersApi;
        }

        public async Task<User> Login(string login, string password)
        {
            try
            {
                string query = _url + "Login";
                var user = await _proxy.SendAsync<User>(HttpMethod.Post, query, new { login, password });
                return user;
            }
            catch (HttpRequestException e)
            {
                throw new WebProxyException(e);
            }
        }

        public async Task<User> Register(string login, string password, string name, string email, bool isManager)
        {
            try
            {
                string query = _url + "Register";
                var user = await _proxy.SendAsync<User>(HttpMethod.Post, query, new { login, password, name, email, isManager });
                return user;
            }
            catch (HttpRequestException e)
            {
                throw new WebProxyException(e);
            }
        }
    }
}
