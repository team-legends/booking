﻿using Booking.Backend.Aggregator.Settings;
using Grpc.Net.Client;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;
using static Booking.Backend.Aggregator.PromoProvider;

namespace Booking.Backend.Aggregator.Services
{
    public class PromoService
    {
        public PromoService(IOptions<CalledApiAddresses> options)
        {
            var promoApiUrl = options.Value.PromoApi;
            var chanal = GrpcChannel.ForAddress(promoApiUrl);
            _client = new PromoProviderClient(chanal);
        }

        public async Task<GetPromoReply> GetPromoInfo(string region)
        {
            var request = new GetPromoRequest() { Region = region };
            var promoInfo = await _client.GetPromoAsync(request);
            return promoInfo;
        }

        public async Task<AddPromoReply> AddPromo(AddPromoRequest addPromoRequest)
        {
            var createdPromoInfo = await _client.AddPromoAsync(addPromoRequest);
            return createdPromoInfo;
        }

        private readonly PromoProviderClient _client;
    }
}
