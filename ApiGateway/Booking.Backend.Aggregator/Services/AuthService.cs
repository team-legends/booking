﻿using Booking.Backend.Aggregator.Exceptions;
using Booking.Backend.Aggregator.Services.Interfaces;
using Booking.Backend.Users.Contracts;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Backend.Aggregator.Services
{
    public class AuthService : IAuthService
    {
        private readonly IUsersService _usersService;
        private readonly IConfiguration _configuration;
        public AuthService(IUsersService usersService, IConfiguration configuration)
        {
            _usersService = usersService;
            _configuration = configuration;
        }

        public async Task<(Guid, string)> Login(string login, string password)
        {
            try
            {
                var user = await _usersService.Login(login, password);

                string token = CreateToken(user);
                return (user.Id, token);
            }
            catch (Exception ex)
            {
                throw new CannotLoginUserException("Cannot login user", ex);
            }
        }

        public async Task<(Guid, string)> Register(string login, string password, string name, string email, bool isManager)
        {
            try
            {
                var user = await _usersService.Register(login, password, name, email, isManager);
                string token = CreateToken(user);
                return (user.Id, token);

            }
            catch (Exception ex)
            {
                throw new CannotRegisterUserException("Cannot register user", ex);
            }
        }

        private string CreateToken(User user)
        {
            List<Claim> claims = new List<Claim>()
        {
            new Claim(ClaimTypes.Email, user.Email),
            new Claim(ClaimTypes.Role, user.Role?.ToString())
        };

            var configKey = _configuration.GetSection("Jwt:Key").Value;

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configKey));

            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                claims: claims,
                expires: DateTime.Now.AddHours(24),
                signingCredentials: creds
            );

            var jwt = new JwtSecurityTokenHandler().WriteToken(token);

            return jwt;
        }
    }
}
