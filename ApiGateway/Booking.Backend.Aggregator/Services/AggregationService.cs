﻿using Booking.Backend.Aggregator.Models;
using Booking.Backend.Hotels.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.Json;
using Microsoft.Extensions.Caching.Distributed;
using Booking.Backend.Aggregator.Requests;

namespace Booking.Backend.Aggregator.Services
{
    public class AggregationService : IAggregationService
    {
        private readonly IHotelsService _hotelsService;
        private readonly IReservationsService _reservationsService;
        private readonly IDistributedCache _redisCache;
        public AggregationService(IHotelsService hotelsService, IReservationsService reservationsService, IDistributedCache redisCache)
        {
            _hotelsService = hotelsService;
            _reservationsService = reservationsService;
            _redisCache = redisCache;
        }

        public async Task<List<AggregatedHotel>> GetFilteredHotels(
            string destination, HotelFilterOption option, DateTime dateFrom, DateTime dateTo)
        {
            var hotels = (await _hotelsService.GetHotels(destination, option)) ?? new List<AggregatedHotel>();

            var hotelsIdsToSearch = hotels.Select(h => h.Id).ToList();

            var freeHotels = await _reservationsService.GetFreeHotelsStateOnDefiniteDate(hotelsIdsToSearch, dateFrom, dateTo);

            var freeHotelsIds = freeHotels?.Select(h => h.RoomId).ToHashSet();
            hotels.ToList().ForEach(h => SetHotelState(h, freeHotelsIds));

            return hotels.ToList();
        }

        public async Task<Hotel> GetHotelInfo(Guid id)
        {
            string _key = $"GetHotelInfo_{id}";

            string serialized = await _redisCache.GetStringAsync(_key);
            if (serialized != null)
            {
                return JsonSerializer.Deserialize<Hotel>(serialized);
            }

            var hotel = await _hotelsService.GetHotelInfo(id);

            await _redisCache.SetStringAsync(
                key: _key,
                value: JsonSerializer.Serialize(hotel),
                options: new DistributedCacheEntryOptions
                {
                    SlidingExpiration = TimeSpan.FromSeconds(600)
                });

            return hotel;
        }

        private void SetHotelState(AggregatedHotel hotel, HashSet<Guid> freeHotelsIds)
        {
            if (!freeHotelsIds?.Contains(hotel.Id) ?? true)
                hotel.IsBooked = true;
        }

        public async Task<Guid> CreateHotel(CreateHotelRequest request)
        {
            var id = await _hotelsService.CreateHotel(request);

            string _key = $"GetHotelInfo_{id}";
            await _redisCache.SetStringAsync(
                key: _key,
                value: JsonSerializer.Serialize(request.HotelInfo),
                options: new DistributedCacheEntryOptions
                {
                    SlidingExpiration = TimeSpan.FromSeconds(600)
                });

            return id;
        }

        public async Task EditHotel(Hotel hotel)
        {
            await _hotelsService.EditHotel(hotel);

            string _key = $"GetHotelInfo_{hotel.Id}";
            await _redisCache.SetStringAsync(
                key: _key,
                value: JsonSerializer.Serialize(hotel),
                options: new DistributedCacheEntryOptions
                {
                    SlidingExpiration = TimeSpan.FromSeconds(600)
                });
        }

        public async Task DeleteHotel(Guid id)
        {
            await _hotelsService.DeleteHotel(id);

            string _key = $"GetHotelInfo_{id}";
            await _redisCache.RemoveAsync(key: _key);
        }
    }
}
