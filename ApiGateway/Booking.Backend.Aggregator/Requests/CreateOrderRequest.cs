﻿using Booking.Backend.Reservations.Contracts;
using System;

namespace Booking.Backend.Aggregator.Requests
{
    public class CreateOrderRequest
    {
        public Guid UserId { get; set; }
        public Guid HotelId { get; set; }
        public Guid RoomId { get; set; }
        public string HotelCountry { get; set; }
        public string HotelName { get; set; }
        public decimal RoomPrice { get; set; }
        public int RoomPersonQuantity { get; set; }
        public string UserName { get; set; }
        public string UserEmail { get; set; }

    }
}
