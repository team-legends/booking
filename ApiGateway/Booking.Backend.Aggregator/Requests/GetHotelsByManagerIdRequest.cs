﻿using System;

namespace Booking.Backend.Aggregator.Requests
{
    public class GetHotelsByManagerIdRequest
    {
        public Guid ManagerId { get; set; }
    }
}
