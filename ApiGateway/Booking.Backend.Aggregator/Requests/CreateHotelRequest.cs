﻿using Booking.Backend.Hotels.Contracts;
using System;

namespace Booking.Backend.Aggregator.Requests
{
    public class CreateHotelRequest
    {
        /// <summary>
        /// Hotel information
        /// </summary>
        public HotelInfo HotelInfo { get; set; }
    }
}
