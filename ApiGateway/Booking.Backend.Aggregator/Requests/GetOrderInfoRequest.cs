﻿using System;

namespace Booking.Backend.Aggregator.Requests
{
    public class GetOrderInfoRequest
    {
        public Guid UserId { get; set; }
        public Guid HotelId { get; set; }
        public Guid RoomId { get; set; }
    }
}
