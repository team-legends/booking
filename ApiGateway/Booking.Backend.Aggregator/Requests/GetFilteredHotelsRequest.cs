﻿using Booking.Backend.Hotels.Contracts;
using System;

namespace Booking.Backend.Aggregator.Requests
{
    public record GetFilteredHotelsRequest
    {
        public string Destination { get; init; }
        public DateTime DateFrom { get; init; }
        public DateTime DateTo { get; init; }
        public HotelFilterOption Option { get; init; }
    }
}
