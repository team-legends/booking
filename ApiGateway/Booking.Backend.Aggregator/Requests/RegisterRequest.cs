﻿namespace Booking.Backend.Aggregator.Requests
{
    public class RegisterRequest
    {
        public string Name { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public bool IsManager  { get; set; }
    }
}
